import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import NavigationBar from './general/NavigationBar';
import Footer from './general/Footer';

class App extends Component {
  render() {
    return (
			<div id="container">
					
				<NavigationBar/>

				<Header/>

				<ShortDescription />

				<Footer />

			</div>

    );
  }
}


class Header extends React.Component{
	
	render(){
		return(
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-heading">Bem Vindo!</div>
		  <Link to={{ pathname: "/register"}} class="btn btn-danger btn-xl text-uppercase" id="tellB">Registe-se!</Link>
        </div>
      </div>
    </header>
		);
	}
}


class ShortDescription extends React.Component{

	render(){
		return(
			<section id="description">
			<div class="container">
				<div class="row">
				<div class=" desc col-lg-12 text-left">
				<h1 class="section-heading text-uppercase">Sistema de Apoio à Decisão Para a Otimização do Planeamento de Serviços de Apoio Domiciliário</h1>
				<h2 class="section-subheading text-muted">Esta plataforma permite determinar a melhor sequência de visitas a ser efetuada pelos auxiliares 
				de apoio domiciliário aos pacientes registados, de maneira a que as suas necessidades sejam completamente satisfeitas!</h2>
				</div>
				</div>
			</div>
			</section>
		);
	}
}

export default App;
