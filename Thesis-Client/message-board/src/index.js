import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Register from './views/Register';
import Login from './views/Login';
import InitialPlan from './views/InitialPlan';
import PatientPage from './views/PatientPage';
import GeneratePlan from './views/GeneratePlan';
import MapSolution from './views/MapSolution';
import UserPage from './views/UserPage';
import PlanPage from './views/PlanPage';
import WorkerPage from './views/WorkerPage';
import TaskPage from './views/TaskPage';
import registerServiceWorker from './registerServiceWorker';

import { BrowserRouter as Router, Route } from 'react-router-dom';

ReactDOM.render(
<Router>
    <div>
	<Route exact path="/" component={App}/>
	<Route path="/home" component={App}/>
    <Route path="/register" component={Register}/>
    <Route path="/login" component={Login}/>
	<Route exact path="/user/:id" component={UserPage}/>
	<Route exact path="/user/:id/start" component={InitialPlan}/>
	<Route exact path="/user/:id/plan/:idP" component={PlanPage}/>
	<Route exact path="/user/:id/plan/:idP/worker/:idW" component={WorkerPage}/>
	<Route exact path="/user/:id/plan/:idP/patient/:idD" component={PatientPage}/>
	<Route exact path="/user/:id/plan/:idP/patient/:idD/task/:idT" component={TaskPage}/>
	<Route exact path="/user/:id/plan/:idP/generate" component={GeneratePlan}/>
	<Route exact path="/user/:id/plan/:idP/solu/:typeSolu" component={MapSolution}/>
    </div>
</Router>, document.getElementById('root'));
registerServiceWorker();