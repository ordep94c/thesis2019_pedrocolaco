import React, { Component } from "react";
import { Link } from 'react-router-dom';
import NavigationBar from '../general/NavigationBar.js';
import Footer from '../general/Footer.js';
import $ from 'jquery';

export default class WorkerPage extends Component {
  constructor(props) {
    super(props);
	
	this.state = {

	worker: {},

  defaultImg:'defaultP.jpg',
  currentImg: '',

	name: '',
	contact: '',
	email: '',
	descr: '',

	idUser: '',
	idPlan: '',
	idWorker: '',

	view: 1
    }

    this.defaultView = this.defaultView.bind(this);
    this.editView = this.editView.bind(this);
 	this.removeView = this.removeView.bind(this);
 	this.changeContact = this.changeContact.bind(this);
 	this.editSuccessView = this.editSuccessView.bind(this);
 	this.removeSuccessView = this.removeSuccessView.bind(this);
 	this.removeWoker = this.removeWoker.bind(this);
 	this.goToPlanPage = this.goToPlanPage.bind(this);
 	this.refreshPage = this.refreshPage.bind(this);
  }

    componentWillReceiveProps (nextProps) {
	    this.setWorker(nextProps)
	}

	componentDidMount () {
	    this.setWorker(this.props)
	}

	setWorker(u){

    var searchIDUser = u.match.params.id;
    var searchIDPlan = u.match.params.idP;
    var searchIDWorker = u.match.params.idW;
    
    var that = this;

    that.setState({idUser: searchIDUser})
    that.setState({idPlan: searchIDPlan})
    that.setState({idWorker: searchIDWorker})

        fetch('http://localhost:8080/users/'+searchIDUser+'/plans/'+searchIDPlan+'/workers/'+searchIDWorker)
        .then(response => response.json())
        .then(function(data){ 
          that.setState({ worker: data })
          that.setState({ name: data.nameWorker })
            that.setState({ currentImg: data.dirPic })
          that.setState({ contact: data.workerContact })
          that.setState({ email: data.emailWorker })
          that.setState({ descr: data.infoWorker })
        });     

  	}

    defaultView () {
	this.setState({ view : 1 });
	}

	editView() {
	this.setState({ view : 2 });
	}

	removeView() {
	this.setState({ view : 3 });
	}

	changeContact(event) {
	    const onlyNumber = (event.target.validity.valid) ? event.target.value : this.state.contact;
	    this.setState({ contact: onlyNumber });
	}

	editSuccessView() {

    var idUser = this.state.idUser;
    var idPlan = this.state.idPlan;
    var idWorker = this.state.idWorker;

    var editName = '';
     var editImage = '';
    var editContact = '';
    var editEmail = '';
    var editInfo = '';

    var nameEdit = $("#inputNamePatient").val();
     var imageEdit = $("#inputImage").val();
    var contactEdit = $("#inputContact").val();
    var emailEdit = $("#inputEmail").val();
    var infoEdit = $("#inputDescrAdd").val();

    if(contactEdit === ''){
      alert("Por Favor Preencha o Campo Relativo ao Número de Contacto")
    }

    else{

    if(nameEdit.length > 0){
      editName = nameEdit;
    }
    else{
      editName = this.state.name
    }

    if(imageEdit.length > 0){
    imageEdit = imageEdit.split("fakepath\\");
    editImage = imageEdit[1];      
    }
    else{
      editImage = this.state.currentImg
    }


    editContact = contactEdit;

    if(emailEdit.length > 0){
      editEmail = emailEdit;
    }
    else{
      editEmail = this.state.email
    }

    if(infoEdit.length > 0){
      editInfo = infoEdit;
    }
    else{
      editInfo = this.state.descr
    }

    var that = this;

    fetch('http://localhost:8080/users/' + idUser + "/plans/" + idPlan + "/workers/" + idWorker, {
          method: 'PUT',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          },
            body: JSON.stringify({
            nameWorker: editName,
            dirPic: editImage,
            workerContact: editContact,
            emailWorker: editEmail,
            infoWorker: editInfo
          })
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
              that.setState({ view: 4 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });
    }

	}

	refreshPage(){
	  window.location.reload();
	}

	removeSuccessView() {
	this.setState({ view : 5 });
	}

	removeWoker(){

    var idUser = this.state.idUser;
    var idPlan = this.state.idPlan;
    var idWorker = this.state.idWorker;

    var that = this;

    fetch('http://localhost:8080/users/'+idUser+'/plans/'+idPlan+'/workers/'+idWorker, {
          method: 'DELETE',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          }
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
              that.setState({ view: 5 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });
  	}

	goToPlanPage(){
    	this.props.history.push({ pathname: "/user/" + this.state.idUser + "/plan/" + this.state.idPlan })
  	}
    
  
  render() {

	const areaView = this.state.view;

    return (
	  <div class="workerPage">
	  
	  <NavigationBar />

	  <h1 className="generatePlan1-heading">Auxiliar</h1>
	  
	{ areaView !== 2 && areaView !== 3 && areaView !== 4 && areaView !== 5 &&

	 	 <WorkerInfo 
			nameW = {this.state.name} 
      defaultI = {this.state.defaultImg}
      currentI = {this.state.currentImg}
      contactW = {this.state.contact}
      emailW = {this.state.email}
      descrW = {this.state.descr}
      editV = {this.editView}
			removeV = {this.removeView}
			/>

	}

  	{ areaView === 2 &&
    
     <EditWorker
			nameW = {this.state.name} 
      contactW = {this.state.contact}
      emailW = {this.state.email}
			descrW = {this.state.descr}
			historyV = {this.defaultView}
			editSuccessV = {this.editSuccessView}
			changeC = {this.changeContact}
			/>
	}

	{ areaView === 3 &&
    
     <RemoveWorker
			historyV = {this.defaultView}
			removeV = {this.removeWoker}
			/>
	}


	{ areaView === 4 &&

	<EditWorkerSuccess
		refresh = {this.refreshPage}
		/>

	}

	{ areaView === 5 &&

	<RemoveWorkerSuccess
		planV = {this.goToPlanPage}
		/>

	}

	  <Footer/>
	  
	 </div>
    );
  }
  
}


class WorkerInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {

  function importAll(r) {
    let images = {};
    r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
    return images;
  }

  const images = importAll(require.context('../imgs', false, /\.(png|jpe?g|svg)$/));
  
  var profilePic = '';


  if(!this.props.currentI || 0 === this.props.currentI.length){
    profilePic = this.props.defaultI;
  }
  else{
    profilePic = this.props.currentI;
  }

    return (   
	
	<div class="workerInfo">     

		<div class="container-fluid span6">
		<div class="row">

		<div class="col-sm-4">
	      	<div id = "workerPageCol1">
        <img className="img-responsive" src={images[profilePic]} title="profile image" class="img-circle img-profile" alt="Profile Pic" id="imgID" />  		</div>
	    </div>

	    <div class="col-sm-6">
	      	<div id = "workerPageCol2">
	            <h1>{this.props.nameW}</h1>
	            <h4>{this.props.contactW}</h4>
	            <h4>{this.props.emailW}</h4>
	            <br/>
	            <h4>{this.props.descrW}</h4>
	    	</div>
	    </div>

	    <div class="col-sm-2">
	   		<div id = "workerPageCol3">
	            <div class="btn-group" id="buttonWorkerOpt">
	                <a class="btn dropdown-toggle btn-danger" data-toggle="dropdown" id="buttonWorkerOpt" href="#">
	                    Opções  
	                    <span class="icon-cog icon-white"></span><span class="caret"></span>
	                </a>
	                <ul class="dropdown-menu">
	                    <li><a href="#" onClick={this.props.editV}><span class="icon-wrench"></span>Editar</a></li>
	                    <li><a href="#" onClick={this.props.removeV}><span class="icon-trash"></span>Remover</a></li>
	                </ul>
	            </div>
	     	</div>
	    </div>

		</div>
		</div>

	</div>
	
	);
  }
  
}


class EditWorker extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {
    return (   
	
	<div class="editWorker">     

    <form id="form-signin">
        <input type="text" id="inputNamePatient" class="form-control" placeholder={this.props.nameW} />
        <input type="file" id="inputImage" class="form-control" placeholder="Seleccione Imagem"/>
        <input class="form-control" id="inputContact" type="text" pattern="[0-9]*" value={this.props.contactW} onChange={this.props.changeC}></input>
        <input type="text" id="inputEmail" class="form-control" placeholder={this.props.emailW} />
       
        <textarea id="inputDescrAdd" class="form-control" placeholder={this.props.descrW} />
        
        <button type="button" id="editWorkerButton" onClick={ this.props.historyV } class="btn btn-danger">Cancelar</button>
		<button type="button" id="editWorkerButton2" onClick={ this.props.editSuccessV } class="btn btn-success">Editar</button>     
    </form>

	</div>
	
	);
  }
  
}

class RemoveWorker extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {
    return (   
	
	<div class="row">
  	<section id = "removePatient">   

	<h1>Tem a certeza que pretende remover este trabalhador do plano?</h1>
        
    <button type="button" id="removeWorkerButton" onClick={ this.props.historyV } class="btn btn-danger">Não</button>
	<button type="button" id="removeWorkerButton2" onClick={ this.props.removeV } class="btn btn-success">Sim</button>     

	</section>
	</div>
	
	);
  }
  
}

class EditWorkerSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>O Auxiliar Foi Editado Com Sucesso!</h3>

    <button type="button" onClick={this.props.refresh} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
  	</div>

      );
  }
  
}


class RemoveWorkerSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>O Auxiliar Foi Removido do Plano com Sucesso!</h3>

    <button type="button" onClick={this.props.planV} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
  </div>

      );
  }
  
}