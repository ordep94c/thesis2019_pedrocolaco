import React, { Component } from "react";
import { Link } from 'react-router-dom';
import NavigationBar from '../general/NavigationBar.js';
import Footer from '../general/Footer.js';


export default class UserPage extends Component {
  constructor(props) {
    super(props);
    
	this.state = {
	  user: {},
	  plans:[]
    };
  }
  

    componentWillReceiveProps (nextProps) {
	    this.setUser(nextProps)
	}

	componentDidMount () {
	    this.setUser(this.props)
	}

	setUser(u){

    var searchID = u.match.params.id;
    
    var that = this;

        fetch('http://localhost:8080/users/'+searchID)
        .then(response => response.json())
        .then(function(data){
          that.setState({user: data})
        })

        fetch('http://localhost:8080/users/'+searchID+'/plans')
        .then(response => response.json())
        .then(function(data){ 
          that.setState({ plans: data })
        });     

  	}

    render() {

    var searchID = this.state.user.userID;
	
	var addButton = [<Link to={{ pathname: "/user/" + searchID + "/start" }} id="addPlanButton" class="btn btn-danger">
                      <span class="glyphicon glyphicon-plus"></span> Adicionar Novo Plano 
                    </Link>];

    var plansLinks = [];
    
    for (let i = 0; i < this.state.plans.length ; i++) {

    	var path = "/user/" + searchID + "/plan/" + this.state.plans[i].planId;
        plansLinks.push(<Link to = {{ pathname: path }}><h4 class="section-heading text-uppercase">{this.state.plans[i].namePlan}</h4></Link>)

    }
    

    return (
	  <div class="userPage">
	  
	  <NavigationBar />

	  <h1 className="generatePlan1-heading">Utilizador</h1>

	  <section id="sectionNameUser"> 
	  <h2 className="userName-heading">{this.state.user.userName}</h2>
	  </section>

	  <div class="row">
	  <section id = "plansUser">
	  	<h2 class="section-heading text-uppercase">Planos</h2>
	    
		{plansLinks}

		{addButton}

	  </section>
	  </div>
	     
	   <Footer/>
	  
	  </div>
    );
  }
}

