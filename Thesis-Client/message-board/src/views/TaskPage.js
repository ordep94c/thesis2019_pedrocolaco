import React, { Component } from "react";
import { Link } from 'react-router-dom';
import NavigationBar from '../general/NavigationBar.js';
import Footer from '../general/Footer.js';
import $ from 'jquery';

export default class TaskPage extends Component {
  constructor(props) {
    super(props);
	
	this.state = {

	  idUserT: '',
	  idPlanT: '',
	  idPatientT: '',
	  idTaskT: '',

	  day: '',
	  taskType: '',
	  taskTypeTransp: '',
	  twBegin: '',
	  twEnd: '',
	  dur: '',
	  resize: '',
	  info: '',

	  view: 1
    }

    this.defaultView = this.defaultView.bind(this);
    this.editView = this.editView.bind(this);
 	this.removeView = this.removeView.bind(this);
 	this.removeSuccessView = this.removeSuccessView.bind(this);
 	this.goToPatientPage = this.goToPatientPage.bind(this);
 	this.editSuccessView = this.editSuccessView.bind(this);
 	this.refreshPage = this.refreshPage.bind(this);
  }

    componentWillReceiveProps (nextProps) {
	    this.setTask(nextProps)
	}

	componentWillMount () {
	    this.setTask(this.props)
	}

	setTask(t){

	var searchIDUser = t.match.params.id;
    var searchIDPlan = t.match.params.idP;
    var searchIDPatient = t.match.params.idD;
    var searchIDTask = t.match.params.idT;
    
    var that = this;

    that.setState({idUserT: searchIDUser})
    that.setState({idPlanT: searchIDPlan})
    that.setState({idPatientT: searchIDPatient})
    that.setState({idTaskT: searchIDTask})         

      	fetch('http://localhost:8080/users/'+searchIDUser+'/plans/'+searchIDPlan+'/patients/'+searchIDPatient+"/tasks/"+searchIDTask)
        .then(response => response.json())
        .then(function(data){
          that.setState({day: data.dayTask})
          that.setState({taskType: data.typeTask})
          that.setState({taskTypeTransp: data.typeTransportTask})
          that.setState({twBegin: data.iniTWTask})
          that.setState({twEnd: data.finTWTask})
          that.setState({dur: data.durationTask})
          that.setState({resize: data.changeTWTask})
          that.setState({info: data.infoTask})
        })

	}

    defaultView () {
	this.setState({ view : 1 });
	}

	editView() {
	this.setState({ view : 2 });
	}

	removeView() {
	this.setState({ view : 3 });
	}

	removeSuccessView(){
	
	var idUser = this.state.idUserT;
    var idPlan = this.state.idPlanT;
    var idPatient = this.state.idPatientT;
    var idTask = this.state.idTaskT;

    var that = this;

    fetch('http://localhost:8080/users/' + idUser + '/plans/' + idPlan + '/patients/' + idPatient + '/tasks/' + idTask, {
          method: 'DELETE',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          }
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
              that.setState({ view: 4 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });

	}


	goToPatientPage(){
    	this.props.history.push({ pathname: "/user/" + this.state.idUserT + "/plan/" + this.state.idPlanT + "/patient/" + this.state.idPatientT })
  	}

  	editSuccessView(){

	var idUser = this.state.idUserT;
    var idPlan = this.state.idPlanT;
    var idPatient = this.state.idPatientT;
    var idTask = this.state.idTaskT;

    var typeTask = '';
    var typeTransportTask = '';
    var iniTWTask = '';
    var finTWTask = '';
    var durationTask = '';
    var changeTWTask = '';

    var typeTaskEdit = $("#inputTaskEdit1").val();
    var typeTransportTaskEdit = $("#inputTaskEdit2").val();
    var iniTWTaskEdit = $("#inputTaskEdit3").val();
    var finTWTaskEdit = $("#inputTaskEdit4").val();
    var durationTaskEdit = $("#inputTaskEdit5").val();
    var changeTWTaskEdit = $("#inputTaskEdit6").val();
    var infoTaskEdit = $("#inputTask7").val();

    if(typeTaskEdit === '1 Auxiliar'){
    	typeTask = 1;
    }
    else if(typeTaskEdit === '2 Auxiliares'){
    	typeTask = 2;
    }

    if(typeTransportTaskEdit === 'Tarefa Normal'){
    	typeTransportTask = 0;
    }
    else if(typeTransportTaskEdit === 'Tarefa de Transporte Para o Centro Sem Prioridade'){
    	typeTransportTask = 1;
    }
    else if(typeTransportTaskEdit === 'Tarefa de Transporte Para o Centro Com Prioridade'){
    	typeTransportTask = 2;
    }
    else if(typeTransportTaskEdit === 'Tarefa de Transporte Para Casa Sem Prioridade'){
    	typeTransportTask = 3;
    }
    else if(typeTransportTaskEdit === 'Tarefa de Transporte Para Casa Com Prioridade'){
    	typeTransportTask = 4;
    }

    if(changeTWTaskEdit === 'Não'){
    	changeTWTask = false;
    }
    else if(changeTWTaskEdit === 'Sim'){
    	changeTWTask = true;
    }

    var twIValues = iniTWTaskEdit.split(":");
    iniTWTask = parseInt( ( twIValues[0] - 8 ) * 60 ) + parseInt(twIValues[1]); 

    var twFValues = finTWTaskEdit.split(":");
    finTWTask = parseInt( ( twFValues[0] - 8 ) * 60 ) + parseInt(twFValues[1]); 

    var durValues = durationTaskEdit.split(" Minutos");
    durationTask = durValues[0];

    var that = this;

    fetch('http://localhost:8080/users/' + idUser + "/plans/" + idPlan + "/patients/" + idPatient + "/tasks/" + idTask, {
          method: 'PUT',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          },
            body: JSON.stringify({
            typeTask: typeTask,
            typeTransportTask: typeTransportTask,
            iniTWTask: iniTWTask,
            finTWTask: finTWTask,
            durationTask: durationTask,
            changeTWTask: changeTWTask,
            infoTask: infoTaskEdit
          })
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
            	console.log(data)
              that.setState({ view: 5 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });

	}

	refreshPage(){
	  window.location.reload();
	}
    
  
  render() {

	const areaView = this.state.view;

    return (
	  <div class="taskPage">
	  
	  <NavigationBar />

	  <h1 className="generatePlan1-heading">Tarefa</h1>
	  
	{ areaView !== 2 && areaView !== 3 && areaView !== 4 && areaView !== 5 && 

	 	 <TaskInfo 
	 	 	dayT = {this.state.day}
	  		taskTypeT = {this.state.taskType}
	  		taskTypeTranspT = {this.state.taskTypeTransp}
			twBeginT = {this.state.twBegin}
			twEndT = {this.state.twEnd}
			durT = {this.state.dur}
			resizeT = {this.state.resize}
			infoT = {this.state.info}

			editV = {this.editView}
			removeV = {this.removeView}
			/>

	}

  	{ areaView === 2 &&
    
     <EditTask
			taskTypeT = {this.state.taskType}
			typeTranspTaskT = {this.state.taskTypeTransp}
			twBeginT = {this.state.twBegin}
			twEndT = {this.state.twEnd}
			durT = {this.state.dur}
			resizeT = {this.state.resize}
			infoT = {this.state.info}

			defaultV = {this.defaultView}
			editSuccessV = {this.editSuccessView}
			/>
	}

	{ areaView === 3 &&
    
     <RemoveTask
			defaultV = {this.defaultView}
			removeSuccessV = {this.removeSuccessView}
			/>
	}

	{ areaView === 4 &&
    
     <RemoveTaskSuccess
			goToPatientPage = {this.goToPatientPage}
			/>
	}

	{ areaView === 5 &&
    
     <EditTaskSuccess
			refresh = {this.refreshPage}
			/>
	}

	  <Footer/>
	  
	 </div>
    );
  }
  
}


class TaskInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {

  	var typeTask = null;

  	if(this.props.taskTypeT === 1){
		typeTask = <h4>1 Auxiliar</h4>
  	}
  	else if(this.props.taskTypeT === 2){
  		typeTask = <h4>2 Auxiliares</h4>
  	}


	var typeTranspTask = null;

	if(this.props.taskTypeTranspT === 0){
		typeTranspTask = <h4>Tarefa Normal</h4>
	}
	else if(this.props.taskTypeTranspT === 1){
		typeTranspTask = <h4>Tarefa de Transporte Para o Centro Sem Prioridade</h4>
	}
	else if(this.props.taskTypeTranspT === 2){
		typeTranspTask = <h4>Tarefa de Transporte Para o Centro Com Prioridade</h4>
	}
	else if(this.props.taskTypeTranspT === 3){
		typeTranspTask = <h4>Tarefa de Transporte Para Casa Sem Prioridade</h4>
	}
	else if(this.props.taskTypeTranspT === 4){
		typeTranspTask = <h4>Tarefa de Transporte Para Casa Com Prioridade</h4>
	}


  	var resizeTW = null;

	if(this.props.resizeT === false){
		resizeTW = <h4>Possível Alargamento da Janela Temporal: Não</h4>
	}
	else if(this.props.resizeT === true){
		resizeTW = <h4>Possível Alargamento da Janela Temporal: Sim</h4>
	}

    var totalMinutesTWi = this.props.twBeginT;
    var hoursTWi = Math.floor(totalMinutesTWi/60) + 8;
    if (hoursTWi.toString().length == 1) {
        hoursTWi = "0" + hoursTWi;
    }
    var minutesTWi = (totalMinutesTWi % 60);
    if (minutesTWi.toString().length == 1) {
        minutesTWi = "0" + minutesTWi;
    }

    var totalMinutesTWf = this.props.twEndT;
    var hoursTWf = Math.floor(totalMinutesTWf/60) + 8;
    if (hoursTWf.toString().length == 1) {
        hoursTWf = "0" + hoursTWf;
    }
    var minutesTWf = (totalMinutesTWf % 60);
    if (minutesTWf.toString().length == 1) {
        minutesTWf = "0" + minutesTWf;
    }

    var showDay = this.props.dayT + 1;

    return (   
	
	<div class="taskInfo">     

		<div class="container-fluid span6">
		<div class="row">

	    <div class="col-sm-10">
	      	<div id = "taskPageCol">
	            <h1>Dia {showDay}</h1>
	            {typeTask}
	            {typeTranspTask}
	            <h4>Janela Temporal: {hoursTWi}:{minutesTWi} - {hoursTWf}:{minutesTWf}</h4>
	            {resizeTW}
	            <h4>Duração Prevista: {this.props.durT} Minutos</h4>
	            <br/>
	            <h4>{this.props.infoT}</h4>
	    	</div>
	    </div>

	    <div class="col-sm-2">
	   		<div id = "taskPageCol2">
	            <div class="btn-group">
	                <a class="btn dropdown-toggle btn-danger" data-toggle="dropdown" href="#">
	                    Opções  
	                    <span class="icon-cog icon-white"></span><span class="caret"></span>
	                </a>
	                <ul class="dropdown-menu">
	                    <li><a href="#" onClick={this.props.editV}><span class="icon-wrench"></span>Editar</a></li>
	                    <li><a href="#" onClick={this.props.removeV}><span class="icon-trash"></span>Remover</a></li>
	                </ul>
	            </div>
	     	</div>
	    </div>

		</div>
		</div>

	</div>
	
	);
  }
  
}


class EditTask extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {

  	var typeTask = null;

  	if(this.props.taskTypeT === 1){
		typeTask = [ 
				  	<select id="inputTaskEdit1" class="form-control selectpicker show-tick">
					    <option>Seleccione o Número de Auxiliares Necessários</option>
					    <option selected>1 Auxiliar</option>
					    <option>2 Auxiliares</option>
					</select>
					
		    	   ]
  	}
  	else if(this.props.taskTypeT === 2){
  		typeTask = [ 
					<select id="inputTaskEdit1" class="form-control selectpicker show-tick">
		    			<option>Seleccione o Número de Auxiliares Necessários</option>
		     			<option>1 Auxiliar</option>
		    	     	<option selected>2 Auxiliares</option>
		 			</select>
		    	   ]
  	}


  	var typeTranspTask = null;

  	if(this.props.typeTranspTaskT === 0){
  		typeTranspTask = [
		  <select id="inputTaskEdit2" class="form-control selectpicker show-tick">
		    <option>Seleccione o Tipo de Tarefa</option>
		    <option selected>Tarefa Normal</option>
		    <option>Tarefa de Transporte Para o Centro Sem Prioridade</option>
		    <option>Tarefa de Transporte Para o Centro Com Prioridade</option>
		    <option>Tarefa de Transporte Para Casa Sem Prioridade</option>
		    <option>Tarefa de Transporte Para Casa Com Prioridade</option>
		  </select>
		]
  	}
  	else if(this.props.typeTranspTaskT === 1){
  		typeTranspTask = [
		  <select id="inputTaskEdit2" class="form-control selectpicker show-tick">
		    <option>Seleccione o Tipo de Tarefa</option>
		    <option>Tarefa Normal</option>
		    <option selected>Tarefa de Transporte Para o Centro Sem Prioridade</option>
		    <option>Tarefa de Transporte Para o Centro Com Prioridade</option>
		    <option>Tarefa de Transporte Para Casa Sem Prioridade</option>
		    <option>Tarefa de Transporte Para Casa Com Prioridade</option>
		  </select>
		]
  	}
  	else if(this.props.typeTranspTaskT === 2){
  		typeTranspTask = [
		  <select id="inputTaskEdit2" class="form-control selectpicker show-tick">
		    <option>Seleccione o Tipo de Tarefa</option>
		    <option selected>Tarefa Normal</option>
		    <option>Tarefa de Transporte Para o Centro Sem Prioridade</option>
		    <option selected>Tarefa de Transporte Para o Centro Com Prioridade</option>
		    <option>Tarefa de Transporte Para Casa Sem Prioridade</option>
		    <option>Tarefa de Transporte Para Casa Sem Prioridade</option>
		  </select>
		]
  	}
  	else if(this.props.typeTranspTaskT === 3){
  		typeTranspTask = [
		  <select id="inputTaskEdit2" class="form-control selectpicker show-tick">
		    <option>Seleccione o Tipo de Tarefa</option>
		    <option>Tarefa Normal</option>
		    <option>Tarefa de Transporte Para o Centro Sem Prioridade</option>
		    <option>Tarefa de Transporte Para o Centro Com Prioridade</option>
		    <option selected>Tarefa de Transporte Para Casa Sem Prioridade</option>
		    <option>Tarefa de Transporte Para Casa Com Prioridade</option>
		  </select>
		]
  	}
  	else if(this.props.typeTranspTaskT === 4){
  		typeTranspTask = [
  		  <select id="inputTaskEdit2" class="form-control selectpicker show-tick">
		    <option>Seleccione o Tipo de Tarefa</option>
		    <option>Tarefa Normal</option>
		    <option>Tarefa de Transporte Para o Centro Sem Prioridade</option>
		    <option>Tarefa de Transporte Para o Centro Com Prioridade</option>
		    <option>Tarefa de Transporte Para Casa Sem Prioridade</option>
		    <option selected>Tarefa de Transporte Para Casa Com Prioridade</option>
		  </select>
		]
  	}

	
	var resizeTask = null;
	if(this.props.resizeT === false){
		resizeTask = [ 
		  <select id="inputTaskEdit6" class="form-control selectpicker show-tick">
		  	<option>A Janela Temporal Poderá Ser Alargada?</option>
		    <option selected>Não</option>
		    <option>Sim</option>
		  </select>
					
		    	   ]
  	}
  	else if(this.props.resizeT === true){
  		resizeTask = [ 
		  <select id="inputTaskEdit6" class="form-control selectpicker show-tick">
		  	<option>A Janela Temporal Poderá Ser Alargada?</option>
		    <option>Não</option>
		    <option selected>Sim</option>
		  </select>
		    	   ]
  	}


    var totalMinutesTWi = this.props.twBeginT;
    var hoursTWi = Math.floor(totalMinutesTWi/60) + 8;
    if (hoursTWi.toString().length == 1) {
        hoursTWi = "0" + hoursTWi;
    }
    var minutesTWi = (totalMinutesTWi % 60);
    if (minutesTWi.toString().length == 1) {
        minutesTWi = "0" + minutesTWi;
    }

    var totalMinutesTWf = this.props.twEndT;
    var hoursTWf = Math.floor(totalMinutesTWf/60) + 8;
    if (hoursTWf.toString().length == 1) {
        hoursTWf = "0" + hoursTWf;
    }
    var minutesTWf = (totalMinutesTWf % 60);
    if (minutesTWf.toString().length == 1) {
        minutesTWf = "0" + minutesTWf;
    }


    var minutesDur = this.props.durT;

    return (   
	
	<div class="editWorker">    

    <form id="form-signin">

    	<div id = "inputTask1" class="container text-center">
		    {typeTask}
		</div>

		<div id = "inputTask2" class="container text-center">
			{typeTranspTask}
		</div>

		<div id = "inputTask3" class="container text-center">
		  <select id="inputTaskEdit3" class="form-control selectpicker show-tick">
		  	<option>Seleccione a Janela Temporal - Início</option>
		  	<option selected>{hoursTWi}:{minutesTWi}</option>
		    <option>8:00</option>
		    <option>8:30</option>
		    <option>9:00</option>
		    <option>9:30</option>
		    <option>10:00</option>
		    <option>10:30</option>
		    <option>11:00</option>
		    <option>11:30</option>
		    <option>12:00</option>
		    <option>12:30</option>
		    <option>12:00</option>
		    <option>12:30</option>
		    <option>13:00</option>
		    <option>13:30</option>
		    <option>14:00</option>
		    <option>14:30</option>
		    <option>15:00</option>
		    <option>15:30</option>
		    <option>16:00</option>
		    <option>16:30</option>
		    <option>17:00</option>
		    <option>17:30</option>
		    <option>18:00</option>
		    <option>18:30</option>
		    <option>19:00</option>
		    <option>19:30</option>
		    <option>20:00</option>
		  </select>
		</div>

		<div id = "inputTask4" class="container text-center">
		  <select id="inputTaskEdit4" class="form-control selectpicker show-tick">
		  	<option>Seleccione a Janela Temporal - Fim</option>
		  	<option selected>{hoursTWf}:{minutesTWf}</option>
		    <option>8:00</option>
		    <option>8:30</option>
		    <option>9:00</option>
		    <option>9:30</option>
		    <option>10:00</option>
		    <option>10:30</option>
		    <option>11:00</option>
		    <option>11:30</option>
		    <option>12:00</option>
		    <option>12:30</option>
		    <option>12:00</option>
		    <option>12:30</option>
		    <option>13:00</option>
		    <option>13:30</option>
		    <option>14:00</option>
		    <option>14:30</option>
		    <option>15:00</option>
		    <option>15:30</option>
		    <option>16:00</option>
		    <option>16:30</option>
		    <option>17:00</option>
		    <option>17:30</option>
		    <option>18:00</option>
		    <option>18:30</option>
		    <option>19:00</option>
		    <option>19:30</option>
		    <option>20:00</option>
		  </select>
		</div>

		<div id = "inputTask5" class="container text-center">
		  <select id="inputTaskEdit5" class="form-control selectpicker show-tick">
		  	<option>Seleccione a Duração Prevista</option>
		  	<option selected>{minutesDur} Minutos</option>
		    <option>5 Minutos</option>
		    <option>10 Minutos</option>
		    <option>15 Minutos</option>
		    <option>20 Minutos</option>
		    <option>25 Minutos</option>
		    <option>30 Minutos</option>
		    <option>35 Minutos</option>
		    <option>40 Minutos</option>
		    <option>45 Minutos</option>
		    <option>50 Minutos</option>
		    <option>55 Minutos</option>
		    <option>60 Minutos</option>
		    <option>65 Minutos</option>
		    <option>70 Minutos</option>
		    <option>75 Minutos</option>
		    <option>80 Minutos</option>
		    <option>85 Minutos</option>
		    <option>90 Minutos</option>
		    <option>95 Minutos</option>
		    <option>100 Minutos</option>
		    <option>105 Minutos</option>
		    <option>110 Minutos</option>
		    <option>115 Minutos</option>
		    <option>120 Minutos</option>
		  </select>
		</div>

		<div id = "inputTask6" class="container text-center">
			{resizeTask}
		</div>

       
        <textarea id = "inputTask7" class="form-control" placeholder={this.props.infoT} />
        
        <button type="button" id="editWorkerButton" onClick={ this.props.defaultV } class="btn btn-danger">Cancelar</button>
		<button type="button" id="editWorkerButton2" onClick={ this.props.editSuccessV } class="btn btn-success">Editar</button>     
    </form>

	</div>
	
	);
  }
  
}


class RemoveTask extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {
    return (   

    <div class="row">
	  	<section id = "removePatient">    

		<h1>Tem a certeza que pretende remover esta tarefa do plano?</h1>
	        
	    <button type="button" id="removeWorkerButton" onClick={ this.props.defaultV } class="btn btn-danger">Não</button>
		<button type="button" id="removeWorkerButton2" onClick={ this.props.removeSuccessV } class="btn btn-success">Sim</button>     

		</section>
	</div>
	
	);
  }
  
}


class RemoveTaskSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
	    <section id = "quiz8">

	    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
	    <h1>Já Está!</h1>
	    <h3>A Tarefa Foi Removida do Plano com Sucesso!</h3>

	    <button type="button" onClick={this.props.goToPatientPage} id="quiz8Button" class="btn btn-success">Continuar</button>

	    </section>
  	</div>

      );
  }
  
}


class EditTaskSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>A Tarefa Foi Editada com Sucesso!</h3>

    <button type="button" onClick={this.props.refresh} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
  </div>

      );
  }
  
}

