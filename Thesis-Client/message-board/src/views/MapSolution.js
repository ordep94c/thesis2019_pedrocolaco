import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Map, InfoWindow, Marker, GoogleApiWrapper,Polyline  } from "google-maps-react";
import NavigationBar from '../general/NavigationBar.js';
import Footer from '../general/Footer.js';

export class MapSolution extends Component {
	constructor(props) {
		super(props);
		this.onMarkerClick = this.onMarkerClick.bind(this);
		this.state = {

			planSolu: {},
			idUser: '',
			idPlan: '',
			typeSolu: '',
			solu: [],
			showingInfoWindow: false,
			activeMarker: {},
			selectedPlace: {},
			view: 1

		}

	}

	componentWillReceiveProps (nextProps) {
		this.setSolu(nextProps)
	}

	componentDidMount () {
		this.setSolu(this.props)
	}

	setSolu(u){

		var idUser = u.match.params.id;
		var idPlan = u.match.params.idP;
		var typeSolu = u.match.params.typeSolu;

		var that = this;
		that.setState({idUser: idUser})
		that.setState({idPlan: idPlan})
		that.setState({typeSolu: typeSolu})

		fetch('http://localhost:8080/users/'+idUser+'/plans/'+idPlan)
		.then(response => response.json())
		.then(function(data){

			that.setState({planSolu: data})

		})

		fetch('http://localhost:8080/users/'+idUser+'/plans/'+idPlan+'/solus')
		.then(response => response.json())
		.then(function(data){

			that.setState({solu: data})
			if(data.length > 0){
				that.setState({view: 2})
			}

		})

	}

	onMarkerClick(props, marker, e) {
		this.setState({
			selectedPlace: props,
			activeMarker: marker,
			showingInfoWindow: true
		});
	}



	render() {
		const areaView = this.state.view;
		const idU = this.state.idUser;
		const idP = this.state.idPlan;

		var title = null;
		if(this.state.typeSolu === '1'){
			title =  <h1 class="soluPage-heading">Solução - Minimização do Tempo Total de Trabalho</h1> 
		}
		else if(this.state.typeSolu === '2'){
			title =  <h1 class="soluPage-heading">Solução - Minimização do Tempo de Espera Entre Tarefas</h1> 
		}
		else if(this.state.typeSolu === '3'){
			title =  <h1 class="soluPage-heading">Solução - Maximização da Justiça do Tempo Total de Trabalho</h1> 
		}

		return (
		<div class="soluPage">

		<NavigationBar />

		{title}

		{ areaView === 1 &&
			   <div class="row">
			    <section id = "quiz8">

			    <h3>Solução Ainda Não Existente</h3>

			    <Link to={{ pathname: "/user/" + idU + "/plan/" + idP }} id="quiz8Button" class="btn btn-success">
                     Continuar
                </Link>

			    </section>
			  </div>
		}

		{ areaView === 2 &&
			<MapInfo
			typeSolu = {this.state.typeSolu}
			solu = {this.state.solu}
			showingInfoWindow = {this.state.showingInfoWindow}
			activeMarker = {this.state.activeMarker}
			selectedPlace = {this.state.selectedPlace}
			onMarkerClick = {this.onMarkerClick}
      		g = {this.props.google}
			/>
		}

		{ areaView === 2 &&
			<SoluInfo
			plan = {this.state.planSolu}
			/>
		}


		<Footer/>

		</div>
		);
	}
}



class MapInfo extends Component {
	constructor(props) {
		super(props)
		this.state = {
		}
	}

render() {

	const pathCoordinatesTeam0 = [ 
		{lat: 38.7244, lng: -9.22363},
		{lat: 38.72429, lng: -9.22276},
		{lat: 38.72482, lng: -9.2235},
		{lat: 38.72434, lng: -9.22758},
		{lat: 38.70496, lng: -9.22931},
		{lat: 38.70378, lng: -9.22785},
		{lat: 38.70479, lng: -9.23187},
		{lat: 38.7244, lng: -9.22363}
		] 

		const pathCoordinatesTeam2 = [ 
		{lat: 38.7244,  lng: -9.22363},
		{lat: 38.72482, lng: -9.2235},
		{lat: 38.7244,  lng: -9.22363},
		{lat: 38.70228, lng: -9.23019},
		{lat: 38.70378, lng: -9.22785},
		{lat: 38.70479, lng: -9.23187},
		{lat: 38.72482, lng: -9.2235},
		{lat: 38.72434, lng: -9.22758},
		{lat: 38.72154, lng: -9.22678},
		{lat: 38.7045,  lng: -9.22895},
		{lat: 38.70378, lng: -9.22785},
		{lat: 38.70479, lng: -9.23187}
		] 

		const pathCoordinatesTeam4 = [ 
		{lat: 38.7244,  lng: -9.22363},
		{lat: 38.70228, lng: -9.23019},
		{lat: 38.7045,  lng: -9.22895},
		{lat: 38.7244,  lng: -9.22363},
		{lat: 38.72154, lng: -9.22678},
		{lat: 38.7244,  lng: -9.22363},
		{lat: 38.70154, lng: -9.22868},
		{lat: 38.70228, lng: -9.23019}
		] 

		var carouselElements = [];
		for (let i = 1; i < this.props.solu.length ; i++) {
			carouselElements.push( <li data-target="#myCarousel" data-slide-to={i}></li> )
		}

		var carouselElementsData = [];
		for (let i = 0; i < this.props.solu.length ; i++) {

			var solutionDay = {};
			if(this.props.typeSolu === '1'){
				solutionDay = this.props.solu[i].totalTimeSolution;
			}
			else if(this.props.typeSolu === '2'){
				solutionDay = this.props.solu[i].waitingTimeSolution;
			}
			else if(this.props.typeSolu === '3'){
				solutionDay = this.props.solu[i].fairSolution;
			}

			var mapRoutesSolution = solutionDay.routesArcs;
			var mapTimeWorkedByWorker = solutionDay.valuesTimeWorkedByWorker;
			var mapvaluesTimeWaitingByWorker = solutionDay.valuesTimeWaitingByWorker;

			var carouselWorkersData = [];
			var carouselWorkersRoutes = [];

			carouselElementsData.push(
			<h3>Dia {i+1}</h3>
			)

			var teamID = 1;
			var auxTV = 0;
			for (let j = 0; j < (Object.keys(mapRoutesSolution).length) ; j++) {

				if(auxTV === 2){
					teamID = teamID +1;
					auxTV = 0;
				}

				carouselElementsData.push(
				<div className="col-md-2" id="colRT1">
				<h3 class="section-heading text-uppercase">Equipa {teamID}</h3>
				<h4>Worker {j}</h4>
				</div>
				)

				var routeWorker = mapRoutesSolution[j];
				var routeW2 = [];
				var routeW = [
				<div className="col-md-3">
				<h2>Worker {j}</h2>
				{routeW2}
				<h3>Tempo Total de Trabalho: {Math.round(mapTimeWorkedByWorker[j])} Minutos</h3>
				<h3>Tempo Total de Espera Entre Tarefas: {mapvaluesTimeWaitingByWorker[j]}  Minutos</h3>
				</div>
				];

				for(let k=0; k < routeWorker.routeSol.length; k++){
					var type = routeWorker.routeSol[k].type;
					var typeTransp = routeWorker.routeSol[k].typeTransp;
					var typeString = "";
					var typeTranspString = "";

					if(type === 1){
						typeString = "Tarefa Requer 2 Auxiliares"
					}
					else if(type === 2){
						typeString = "Tarefa Requer 1 Auxiliar"
					}

					if(typeTransp === 0){
						typeTranspString = "Tarefa Normal"
					}
					else if(typeTransp === 1){
						typeTranspString = "Tarefa de Transporte Para o Centro, Sem Prioridade"
					}
					else if(typeTransp === 2){
						typeTranspString = "Tarefa de Transporte Para o Centro, Com Prioridade"
					}
					else if(typeTransp === 3){
						typeTranspString = "Tarefa de Transporte Para Casa, Sem Prioridade"
					}
					else if(typeTransp === 4){
						typeTranspString = "Tarefa de Transporte Para o Centro, Com Prioridade"
					}
				

				    	var totalMinutesTWi = routeWorker.routeSol[k].startTime;
					    var hoursTWi = Math.floor(totalMinutesTWi/60) + 8;
					    if (hoursTWi.toString().length == 1) {
					        hoursTWi = "0" + hoursTWi;
					    }
					    var minutesTWi = (totalMinutesTWi % 60);
					    minutesTWi = Math.round(minutesTWi);
					    if (minutesTWi.toString().length == 1) {
					        minutesTWi = "0" + minutesTWi;
					    }

					    var totalMinutesTWf = routeWorker.routeSol[k].finishTime;
					    var hoursTWf = Math.floor(totalMinutesTWf/60) + 8;
					    if (hoursTWf.toString().length == 1) {
					        hoursTWf = "0" + hoursTWf;
					    }
					    var minutesTWf = (totalMinutesTWf % 60);
					    minutesTWf = Math.round(minutesTWf);
					    if (minutesTWf.toString().length == 1) {
					        minutesTWf = "0" + minutesTWf;
					    }

					 var waitTimePediction = Math.round(routeWorker.routeSol[k].waitTime);

					var route = [
					<div>
					<h4> {typeString} </h4>
					<h4> {typeTranspString} </h4>
					<h5> {routeWorker.routeSol[k].node1} - {routeWorker.routeSol[k].node2} </h5>
					<h5> {hoursTWi}:{minutesTWi} - {hoursTWf}:{minutesTWf} </h5>
					<h5> Tempo de Espera Previsto: {waitTimePediction} Minutos </h5>
					<br/>
					</div>
					];

					routeW2.push(route);
				}

				carouselWorkersRoutes.push(routeW);

				auxTV = auxTV +1;
			}


			carouselElementsData.push(
			<div class="item active">
			

			<section id="routesTeams">
			<div>

			<div class="container">

			<div class="row">

			{carouselWorkersData}

			</div>
			</div>

			</div>
			</section>

			<div class="row">

			{carouselWorkersRoutes}

			</div>

			<section id = "teamsRoutesData">

			<h3 class="section-heading text-uppercase">Tempo de Trabalho Total Previsto: {Math.round(solutionDay.totalTimeWorked)} Minutos</h3>
			<h3 class="section-heading text-uppercase">Tempo de Espera Total Previsto: {Math.round(solutionDay.totalTimeWaiting)} Minutos</h3>
			<h3 class="section-heading text-uppercase">Justiça: {Math.round(solutionDay.fairnessValue * 100)/100} %</h3>

			</section>

			</div>)
		}

	return (   

	<div id="mainSoluPage">



	<section id = "soluID">
	<div>
	<Map
	google={this.props.g}
	className={"map"}
	initialCenter={{
		lat: 38.7244,
		lng: -9.22363
	}}
	zoom={13}
	onClick={this.onClick}
	>

	<Polyline
	path= {pathCoordinatesTeam0}
	geodesic= {true}
	options={{ strokeColor: '#FF0000',
	strokeOpacity: 1.0,
	strokeWeight: 2,
	}}
	/>

<Polyline
path= {pathCoordinatesTeam2}
geodesic= {true}
options={{ strokeColor: '#FFC125',
strokeOpacity: 1.0,
strokeWeight: 2,
}}
/>

<Polyline
path= {pathCoordinatesTeam4}
geodesic= {true}
options={{ strokeColor: '#113377',
strokeOpacity: 1.0,
strokeWeight: 2,
}}
/>


<Marker
onClick={this.props.onMarkerClick}
name = { 'Centro' }
icon={{
	url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
	scaledSize: new window.google.maps.Size(50, 50)
}}
position = {{lat: 38.7244,  lng: -9.22363}}
/>		
<Marker
onClick={this.props.onMarkerClick}
name = { 'Madalena Gonçalves' }
position = {{lat: 38.70154,  lng: -9.22868}}
/>
<Marker
onClick={this.props.onMarkerClick}
name = { 'Mª de Lurdes Robalo' }
position = {{lat: 38.70228,  lng: -9.23019}}
/>
<Marker
onClick={this.props.onMarkerClick}
name = { 'Manuel Barros' }
position = {{lat: 38.70378,  lng: -9.22785}}
/>
<Marker
onClick={this.props.onMarkerClick}
name = { 'Mª Vitória Santos' }
position = {{lat: 38.70496,  lng: -9.22931}}
/>
<Marker
onClick={this.props.onMarkerClick}
name = { 'Carlos Firmino' }
position = {{lat: 38.70479,  lng: -9.23187}}
/>
<Marker
onClick={this.props.onMarkerClick}
name = { 'Mª Lurdes das Neves' }
position = {{lat: 38.7045,  lng: -9.22895}}
/>
<Marker
onClick={this.onMarkerClick}
name = { 'Armindo Lima Rocha' }
position = {{lat: 38.72429,  lng: -9.22276}}
/>
<Marker
onClick={this.props.onMarkerClick}
name = { 'Francelina de Jesus Neves' }
position = {{lat: 38.72482,  lng: -9.2235}}
/>
<Marker
onClick={this.props.onMarkerClick}
name = { 'Ilda Augusta Freitas' }
position = {{lat: 38.72434,  lng: -9.22758}}
/>
<Marker
onClick={this.props.onMarkerClick}
name = { 'Mª Filomena Lázaro Nunes Paulo' }
position = {{lat: 38.72154,  lng:  -9.22678}}
/>

<InfoWindow
marker={this.props.activeMarker}	
visible={this.props.showingInfoWindow}
>
<div>
<h1>{this.props.selectedPlace.name}</h1>
</div>
</InfoWindow>

</Map>

</div>
</section>



<section id = "routesTeamsList">

<h2 class="section-heading text-uppercase" id="titleRoutes">Rotas</h2>

<div id="myCarousel" class="carousel slide" data-ride="carousel">

<ol class="carousel-indicators">
<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
{carouselElements}
</ol>

<div class="carousel-inner">
{carouselElementsData}
</div>

<a class="left carousel-control" href="#myCarousel" data-slide="prev">
<span class="glyphicon glyphicon-chevron-left"></span>
<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#myCarousel" data-slide="next">
<span class="glyphicon glyphicon-chevron-right"></span>
<span class="sr-only">Next</span>
</a>
</div>

</section>

</div>

);
}

}

class SoluInfo extends Component {
	constructor(props) {
		super(props)
		this.state = {
		}
	}

	render() {

		var maxMinutes = this.props.plan.maxTime / 60;

		var totalMinuteslunch = this.props.plan.lunchTime;
		var hoursLunch = Math.floor(totalMinuteslunch/60) + 8;
		if (hoursLunch.toString().length == 1) {
			hoursLunch = "0" + hoursLunch;
		}
		var minutesLunch = (totalMinuteslunch % 60);
		if (minutesLunch.toString().length == 1) {
			minutesLunch = "0" + minutesLunch;
		}

		var constraint = '';
		if(this.props.plan.sameTeam_samePatient_constraint === true){
			constraint = 'Sim';
		}
		else{
			constraint = 'Não';
		}

		var search = '';
		if(this.props.plan.extensiveSearch === true){
			search = 'Sim';
		}
		else{
			search = 'Não';
		}

		return (   

			<div class="soluInfo">     

			<h4>Trabalho Diário Máximo Permitido: {maxMinutes} Horas</h4>
			<h4>Tempo Máximo de Espera Entre Tarefas: {this.props.plan.maxWaitTimeBetweenTasks} Minutos</h4>
			<h4>Horário de Almoço: {hoursLunch}:{minutesLunch}</h4>
			<h4>Mesmo Paciente - Mesma Equipa: {constraint}</h4>
			<h4>Alargamento de Janelas Temporais: {this.props.plan.tasksTWresize} %</h4>
			<h4>Procura Exaustiva: {search}</h4>

			</div>

			);
		}

	}


	export default GoogleApiWrapper({
		apiKey: "AIzaSyBxc6paFHyMgKf_uvLL0aexnLGM6BA_J60"
	})(MapSolution);