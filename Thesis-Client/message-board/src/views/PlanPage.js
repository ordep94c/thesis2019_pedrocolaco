import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import NavigationBar from '../general/NavigationBar.js';
import Footer from '../general/Footer.js';
import $ from 'jquery';

export class PlanPage extends Component {
  constructor(props) {
    super(props);
    
	this.state = {
    plan: {},

	  namePlan: '',
	  transport: '',
	  horizon: '',
	  lunches: '',

    transpV: '',
    lunchV: '',

	  listPatientsData: [],
	  listWorkersData: [],
    listVansData: [],
    newVans: '',

    idUserOfPlan: '',
    idOfPlan: '',

	  view: 1,
    seeSolus: 1,

    marker:
      {
        name: "Lisbon",
        position: { lat: 38.713533, lng: -9.238069}
      }
    }

    this.onClickMap = this.onClickMap.bind(this);
	  this.workersView = this.workersView.bind(this);
    this.patientsView = this.patientsView.bind(this);
    this.vansView = this.vansView.bind(this);
    this.editVansView = this.editVansView.bind(this);
    this.changeVans = this.changeVans.bind(this);
    this.addWorkersView = this.addWorkersView.bind(this);
    this.addPatientsView = this.addPatientsView.bind(this);
    this.editPlanView = this.editPlanView.bind(this);
    this.removePlanView = this.removePlanView.bind(this);
    this.newWorkerView = this.newWorkerView.bind(this);
    this.newPatientView = this.newPatientView.bind(this);
    this.newVanView = this.newVanView.bind(this);
    this.removePlanSuccessView = this.removePlanSuccessView.bind(this);
    this.goToUserPage = this.goToUserPage.bind(this);
    this.editPlanSuccessView = this.editPlanSuccessView.bind(this);
    this.removePlan = this.removePlan.bind(this);
    this.changeDays = this.changeDays.bind(this);
    this.refreshPage = this.refreshPage.bind(this);
    this.seeAllSolus = this.seeAllSolus.bind(this);
    this.clearSolu = this.clearSolu.bind(this);
  }


    componentWillReceiveProps (nextProps) {
        this.setPlan(nextProps)
    }

    componentDidMount () {
        this.setPlan(this.props)
    }

    onClickMap(t, map, coord) {
    const { latLng } = coord;
    const lat = latLng.lat();
    const lng = latLng.lng();

    this.setState( {
      marker: 
        {
        position: { lat, lng }
        },
      newLati: lat,
      newLongi: lng
    });

    $('#inputLati').val(lat);
    $('#inputLongi').val(lng);  
    }

    setPlan(u){

    var idUser = u.match.params.id;
    var idPlan = u.match.params.idP;
    
    var that = this;
    that.setState({idUserOfPlan: idUser})
    that.setState({idOfPlan: idPlan})
    

        fetch('http://localhost:8080/users/'+idUser+'/plans/'+idPlan)
        .then(response => response.json())
        .then(function(data){

          that.setState({namePlan: data.namePlan})
          that.setState({transport: data.transpCar})
          that.setState({horizon: data.days})
          that.setState({lunches: data.lunch})

          if(data.transpCar === true){
            that.setState({transpV: 'Carro'})
          }
          else if(data.transpCar === false){
            that.setState({transpV: 'A Pé'})
          }

          if(data.lunch === true){
            that.setState({lunchV: 'Sim'})
          }
          else if(data.lunch === false){
            that.setState({lunchV: 'Não'})
          }

        })


        fetch('http://localhost:8080/users/'+idUser+'/plans/'+idPlan+'/workers')
        .then(response => response.json())
        .then(function(data){ 
          that.setState({ listWorkersData: data })
        })   

        fetch('http://localhost:8080/users/'+idUser+'/plans/'+idPlan+'/patients')
        .then(response => response.json())
        .then(function(data){ 
          that.setState({ listPatientsData: data })
        })

        fetch('http://localhost:8080/users/'+idUser+'/plans/'+idPlan+'/vans')
        .then(response => response.json())
        .then(function(data){ 
          that.setState({ listVansData: data })
        });   

    }  

  workersView () {
	this.setState({ view : 1 });
	}

	patientsView() {
	this.setState({ view : 2 });
	}

  vansView() {
  this.setState({ view : 13 });
  }

  editVansView() {
  this.setState({ view : 11 });
  }

	addWorkersView() {
	this.setState({ view : 3 });
	}

	addPatientsView() {
	this.setState({ view : 4 });
	}

  editPlanView(){
  this.setState({ view : 5 });
  }

  removePlanView(){
  this.setState({ view : 6 });
  }

  changeVans(event) {
    const onlyNumber = (event.target.validity.valid) ? event.target.value : this.state.newVans;
    this.setState({ newVans: onlyNumber });
  }

  removePlan(){

    var idUser = this.state.idUserOfPlan;
    var idPlan = this.state.idOfPlan;

    var that = this;

    fetch('http://localhost:8080/users/' + idUser + "/plans/" + idPlan, {
          method: 'DELETE',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          }
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
              that.setState({ view: 9 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });
  }

  newWorkerView(){

    var idUser = this.state.idUserOfPlan;
    var idPlan = this.state.idOfPlan;

    var nameNew = $("#inputNamePatient").val();
    var imageEdit = $("#inputImage").val();
    var contactNew = $("#inputContact").val();
    var emailNew = $("#inputEmail").val();
    var infoNew = $("#inputDescrAdd").val();

    var imageAll = imageEdit.split("fakepath\\");
    imageEdit = imageAll[1];

    var that = this;

    if(nameNew === ""){
      alert("Por Favor Preencha o Nome do Novo Auxiliar")
    }
    else{

    fetch('http://localhost:8080/users/' + idUser + "/plans/" + idPlan + "/workers", {
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          },
            body: JSON.stringify({
            nameWorker: nameNew,
            dirPic: imageEdit,
            workerContact: contactNew,
            emailWorker: emailNew,
            infoWorker: infoNew
          })
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
              that.setState({ view: 7 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });
  }    

  }

  newPatientView(){

    var idUser = this.state.idUserOfPlan;
    var idPlan = this.state.idOfPlan;

    var nameNew = $("#inputNamePatient").val();
      var imageEdit = $("#inputImage").val();
    var contactNew = $("#inputContact").val();
    var emailNew = $("#inputEmail").val();
    var adressNew = $("#inputAdress").val();
    var latiNew = $("#inputLati").val();
    var longiNew = $("#inputLongi").val();
    var infoNew = $("#inputDescrAdd").val();

    var imageAll = imageEdit.split("fakepath\\");
    imageEdit = imageAll[1];

    var that = this;

    if(nameNew === ""){
      alert("Por Favor Preencha o Nome do Novo Paciente")
    }
    else{

    fetch('http://localhost:8080/users/' + idUser + "/plans/" + idPlan + "/patients", {
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          },
            body: JSON.stringify({
            name: nameNew,
            dirPic: imageEdit,
            contact: contactNew,
            email: emailNew,
            adress: adressNew,
            lati: latiNew,
            longi: longiNew,
            info: infoNew
          })
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
              that.setState({ view: 8 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });
    }    

  }

  newVanView(){

    var idUser = this.state.idUserOfPlan;
    var idPlan = this.state.idOfPlan;
    var vansT = this.state.newVans;

    var vansCap = [];

    for(var i=0; i<vansT; i++){
      var idSelect = "newVanCapVal"+i;
      var vv = $( "#newVanCapVal"+i+" option:selected" ).text();
      vansCap.push(vv);
    }

    var found = 0;
    for(var j=0; j<vansCap.length; j++){
      var n = vansCap[j];
      if( !isNaN(n) ){
      }
      else{
        found = 1;
      }
    }

   var that = this;

    if(found === 1 ){  
          alert("Por Favor Preencha Todos os Campos")
    }
    else{
      console.log(vansCap)

        fetch('http://localhost:8080/users/' + idUser + "/plans/" + idPlan + "/vans", {
          method: 'PUT',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          },
            body: JSON.stringify(vansCap)
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
              that.setState({ view: 12 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });
    }

  }

  removePlanSuccessView(){
    this.setState({ view: 9 });
  }

  goToUserPage(){
    var idUser = this.state.idUserOfPlan;
    var pathToGo = "/user/" + idUser;

    this.props.history.push({ pathname: pathToGo  })
  }

  changeDays(event) {
    const onlyNumber = (event.target.validity.valid) ? event.target.value : this.state.horizon;
    this.setState({ horizon: onlyNumber });
  }

  editPlanSuccessView(){

    var idUser = this.state.idUserOfPlan;
    var idPlan = this.state.idOfPlan;

    var editName = '';
    var editTrans = '';
    var editDays = '';
    var editLunch = '';

    var nameEdit = $("#inputNameEditPlan").val();
    var transpEdit = $("#inputTransportEditPlan").val();
    var daysEdit = $("#inputDaysEditPlan").val();
    var lunchEdit = $("#lunchInputEditPlan").val();

    if(daysEdit === ''){
      alert("Por Favor Preencha o Campo Relativo ao Número de Dias")
    }

    else{

    if(nameEdit.length > 0){
      editName = nameEdit;
    }
    else{
      editName = this.state.namePlan
    }

    editDays = daysEdit;

    if(transpEdit === 'Carro'){
      editTrans = true;
    }
    else if(transpEdit === 'A Pé'){
      editTrans = false;
    }
    else{
      editTrans = this.state.transport;
    }

    if(lunchEdit === 'Sim'){
      editLunch = true;
    }
    else if(lunchEdit === 'Não'){
      editLunch = false;
    }
    else{
      editLunch = this.state.lunches;
    }

    var that = this;

    fetch('http://localhost:8080/users/' + idUser + "/plans/" + idPlan, {
          method: 'PUT',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          },
            body: JSON.stringify({
            namePlan: editName,
            transpCar: editTrans,
            days: editDays,
            lunch: editLunch
          })
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
              that.setState({ view: 10 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });
    }

  }

  seeAllSolus(){
    this.setState({ seeSolus : 2 });
  }

  clearSolu(){

    var idUser = this.state.idUserOfPlan;
    var idPlan = this.state.idOfPlan;

    var that = this;

    fetch('http://localhost:8080/users/' + idUser + "/plans/" + idPlan + "/solus", {
          method: 'DELETE',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          }
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
              that.setState({ view: 14 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });
  }

  refreshPage(){
  window.location.reload();
  }

  render() {

  const areaView = this.state.view;
  const seeAllSolus = this.state.seeSolus;

    return (
	  <div class="mapsPage">
	  
    <NavigationBar />
	  
    { areaView !== 3 && areaView !== 4 && areaView !== 5 && areaView !== 6 && areaView !== 7 && areaView !== 8 && areaView !== 9 && areaView !== 10 && areaView !== 11 && areaView !== 12 && areaView !== 14 &&

    <PlanInfo 
      namePlan = {this.state.namePlan} 
      transport = {this.state.transpV}
      horizon = {this.state.horizon}
      lunches = {this.state.lunchV}
      editPlan = {this.editPlanView}
      removePlan = {this.removePlanView}
      />
  
    }

    { areaView !== 3 && areaView !== 4 && areaView !== 5 && areaView !== 6 && areaView !== 7 && areaView !== 8 && areaView !== 9 && areaView !== 10 && areaView !== 11 && areaView !== 12 && areaView !== 14 &&

      <WorkersPatientsInfo 
          viewW = {this.state.view}
          workersV = {this.workersView}
          patientsV = {this.patientsView}
          vansV = {this.vansView}
          addW = {this.addWorkersView}
          addP = {this.addPatientsView}
          addV = {this.vansView}
          editV = {this.editVansView}
          seeSolus = {this.seeAllSolus}
          dataPatients = {this.state.listPatientsData}
          dataWorkers = {this.state.listWorkersData}
          dataVans = {this.state.listVansData}
          idUser = {this.state.idUserOfPlan}
          idPlan = {this.state.idOfPlan}
      />


    }

    { areaView === 3 &&
      
       <AddWorker
        defaultV = {this.workersView}
        newWorkerV = {this.newWorkerView}

      />
    }


    { areaView === 4 &&
      
       <AddPatient
        defaultV = {this.workersView}
        newPatientV = {this.newPatientView}
        mark = {this.state.marker}
        onClickM = {this.onClickMap}
        g = {this.props.google}
      />
    }

    { areaView === 11 &&
      
       <EditVans
        defaultV = {this.workersView}
        newVanV = {this.newVanView}
        newV = {this.state.newVans}
        changeV = {this.changeVans}
      />
    }

    { areaView === 12 &&
    
    <VansSuccess
        refresh = {this.refreshPage}
    />

    }

    { areaView === 5 &&
      
       <EditPlan
        nameP = {this.state.namePlan} 
        transP = {this.state.transport}
        daysP = {this.state.horizon}
        lunchesP = {this.state.lunches}
        defaultV = {this.workersView}
        editPlanV = {this.editPlanSuccessView}
        changeD = {this.changeDays}
        />
    }


    { areaView === 6 &&
      
       <RemovePlan
        removePlanV = {this.removePlanSuccessView}
        removePlan = {this.removePlan}
        defaultV = {this.workersView}
        />
    }

    { areaView === 7 &&

      <WorkerSuccess
            refresh = {this.refreshPage}
      />

    }

    { areaView === 8 &&
    
    <PatientSuccess
        refresh = {this.refreshPage}
    />

    }

    { areaView === 9 &&

    <RemovePlanSuccess
      goToUserP = {this.goToUserPage}
    />
    
    }
    
    }

    { areaView === 10 &&
      <EditPlanSuccess
      refresh = {this.refreshPage}
      />
    }

    { seeAllSolus === 2 && areaView !== 14 &&
        <MapPlan 
          idUser = {this.state.idUserOfPlan}
          idPlan = {this.state.idOfPlan}
          removeSolu = {this.clearSolu}
        />
    }

    { areaView === 14 &&

    <RemoveSoluSuccess
      refresh = {this.refreshPage}
    />

    }

    <Footer/>

	  </div>
    );
  }
}


class PlanInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {

    return (   
	
	<div class="planInfo">     
	<div class="container">
	<div class="row">
        <div class="col-md-12">
            <div class="panel with-nav-tabs panel-danger" id="panelPlan">
                <div class="panel-headingPlan">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1danger" data-toggle="tab">Nome</a></li>
                            <li><a href="#tab2danger" data-toggle="tab">Transporte</a></li>
                            <li><a href="#tab3danger" data-toggle="tab">Dias</a></li>
                            <li><a href="#tab4danger" data-toggle="tab">Almoços</a></li>
                            <li><a href="#tab5danger" data-toggle="tab">Opções</a></li>
                        </ul>
                </div>
                <div class="panel-body" id="panelBodyPlan">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1danger">{this.props.namePlan}</div>
                        <div class="tab-pane fade" id="tab2danger">{this.props.transport}</div>
                        <div class="tab-pane fade" id="tab3danger">{this.props.horizon}</div>
                        <div class="tab-pane fade" id="tab4danger">{this.props.lunches}</div>
                        <div class="tab-pane fade" id="tab5danger">  
                        <a href="#" onClick={this.props.editPlan} id="aEdit"><span class="icon-wrench"></span>Editar</a>
                        |
                        <a href="#" onClick={this.props.removePlan} id="aRemove"><span class="icon-wrench"></span>Remover</a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    
	</div>
	</div>
	</div>
	
	);
  }
  
}


class WorkersPatientsInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {


	var addButton = [<button type="button" id="addWorkerButton" class="btn btn-danger" onClick={this.props.addW}><span class="glyphicon glyphicon-plus"></span>Adicionar Auxiliar</button>];
					
	var addButton2 = [<button type="button" id="addPatientButton" class="btn btn-danger" onClick={this.props.addP}><span class="glyphicon glyphicon-plus"></span>Adicionar Paciente</button>];

  var addButton5 = [<button type="button" id="addVanButton" class="btn btn-danger" onClick={this.props.editV}><span class="glyphicon glyphicon-plus"></span>Editar Veículos</button>];


  var idU = this.props.idUser;
  var idP = this.props.idPlan;


  var addButton3 = [<div id="divGenerateButton">
                    <Link to={{ pathname: "/user/" + idU + "/plan/" + idP + "/generate" }} id="generatePlanButton" class="btn btn-success">
                      Gerar Solução 
                    </Link>
                    </div>
                    ];

  var addButton4 = [<div id="divGenerateButton">
                    <button type="button" id="generatePlanButton" class="btn btn-success" onClick={this.props.seeSolus}>Ver Solução</button>
                    </div>
                    ];


    const step = this.props.viewW;
	  var showComponent = null;

    var workersLinks = [];
    
    for (let i = 0; i < this.props.dataWorkers.length ; i++) {

      var path = "/user/" + this.props.idUser + "/plan/" + this.props.idPlan + "/worker/" + this.props.dataWorkers[i].workerId;
      workersLinks.push(<Link to = {{ pathname: path }}><h4 class="section-heading">{this.props.dataWorkers[i].nameWorker}</h4></Link>)

    }


    var patientsLinks = [];
    
    for (let i = 0; i < this.props.dataPatients.length ; i++) {

      var path = "/user/" + this.props.idUser + "/plan/" + this.props.idPlan + "/patient/" + this.props.dataPatients[i].idpatient;
      patientsLinks.push(<Link to = {{ pathname: path }}><h4 class="section-heading">{this.props.dataPatients[i].name}</h4></Link>)

    }

    
    var vansLinks = [];

    for(let i = 0; i < this.props.dataVans.length; i++){
      var van = "Veículo "+(i+1);
      var cap = " --- Capacidade: "+this.props.dataVans[i].capacity;
      vansLinks.push(<h4 class="section-heading">{van}{cap}</h4>)
    }
	  
	  if(step === 1){
		  showComponent = [		  <section id = "listWorkers">
        									     <h2>Auxiliares</h2>
        									     {workersLinks}
        								       {addButton}
        								    </section>		  
						];
	  }

	  else if(step === 2){
		  showComponent = [		  <section id = "listPatients">
									             <h2>Pacientes</h2>
                              {patientsLinks}
            								  {addButton2}
								            </section>
						];
	  }

    else if(step === 13){
      showComponent = [     <section id = "listVans">
                               <h2>Veículos</h2>
                              {vansLinks}
                              {addButton5}
                            </section>
            ];
    }

    return (   
	
	<div class="workersPatientsInfo">     

  	<div class="container">
  		<div class="row" id="firstRowPlan">

	      <div class="col-sm-8" id="colWPPlan">
	        {showComponent}
	      </div>

	      <div class="col-sm-4" id="colTabsPlan">
	       <section id = "sectionTab">
	        <div class="tab">
	          <input class="tablinks" onClick={this.props.workersV} type="submit" value="Auxiliares"/>

	          <input class="tablinks" onClick={this.props.patientsV} type="submit" value="Pacientes"/>

            <input class="tablinks" onClick={this.props.vansV} type="submit" value="Veículos"/>
	        </div>
	        </section>
	      </div>

  		</div>

      <div class="row" id="secondRow">
	     <div class="col-sm-6">
	     	{addButton3}
	     </div>
	      <div class="col-sm-6">
	     	{addButton4}
	     </div>
      </div>

  	</div>

	</div>
	
	);
  }
  
}


class AddWorker extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {
    return (   
	
	<section id="addWorker">     

    <form id="form-signin">
        <input type="text" id="inputNamePatient" class="form-control" placeholder="Nome" />
        <input type="file" id="inputImage" class="form-control" placeholder="Seleccione Imagem"/>
        <input type="number" id="inputContact" class="form-control" placeholder="Contacto"/>
        <input type="text" id="inputEmail" class="form-control" placeholder="E-mail" />
       
        <textarea id="inputDescrAdd" class="form-control" placeholder="Informação Adicional" />
        
        <button type="button" id="addNewWorkerButton" onClick={ this.props.defaultV } class="btn btn-danger">Cancelar</button>
		    <button type="button" id="addNewWorkerButton2" onClick={ this.props.newWorkerV } class="btn btn-success">Adicionar Auxiliar</button>     
    </form>

	</section>
	
	);
  }
  
}


class AddPatient extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {
    return (   
	
    <div>
    <div class="google-maps">
            <Map
            google={this.props.g}
            className={"map"}
            initialCenter={this.props.mark.position}
            zoom={13}
            onClick={this.props.onClickM}
          >
          
          <Marker
              name = {this.props.mark.name}
              position={this.props.mark.position}
          />
          </Map>
    </div>
    <div id="addWorker">   

    <form id="form-signin">
        <input type="text" id="inputNamePatient" class="form-control" placeholder="Nome" />
        
        <input type="file" id="inputImage" class="form-control" placeholder="Seleccione Imagem"/>
        <input type="number" id="inputContact" class="form-control" placeholder="Contacto"/>
        <input type="text" id="inputEmail" class="form-control" placeholder="E-mail" />
        <input type="text" id="inputAdress" class="form-control" placeholder="Morada" />

        <div class="input-group" id="inputCoordinates">
          <input type="text" id="inputLati" class="form-control" placeholder="Latitude" />
          <span class="input-group-addon">-</span>
          <input type="text" id="inputLongi" class="form-control" placeholder="Longitude" />
        </div>
       
        <textarea id="inputDescrAdd" class="form-control" placeholder="Informação Adicional" />
        
        <button type="button" id="addNewWorkerButton" onClick={ this.props.defaultV } class="btn btn-danger">Cancelar</button>
        <button type="button" id="addNewWorkerButton2" onClick={ this.props.newPatientV } class="btn btn-success">Adicionar Paciente</button>     

    </form>

	  </div>
    </div>
	
	);
  }
  
}

class EditVans extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {

    var capacityVans = [];
    var totalV = this.props.newV;

    for(var i = 0; i < totalV ; i++){
      var idVanC = "newVanCapVal"+i;
      var textVanC = "Seleccione os Lugares Disponíveis do Veículo "+(i+1)+"  ";
      capacityVans.push(
    
      <select id={idVanC} name="state" class="form-control selectpicker" >
        <option value=" " >{textVanC}</option>
        <option value="1">1</option> <option>2</option> <option>3</option> <option>4</option> <option>5</option>
        <option>6</option> <option>7</option> <option>8</option> <option>9</option> <option>10</option>
        <option>11</option> <option>12</option> <option>13</option> <option>14</option> <option>15</option>
        <option>16</option> <option>17</option> <option>18</option> <option>19</option> <option>20</option>
        <option>21</option> <option>22</option> <option>23</option> <option>24</option> <option>25</option>
        <option>26</option> <option>27</option> <option>28</option> <option>29</option> <option>30</option>
        <option>31</option> <option>32</option> <option>33</option> <option>34</option> <option>35</option>
        <option>36</option> <option>37</option> <option>38</option> <option>39</option> <option>40</option>
        <option>41</option> <option>42</option> <option>43</option> <option>44</option> <option>45</option>
        <option>46</option> <option>47</option> <option>48</option> <option>49</option> <option>50</option>
        </select>
    )
    }

    return (   
  
  <section id="addVans">   

    <form id="form-signin">
    
    <input class="form-control" id="quiz1Input222" type="text" pattern="[0-9]*" placeholder ="Insira o Número de Veículos" value={this.props.newV} onChange={this.props.changeV}></input>
    
    {capacityVans}
        
    <button type="button" id="addNewWorkerButton" onClick={ this.props.defaultV } class="btn btn-danger">Cancelar</button>
    <button type="button" id="addNewWorkerButton2" onClick={ this.props.newVanV } class="btn btn-success">Editar Veículos</button>     

    </form>

  </section>
  
  );
  }
  
}


class EditPlan extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  
  render() {

    const step = this.props.transP;
    var showComponent = null;
    if(step === true){
      showComponent = [<select class="form-control selectpicker show-tick" id="inputTransportEditPlan">
                        <option>Modo de Transporte</option>
                        <option>A Pé</option>
                        <option selected>Carro</option>
                      </select>];
    }
    else{
      showComponent = [<select class="form-control selectpicker show-tick" id="inputTransportEditPlan">
                        <option>Modo de Transporte</option>
                        <option selected>A Pé</option>
                        <option>Carro</option>
                      </select>];
    }

    const step2 = this.props.lunchesP;
    var showComponent2 = null;
    if(step2 === true){
      showComponent2 = [<select class="form-control selectpicker show-tick" id="lunchInputEditPlan">
                          <option>Almoços</option>
                          <option>Não</option>
                          <option selected>Sim</option>
                        </select>];
    }
    else{
      showComponent2 = [<select class="form-control selectpicker show-tick" id="lunchInputEditPlan">
                          <option>Almoços</option>
                          <option selected>Não</option>
                          <option>Sim</option>
                        </select>];
    }

    return (   
  

  <div class="row">
  <section id="editPlan">     

    <form id="form-signin">
        <input type="text" id="inputNameEditPlan" class="form-control" placeholder={this.props.nameP} />

          {showComponent}
  
        <input class="form-control" id="inputDaysEditPlan" type="text" pattern="[0-9]*" value={this.props.daysP} onChange={this.props.changeD}></input>

          {showComponent2}
         
        <button type="button" id="editPlanButton" onClick={ this.props.defaultV } class="btn btn-danger">Cancelar</button>
        <button type="button" id="editPlanButton2" onClick={ this.props.editPlanV } class="btn btn-success">Editar</button>     
    </form>

  </section>
  </div>

  );
  }
  
}


class RemovePlan extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {
    return (   

  <div class="row">
  <section id = "removePatient">   

    <h1>Tem a certeza que pretende remover este plano?</h1>
        
    <button type="button" id="removeWorkerButton" onClick={ this.props.defaultV } class="btn btn-danger">Não</button>
    <button type="button" id="removeWorkerButton2" onClick={ this.props.removePlan } class="btn btn-success">Sim</button>     

  </section>
  </div>
  
  );
  }
  
}


class WorkerSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>O Auxiliar Foi Adicionado Com Sucesso!</h3>

    <button type="button" onClick={this.props.refresh} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
    </div>

      );
  }
  
}

class PatientSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>O Paciente Foi Adicionado Com Sucesso!</h3>

    <button type="button" onClick={this.props.refresh} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
    </div>

      );
  }
  
}

class VansSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>Os Veículos Foram Editados Com Sucesso!</h3>

    <button type="button" onClick={this.props.refresh} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
    </div>

      );
  }
  
}

class RemovePlanSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>O Plano Foi Removido Com Sucesso!</h3>

    <button type="button" onClick={this.props.goToUserP} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
  </div>

      );
  }
  
}

class EditPlanSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>O Plano Foi Editado Com Sucesso!</h3>

    <button type="button" onClick={this.props.refresh} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
  </div>

      );
  }
  
}

class MapPlan extends Component {
  constructor(props) {
    super(props)
    this.state = {
    timeURL : "totalTime.png",
    waitURL : "waitTime.png",
    fairURL : "balanceTime.png"
    }
  }
  
  render() {

  var removeSoluButton = [<button type="button" id="clearSoluButton" onClick={ this.props.removeSolu } className="btn btn-success">Descartar Soluções</button>];

  var idU = this.props.idUser;
  var idP = this.props.idPlan;
  var pathname1 = "/user/"+idU+"/plan/"+idP+"/solu/1";
  var pathname2 = "/user/"+idU+"/plan/"+idP+"/solu/2";
  var pathname3 = "/user/"+idU+"/plan/"+idP+"/solu/3";
    
    return (
  
  <section id="mapPlan">
  
  <div className="container">
  
  <div className="gall col-lg-12 text-center">
    <h2 className="section-heading text-uppercase">Escolha Uma Solução</h2>
    </div>
    
  <div className="row">
  
  <div className="col-md-4" id="colId1">

    <Link to={{ pathname: pathname1}}>
      <a className="thumbnail">   
        <img className="img-responsive" alt="Fairness" src={require(`../imgs/${this.state.timeURL}`)} />
      </a>
    </Link>
    <div className="img-content">
      <h2>Minimização do Tempo Total de Trabalho</h2>
    </div>

  </div>
  
  <div className="col-md-4" id="colId2">

    <Link to={{ pathname: pathname2}}>
      <a className="thumbnail">   
        <img className="img-responsive" alt="Fairness" src={require(`../imgs/${this.state.waitURL}`)} />
      </a>
    </Link>
    <div className="img-content">
      <h2>Minimização do Tempo Total de Espera Entre Tarefas</h2>
    </div>

  </div>
  
  <div className="col-md-4" id="colId3">

    <Link to={{ pathname: pathname3}}>
      <a className="thumbnail">   
        <img className="img-responsive" alt="Fairness" src={require(`../imgs/${this.state.fairURL}`)} />
      </a>
    </Link>
    <div className="img-content">
      <h2>Maximização da Justiça do Tempo Total de Trabalho</h2>
    </div>

  </div>
  
  </div>
  
  </div>
      {removeSoluButton}
  </section>

      );
  }
   
}

class RemoveSoluSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>As Soluções Foram Descartadas Com Sucesso!</h3>

    <button type="button" onClick={this.props.refresh} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
  </div>

      );
  }
  
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyBxc6paFHyMgKf_uvLL0aexnLGM6BA_J60"
})(PlanPage);