import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';
import NavigationBar from '../general/NavigationBar.js';
import Footer from '../general/Footer.js';

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      inputIdLogin: "",
      inputPasswordLogin: ""
    }
 
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange({target}){    
    this.setState({
      [target.id]:target.value
    });
  }
  
  handleSubmit(e){
    let self = this

    e.preventDefault();

    var userLogin = this.state.inputIdLogin
    var passLogin = this.state.inputPasswordLogin

    if(userLogin === "" || passLogin === ""){  
      alert("Por Favor Preencha Todos os Campos")
    }
    
    else{

      let url = 'http://localhost:8080/users/auth/'+ userLogin +'/'+ passLogin

      fetch(url).then((response) => response.json())
      .then(function(data){

        if(data.userName === null || data.userName === undefined){

        alert("O Nome de Utilizador ou a Password São Inválidos. Por Favor Tente Novamente.")

        }
        else{

        localStorage.setItem("userID", data.userID)
        self.props.history.push({ pathname: "/user/" + data.userID })

        }  
      });
    }
  

  }


  render() {
    return (

    <div class="loginPage">
  
    <div class="container">

        <NavigationBar />

          <form onSubmit={this.handleSubmit} id="form-signin"> 
            
            <h2 class="form-signin-heading">Por Favor Efectue Login</h2>
            
            <input type="text" class="form-control" id="inputIdLogin" placeholder="Nome de Utilizador" onChange={this.handleChange}/>
            
            <input type="password" class="form-control" id="inputPasswordLogin" placeholder="Password" onChange={this.handleChange}/>
                     
			      <button type="submit" id="loginButton" class="btn btn-lg btn-danger btn-block">Login</button>
     
          </form>
        
        <Footer />

    </div>

    </div>
    );
  }

}