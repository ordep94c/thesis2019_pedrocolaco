import React, { Component } from "react";
import { Link } from 'react-router-dom';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import NavigationBar from '../general/NavigationBar.js';
import Footer from '../general/Footer.js';
import $ from 'jquery';

export class PatientPage extends Component {
  constructor(props) {
    super(props);
    
	this.state = {

	  patient: {},

    defaultImg:'defaultP.jpg',
    currentImg: '',

	  name: '',
	  lati: '',
	  longi: '',
	  contact: '',
	  email: '',
	  address: '',
	  descr: '',

	  idUser: '',
	  idPlan: '',
	  idPatient: '',

	  tasks: [],

	  view: 1,

    newLati: '',
    newLongi: '',
	  marker:
        {
          name: "Lisbon",
          position: { lat: 38.713533, lng: -9.238069}
        }

    }

  this.onClickMap = this.onClickMap.bind(this);
  this.historyView = this.historyView.bind(this);
  this.tasksView = this.tasksView.bind(this);
 	this.editView = this.editView.bind(this);
 	this.removeView = this.removeView.bind(this);
 	this.addTaskView = this.addTaskView.bind(this);
 	this.editSuccessView = this.editSuccessView.bind(this);
 	this.removeSuccessView = this.removeSuccessView.bind(this);
 	this.goToPlanPage = this.goToPlanPage.bind(this);
 	this.addTaskSuccessView = this.addTaskSuccessView.bind(this);
 	this.refreshPage = this.refreshPage.bind(this);
  }
    
    componentWillReceiveProps (nextProps) {
	    this.setPatient(nextProps)
	}

	componentWillMount () {
	    this.setPatient(this.props)
	}

	setPatient(p){

    var searchIDUser = p.match.params.id;
    var searchIDPlan = p.match.params.idP;
    var searchIDPatient = p.match.params.idD;
    
    var that = this;

    that.setState({idUser: searchIDUser})
    that.setState({idPlan: searchIDPlan})
    that.setState({idPatient: searchIDPatient})

        fetch('http://localhost:8080/users/'+searchIDUser+'/plans/'+searchIDPlan+'/patients/'+searchIDPatient)
        .then(response => response.json())
        .then(function(data){
          that.setState({patient: data})
          that.setState({ name: data.name })
          that.setState({ currentImg: data.dirPic })
          that.setState({ lati: data.lati })
          that.setState({ longi: data.longi })
          that.setState({ contact: data.contact })
          that.setState({ email: data.email })
          that.setState({ address: data.adress })
          that.setState({ descr: data.info })
        })     
  	

  		fetch('http://localhost:8080/users/'+searchIDUser+'/plans/'+searchIDPlan+'/patients/'+searchIDPatient+"/tasks")
        .then(response => response.json())
        .then(function(data){
          that.setState({tasks: data})
        })

  	}

    historyView () {
	this.setState({ view : 1 });
	}

	tasksView() {
	this.setState({ view : 2 });
	}

	editView() {
	this.setState({ view : 3 });
	}

	removeView() {
	this.setState({ view : 4 });
	}

	addTaskView(){
		this.setState({ view : 5 });
	}

	editSuccessView(){

	var idUser = this.state.idUser;
    var idPlan = this.state.idPlan;
    var idPatient = this.state.idPatient;

    var editName = '';
    var editImage = '';
    var editContact = '';
    var editEmail = '';
    var editAdress = '';
    var editLati = '';
    var editLongi = '';
    var editInfo = '';

    var nameEdit = $("#inputNamePatient").val();
    var imageEdit = $("#inputImage").val();
    var contactEdit = $("#inputContact").val();
    var emailEdit = $("#inputEmail").val();
    var adressEdit = $("#inputAdress").val();
    var latiEdit = $("#inputLati").val();
    var longiEdit = $("#inputLongi").val();
    var infoEdit = $("#inputDescrAdd").val();

    if(nameEdit.length > 0){
      editName = nameEdit;
    }
    else{
      editName = this.state.name
    }

    if(imageEdit.length > 0){
    imageEdit = imageEdit.split("fakepath\\");
    editImage = imageEdit[1];      
    }
    else{
      editImage = this.state.currentImg
    }

    if(contactEdit.length > 0){
      editContact = contactEdit;
    }
    else{
      editContact = this.state.contact
    }

    if(emailEdit.length > 0){
      editEmail = emailEdit;
    }
    else{
      editEmail = this.state.email
    }

    if(adressEdit.length > 0){
      editAdress = adressEdit;
    }
    else{
      editAdress = this.state.address
    }

    if(latiEdit.length > 0){
      editLati = latiEdit;
    }
    else{
      editLati = this.state.lati
    }

    if(longiEdit.length > 0){
      editLongi = longiEdit;
    }
    else{
      editLongi = this.state.longi
    }

    if(infoEdit.length > 0){
      editInfo = infoEdit;
    }
    else{
      editInfo = this.state.descr
    }

    var that = this;

    fetch('http://localhost:8080/users/' + idUser + "/plans/" + idPlan + "/patients/" + idPatient, {
          method: 'PUT',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          },
            body: JSON.stringify({
            name: editName,
            dirPic: editImage,
            contact: editContact,
            email: editEmail,
            adress: editAdress,
            infoPatient: editInfo,
            lati: editLati,
            longi: editLongi
          })
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
            	console.log(data)
              that.setState({ view: 6 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });

	}

	refreshPage(){
	  window.location.reload();
	}

	removeSuccessView(){
	
	var idUser = this.state.idUser;
    var idPlan = this.state.idPlan;
    var idPatient = this.state.idPatient;

    var that = this;

    fetch('http://localhost:8080/users/' + idUser + '/plans/' + idPlan + '/patients/' + idPatient, {
          method: 'DELETE',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          }
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
              that.setState({ view: 7 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });

	}

	addTaskSuccessView(){

	var idUser = this.state.idUser;
    var idPlan = this.state.idPlan;
    var idPatient = this.state.idPatient;
    var patient = this.state.patient;

    var that = this;

    var typeNew = $("#addTask1S option:selected" ).text();
    var typeTNew = $("#addTask2S option:selected" ).text();
    var twINew = $("#addTask3S option:selected" ).text();
    var twFNew = $("#addTask4S option:selected" ).text();
    var durNew = $("#addTask5S option:selected" ).text();
    var resizeTWNew = $("#addTask6S option:selected" ).text();
    var dayNew = $("#addTask7S option:selected" ).text();
    var infoNew = $("#addTask8S option:selected" ).text();

    var typeAdd = 0;
    if(typeNew === "1 Auxiliar"){
    	typeAdd = 1;
    }
    else if(typeNew === "2 Auxiliares"){
    	typeAdd = 2;
    }

    var typeTAdd = -1;
    if(typeTNew === "Tarefa Normal"){
    	typeTAdd = 0;
    }
    else if(typeTNew === "Tarefa de Transporte Para o Centro Sem Prioridade"){
    	typeTAdd = 1;
    }
    else if(typeTNew === "Tarefa de Transporte Para o Centro Com Prioridade"){
    	typeTAdd = 2;
    }
    else if(typeTNew === "Tarefa de Transporte Para Casa Sem Prioridade"){
    	typeTAdd = 3;
    }
    else if(typeTNew === "Tarefa de Transporte Para Casa Com Prioridade"){
    	typeTAdd = 4;
    }

    var twIValues = twINew.split(":");
    var twIAdd = parseInt( ( twIValues[0] - 8 ) * 60 ) + parseInt(twIValues[1]); 

    var twFValues = twFNew.split(":");
    var twFAdd = parseInt( ( twFValues[0] - 8 ) * 60 ) + parseInt(twFValues[1]); 

    var durValues = durNew.split(" Minutos");
    var durAdd = durValues[0];

    var resizeTWadd = false;
    if(resizeTWNew === "Sim"){
    	resizeTWadd = true;
    }

    var dayVal = dayNew.split("Dia ");
    var dayAdd = dayVal[1] - 1;


    if(typeNew === "Seleccione o Número de Auxiliares Necessários" || typeTNew === "Seleccione o Tipo de Tarefa" || twINew === "Seleccione a Janela Temporal - Início" ||
    	twFNew === "Seleccione a Janela Temporal - Fim" || durNew === "Seleccione a Duração Prevista" || resizeTWNew === "A Janela Temporal Poderá Ser Alargada?" || dayNew === "Seleccione o Dia No Qual a Tarefa Será Realizada"){
      alert("Por Favor Seleccione Todas as Informações")
    }
    else if(twFAdd < twIAdd){
    	alert("Por Favor Reveja a Janela Temporal Inserida")
    }
    else{


    fetch('http://localhost:8080/users/' + idUser + "/plans/" + idPlan + "/patients/" + idPatient + "/tasks", {
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          },
            body: JSON.stringify({
            typeTask: typeAdd,
            typeTransportTask: typeTAdd,
            iniTWTask: twIAdd,
            finTWTask: twFAdd,
            durationTask: durAdd,
            changeTWTask: resizeTWadd,
            dayTask: dayAdd,
            infoTask: infoNew,
            patientTask: patient
          })
          })
          .then(response => response.json())
          .then(function(data){
            if(data !== null || data !== 'undefined'){
              that.setState({ view: 8 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });
  	}    


	}

	goToPlanPage(){
    this.props.history.push({ pathname: "/user/" + this.state.idUser + "/plan/" + this.state.idPlan })
  	}


	onClickMap(t, map, coord) {
		const { latLng } = coord;
		const lat = latLng.lat();
		const lng = latLng.lng();

		this.setState( {
			marker: 
			  {
				position: { lat, lng }
			  },
			newLati: lat,
			newLongi: lng
		});

    $('#inputLati').val(lat);
    $('#inputLongi').val(lng);
    
    
	}
  
  render() {

  	const areaView = this.state.view;
	  
	var addButton = [<Link to={{ pathname: "/patient" }} class="btn btn-info btn-lg">
                      <span class="glyphicon glyphicon-plus"></span> Add New Task 
                    </Link>];

    return (
	  <div class="workerPage">
	  
	  <NavigationBar />

	  <h1 className="generatePlan1-heading">Paciente</h1>

	{ areaView !== 3 && areaView !== 4 && areaView !== 5 && areaView !== 6 && areaView !== 7 && areaView !== 8 &&

	  <PatientInfo 
			namePatientP = {this.state.name} 
      defaultI = {this.state.defaultImg}
      currentI = {this.state.currentImg}
			latiP = {this.state.lati}
			longiP = {this.state.longi}
      contactP = {this.state.contact}
      emailP = {this.state.email}
			addressP = {this.state.address}
			descrP = {this.state.descr}
			viewW = {this.state.view}
			editV = {this.editView}
			removeV = {this.removeView}
			/>
	}

	{ areaView !== 3 && areaView !== 4 && areaView !== 5 && areaView !== 6 && areaView !== 7 && areaView !== 8 &&
	  <TasksInfo
	  	idUser = {this.state.idUser}
	    idPlan = {this.state.idPlan}
	    idPatient = {this.state.idPatient}
	  	dataTasks = {this.state.tasks}
	    addTask = {this.addTaskView}
		/>
	}
  	

  	{ areaView === 3 &&
    
     <EditPatient
			namePatientP = {this.state.name} 
      contactP = {this.state.contact}
      emailP = {this.state.email}
			addressP = {this.state.address}
			latiP = {this.state.lati}
			longiP = {this.state.longi}
      newLa = {this.state.newLati}
      newLo = {this.state.newLongi}
			descrP = {this.state.descr}
			historyV = {this.historyView}
			editSuccessV = {this.editSuccessView}
      mark = {this.state.marker}
      onClickM = {this.onClickMap}
      g = {this.props.google}
			/>
	}


	{ areaView === 4 &&
    
     <RemovePatient
			historyV = {this.historyView}
			removeSuccessV = {this.removeSuccessView}
			/>
	}

	{ areaView === 5 &&
    
     <AddTask 
     		 patient = {this.state.patient}
		     historyV = {this.historyView}
		     addTaskSuccessV = {this.addTaskSuccessView}
		     />

	}

	{ areaView === 6 &&

	<EditPatientSuccess
		defaultV = {this.historyView}
		refresh = {this.refreshPage}
		/>

	}

	{ areaView === 7 &&

	<RemovePatientSuccess
		planV = {this.goToPlanPage}
		/>

	}

	{ areaView === 8 &&
	
	<AddTaskSuccess
		defaultV = {this.refreshPage}
		/>
	}

	  <Footer/>
	  
	  </div>
    );
  }
}


class PatientInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {


  function importAll(r) {
    let images = {};
    r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
    return images;
  }

  const images = importAll(require.context('../imgs', false, /\.(png|jpe?g|svg)$/));
  
  var profilePic = '';


  if(!this.props.currentI || 0 === this.props.currentI.length){
    profilePic = this.props.defaultI;
  }
  else{
    profilePic = this.props.currentI;
  }

  
  return (   
	
	<div class="patientInfo">     

		<div class="container-fluid span6">
		<div class="row">

		<div class="col-sm-4">
	      	<div id = "workerPageCol1">
			    <img className="img-responsive" src={images[profilePic]} title="profile image" class="img-circle img-profile" alt="Profile Pic" id="imgID" />
	   		</div>
	    </div>

	    <div class="col-sm-6">
	      	<div id = "workerPageCol2">
	            <h1>{this.props.namePatientP}</h1>
	           	<h4>{this.props.contactP}</h4>
	            <h4>{this.props.emailP}</h4>
	            <h4>{this.props.addressP}</h4>
	            <h4>Latitude: {this.props.latiP} - Longitude: {this.props.longiP}</h4>
	            <br/>
	            <h4>{this.props.descrP}</h4>
	    	</div>
	    </div>

	    <div class="col-sm-2">
	   		<div id = "workerPageCol3">
	            <div class="btn-group" id="buttonWorkerOpt">
	                <a class="btn dropdown-toggle btn-danger" data-toggle="dropdown" id="buttonWorkerOpt" href="#">
	                    Opções  
	                    <span class="icon-cog icon-white"></span><span class="caret"></span>
	                </a>
	                <ul class="dropdown-menu">
	                    <li><a href="#" onClick={this.props.editV}><span class="icon-wrench"></span>Editar</a></li>
	                    <li><a href="#" onClick={this.props.removeV}><span class="icon-trash"></span>Remover</a></li>
	                </ul>
	            </div>
	     	</div>
	    </div>

		</div>
		</div>

	</div>
	
	);
  }
  
}


class TasksInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {

  	var addButton = [<button type="button" id="addTaskButton" class="btn btn-danger" onClick={this.props.addTask}><span class="glyphicon glyphicon-plus"></span>Adicionar Tarefa</button>];

					
    const step = this.props.viewW;
    var showComponent = null;

    var tasksLinks = [];
    
    for (let i = 0; i < this.props.dataTasks.length ; i++) {

    var totalMinutesTWi = this.props.dataTasks[i].iniTWTask;
    var hoursTWi = Math.floor(totalMinutesTWi/60) + 8;
    if (hoursTWi.toString().length == 1) {
        hoursTWi = "0" + hoursTWi;
    }
    var minutesTWi = (totalMinutesTWi % 60);
    if (minutesTWi.toString().length == 1) {
        minutesTWi = "0" + minutesTWi;
    }

    var totalMinutesTWf = this.props.dataTasks[i].finTWTask;
    var hoursTWf = Math.floor(totalMinutesTWf/60) + 8;
    if (hoursTWf.toString().length == 1) {
        hoursTWf = "0" + hoursTWf;
    }
    var minutesTWf = (totalMinutesTWf % 60);
    if (minutesTWf.toString().length == 1) {
        minutesTWf = "0" + minutesTWf;
    }

    var showDay = this.props.dataTasks[i].dayTask + 1;
    var path = "/user/" + this.props.idUser + "/plan/" + this.props.idPlan + "/patient/" + this.props.idPatient + "/task/" + this.props.dataTasks[i].idtask;
    tasksLinks.push(<Link to = {{ pathname: path }}><h4>Dia {showDay} | {hoursTWi}:{minutesTWi} - {hoursTWf}:{minutesTWf} | {this.props.dataTasks[i].durationTask} Minutos</h4></Link>)

    }
	  

    return (   
	
	<div class="tasksInfo">     

		<div class="row">

	  		<div class="col-sm-6" id="colW">

				<section id = "listTasks">
					<h1>Tarefas a Realizar</h1>
					{tasksLinks}
					{addButton}
				</section>

			</div>

	  	</div>

	</div>
	
	);
  }
  
}



class EditPatient extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {
    return (   
  
      <div>
      <div class="google-maps">
            <Map
            google={this.props.g}
            className={"map"}
            initialCenter={this.props.mark.position}
            zoom={13}
            onClick={this.props.onClickM}
          >
          
          <Marker
              name = {this.props.mark.name}
              position={this.props.mark.position}
          />
          </Map>
      </div>
      <div class="editWorker">   

        <form id="form-signin">
            <input type="text" id="inputNamePatient" class="form-control" placeholder={this.props.namePatientP} />

            <input type="file" id="inputImage" class="form-control" placeholder="Seleccione Imagem"/>
            <input type="number" id="inputContact" class="form-control" placeholder={this.props.contactP}/>
            <input type="text" id="inputEmail" class="form-control" placeholder={this.props.emailP} />
            <input type="text" id="inputAdress" class="form-control" placeholder={this.props.addressP}  />

    		<div class="input-group" id="inputCoordinates">
    		    <input type="text" id="inputLati" class="form-control" placeholder={this.props.latiP}/>
    		    <span class="input-group-addon">-</span>
    		    <input type="text" id="inputLongi" class="form-control" placeholder={this.props.longiP} />
    		</div>

        <textarea id="inputDescrAdd" class="form-control" placeholder={this.props.descrP}  /> 
        
        <button type="button" id="editWorkerButton" onClick={ this.props.historyV } class="btn btn-danger">Cancelar</button>
    		<button type="button" id="editWorkerButton2" onClick={ this.props.editSuccessV } class="btn btn-success">Editar</button>     
        </form>

	    </div>
    </div>
	
	);
  }
  
}

class RemovePatient extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {
    return (   
	
	<div class="row">
  	<section id = "removePatient">    

	<h1>Tem a certeza que pretende remover este paciente do plano?</h1>
        
    <button type="button" id="removeWorkerButton" onClick={ this.props.historyV } class="btn btn-danger">Não</button>
	<button type="button" id="removeWorkerButton2" onClick={ this.props.removeSuccessV } class="btn btn-success">Sim</button>     

	</section>
	</div>
	
	);
  }
  
}


class AddTask extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {

    var daysOptions = [];
    
    for (let i = 1; i <= this.props.patient.patientPlan.days ; i++) {
    	daysOptions.push(<option>Dia {i}</option>)
    }

    return (   
	
	<div class="addTaskClass">     

    <form id="form-signin">

    	<div id="addTask1" class="container text-center">
		  <select id="addTask1S" class="form-control selectpicker show-tick">
		    <option selected>Seleccione o Número de Auxiliares Necessários</option>
		    <option>1 Auxiliar</option>
		    <option>2 Auxiliares</option>
		  </select>
		</div>

		<div id="addTask2" class="container text-center">
		  <select id="addTask2S" class="form-control selectpicker show-tick">
		    <option selected>Seleccione o Tipo de Tarefa</option>
		    <option>Tarefa Normal</option>
		    <option>Tarefa de Transporte Para o Centro Sem Prioridade</option>
		    <option>Tarefa de Transporte Para o Centro Com Prioridade</option>
		    <option>Tarefa de Transporte Para Casa Sem Prioridade</option>
		    <option>Tarefa de Transporte Para Casa Com Prioridade</option>
		  </select>
		</div>

		<div id="addTask3" class="container text-center">
		  <select id="addTask3S" class="form-control selectpicker show-tick">
		  	<option selected>Seleccione a Janela Temporal - Início</option>
		    <option>8:00</option>
		    <option>8:30</option>
		    <option>9:00</option>
		    <option>9:30</option>
		    <option>10:00</option>
		    <option>10:30</option>
		    <option>11:00</option>
		    <option>11:30</option>
		    <option>12:00</option>
		    <option>12:30</option>
		    <option>12:00</option>
		    <option>12:30</option>
		    <option>13:00</option>
		    <option>13:30</option>
		    <option>14:00</option>
		    <option>14:30</option>
		    <option>15:00</option>
		    <option>15:30</option>
		    <option>16:00</option>
		    <option>16:30</option>
		    <option>17:00</option>
		    <option>17:30</option>
		    <option>18:00</option>
		    <option>18:30</option>
		    <option>19:00</option>
		    <option>19:30</option>
		    <option>20:00</option>
		  </select>
		</div>

		<div id="addTask4" class="container text-center">
		  <select id="addTask4S" class="form-control selectpicker show-tick">
		  	<option selected>Seleccione a Janela Temporal - Fim</option>
		    <option>8:00</option>
		    <option>8:30</option>
		    <option>9:00</option>
		    <option>9:30</option>
		    <option>10:00</option>
		    <option>10:30</option>
		    <option>11:00</option>
		    <option>11:30</option>
		    <option>12:00</option>
		    <option>12:30</option>
		    <option>12:00</option>
		    <option>12:30</option>
		    <option>13:00</option>
		    <option>13:30</option>
		    <option>14:00</option>
		    <option>14:30</option>
		    <option>15:00</option>
		    <option>15:30</option>
		    <option>16:00</option>
		    <option>16:30</option>
		    <option>17:00</option>
		    <option>17:30</option>
		    <option>18:00</option>
		    <option>18:30</option>
		    <option>19:00</option>
		    <option>19:30</option>
		    <option>20:00</option>
		  </select>
		</div>

		<div id="addTask5" class="container text-center">
		  <select id="addTask5S" class="form-control selectpicker show-tick">
		  	<option selected>Seleccione a Duração Prevista</option>
		    <option>5 Minutos</option>
		    <option>10 Minutos</option>
		    <option>15 Minutos</option>
		    <option>20 Minutos</option>
		    <option>25 Minutos</option>
		    <option>30 Minutos</option>
		    <option>35 Minutos</option>
		    <option>40 Minutos</option>
		    <option>45 Minutos</option>
		    <option>50 Minutos</option>
		    <option>55 Minutos</option>
		    <option>60 Minutos</option>
		    <option>65 Minutos</option>
		    <option>70 Minutos</option>
		    <option>75 Minutos</option>
		    <option>80 Minutos</option>
		    <option>85 Minutos</option>
		    <option>90 Minutos</option>
		    <option>95 Minutos</option>
		    <option>100 Minutos</option>
		    <option>105 Minutos</option>
		    <option>110 Minutos</option>
		    <option>115 Minutos</option>
		    <option>120 Minutos</option>
		  </select>
		</div>

		<div id="addTask6" class="container text-center">
		  <select id="addTask6S" class="form-control selectpicker show-tick">
		  	<option selected>A Janela Temporal Poderá Ser Alargada?</option>
		    <option>Não</option>
		    <option>Sim</option>
		  </select>
		</div>

		<div id="addTask7" class="container text-center">
		  <select id="addTask7S" class="form-control selectpicker show-tick">
		 	<option selected>Seleccione o Dia No Qual a Tarefa Será Realizada</option>
		 	{daysOptions}
		  </select>
		</div>
       
    <textarea id="addTask8" class="form-control" placeholder="Informação Adicional" />
        
        <button type="button" id="addNewWorkerButton" onClick={ this.props.historyV } class="btn btn-danger">Cancelar</button>
		<button type="button" id="addNewWorkerButton2" onClick={ this.props.addTaskSuccessV } class="btn btn-success">Adicionar Tarefa</button>     
    </form>

	</div>
	
	); 
  }
  
}


class EditPatientSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>O Paciente Foi Editado Com Sucesso!</h3>

    <button type="button" onClick={this.props.refresh} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
  	</div>

      );
  }
  
}


class RemovePatientSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>O Paciente Foi Removido do Plano com Sucesso!</h3>

    <button type="button" onClick={this.props.planV} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
  </div>

      );
  }
  
}


class AddTaskSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>A Tarefa Foi Adicionada ao Plano com Sucesso!</h3>

    <button type="button" onClick={this.props.defaultV} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
  </div>

      );
  }
  
}


export default GoogleApiWrapper({
  apiKey: "AIzaSyBxc6paFHyMgKf_uvLL0aexnLGM6BA_J60"
})(PatientPage);