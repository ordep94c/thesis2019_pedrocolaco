import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';
import NavigationBar from '../general/NavigationBar.js';
import Footer from '../general/Footer.js';

export default class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      inputNameRegister: "",
      inputPasswordRegister: "",
      confirmPasswordRegister: "",

      view: 1
    }

    this.goToLoginPage = this.goToLoginPage.bind(this);

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  goToLoginPage(){
    this.props.history.push({ pathname: "/login" })
  }

  handleChange({target}){    
    this.setState({
      [target.id]:target.value
    });
  }
  
  handleSubmit(e){
    let self = this

    e.preventDefault();

    var userReg = this.state.inputNameRegister
    var passReg = this.state.inputPasswordRegister
    var passConf = this.state.confirmPasswordRegister

    if(userReg === "" || passReg === "" || passConf === ""){  
      alert("Por Favor Preencha Todos os Campos")
    }

    else if(passReg !== passConf){
      alert("Por Favor Confirme se os Valores Inseridos nos Campos das Passwords são Iguais")
    }
    
    else{

      let url = 'http://localhost:8080/users/'

      fetch(url, {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Basic'
      },
      body: JSON.stringify({
        userName: userReg,
        password: passReg
      })
      }).then((response) => response.json())
      .then(function(data){
        if(data.userName === null || data.userName === undefined){

        alert("Este nome de Utilizador Já Está Registado. Por Favor Tente Novamente.")

        }
        else{

          self.setState({ view : 2 });

        }  
      });
    }


  
  }


  render() {

    const areaView = this.state.view;

    return (
    
    <div class="registerPage">
  
    <div class="container">
      
    <NavigationBar />

    { areaView === 1 &&
      
      <form onSubmit={this.handleSubmit} id="form-signin">

            <h2 class="form-signin-heading">Por Favor Registe-se</h2>

            <input type="text" class="form-control" id="inputNameRegister" placeholder="Nome de Utilizador" onChange={this.handleChange}/>

            <input type="password" class="form-control" id="inputPasswordRegister" placeholder="Password" onChange={this.handleChange}/>

            <input type="password" class="form-control" id="confirmPasswordRegister" placeholder="Confirmar Password" onChange={this.handleChange}/>

            <button type="submit" id="registerButton" class="btn btn-lg btn-danger btn-block">Registar</button>

      </form>

    }

    { areaView === 2 &&

    <RegisterSuccess
      login = {this.goToLoginPage}
    />

    }
      
    <Footer/>
    
    </div>
    
    </div>
    );
  }

}


class RegisterSuccess extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (   

    <div class="row">
    <section id = "quiz8">

    <i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
    <h1>Já Está!</h1>
    <h3>O Registo Foi Efectuado Com Sucesso!</h3>

    <button type="button" onClick={this.props.login} id="quiz8Button" class="btn btn-success">Continuar</button>

    </section>
  </div>

      );
  }
  
}