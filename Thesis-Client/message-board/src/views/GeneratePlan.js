import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import $ from 'jquery'
import '../App.css';
import NavigationBar from '../general/NavigationBar.js';
import Footer from '../general/Footer.js';

export class GeneratePlan extends Component {
  constructor(props) {
    super(props)
    this.state = {
		step: 1,
		idUserOfPlan: '',
    	idOfPlan: '',

    	newLati: 38.7244,
    	newLongi: -9.22363,
	  	marker:
        {
          name: "Centro APOIO",
          position: { lat: 38.7244, lng: -9.22363}
        }

    }
	this.nextStep = this.nextStep.bind(this);
	this.onClickMap = this.onClickMap.bind(this);
  }

    componentWillReceiveProps (nextProps) {
        this.setPlan(nextProps)
    }

    componentDidMount () {
        this.setPlan(this.props)
    }

    onClickMap(t, map, coord) {
		const { latLng } = coord;
		const lat = latLng.lat();
		const lng = latLng.lng();

		this.setState( {
			marker: 
			  {
				position: { lat, lng }
			  },
			newLati: lat,
			newLongi: lng
		});

    $('#inputLati2').val(lat);
    $('#inputLongi2').val(lng);
    
    
	}

    setPlan(u){

	    var idUser = u.match.params.id;
	    var idPlan = u.match.params.idP;
	    
	    var that = this;
	    that.setState({idUserOfPlan: idUser})
	    that.setState({idOfPlan: idPlan})

	}
    
  
    nextStep() {

    var idUser = this.state.idUserOfPlan;
	var idPlan = this.state.idOfPlan;

   	var editmaxTime = '';
    var editmaxWaitTimeBetweenTasks = '';
    var editlunchTime = '';
    var editsameTeam_samePatient_constraint = '';
    var edittasksTWresize = '';
    var editextensiveSearch = '';

    var lati = $('#inputLati2').val();
    var longi = $('#inputLongi2').val();

    var maxTimeEdit = $("#maximumWorkInput").val();
    var maxWaitTimeBetweenTasksEdit = $("#timeTasksInput").val();
    var lunchTimeEdit = $("#lunchTimeInput").val();
    var sameTeam_samePatient_constraintEdit = $("#fancy-checkbox-danger1").is(':checked');
    var tasksTWresizeEdit = $("#checkedTWInput").val();
    var extensiveSearchEdit = $("#fancy-checkbox-danger3").is(':checked');
    var timeoutEdit = $("#timeoutInput").val();


    editmaxTime = maxTimeEdit*60;

    var lunchValues = lunchTimeEdit.split(":");
    editlunchTime = parseInt( ( lunchValues[0] - 8 ) * 60 ) + parseInt(lunchValues[1]); 


    if(sameTeam_samePatient_constraintEdit === false){
    	editsameTeam_samePatient_constraint = true;
    }
    else if(sameTeam_samePatient_constraintEdit === true){
    	editsameTeam_samePatient_constraint = false;
    }

    if(editmaxTime === '' || maxWaitTimeBetweenTasksEdit === '' || editlunchTime === '' || lati === '' || longi === '' || timeoutEdit === ''){
    	alert("Por Favor Preencha As Informações");
    }
    else{

    var that = this;

    	fetch('http://localhost:8080/users/' + idUser + "/plans/" + idPlan + "/generate", {
          method: 'PUT',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          },
            body: JSON.stringify({
            maxTime: editmaxTime,
            maxWaitTimeBetweenTasks: maxWaitTimeBetweenTasksEdit,
            lunchTime: editlunchTime,
            sameTeam_samePatient_constraint: editsameTeam_samePatient_constraint,
            tasksTWresize: tasksTWresizeEdit,
            extensiveSearch: extensiveSearchEdit,
            timeoutValue: timeoutEdit,
            centerLati: lati,
            centerLongi: longi
          })
          })
          .then(response => response.json())
          .then(function(data){

            if(data !== null || data !== 'undefined'){
              that.setState({ view: 2 });
            }
            else{
              alert("Por Favor Tente Novamente")
            }

          });
    


		this.setState({
	    step : 2
		})
	}

	}


  render() {
	  
	  const step = this.state.step;
	  var showComponent = null;
	  var showComponent2 = null;
	  
	  if(step === 1){
		  showComponent = <SetThings stepPlan = {this.nextStep}
		   							 latiP = {this.state.lati}
									 longiP = {this.state.longi}
      								 newLa = {this.state.newLati}
      								 newLo = {this.state.newLongi}
		        					 mark = {this.state.marker}
      								 onClickM = {this.onClickMap}
      								 g = {this.props.google} />
	  }
	  else if(step === 2){
		  showComponent = <SetThings stepPlan = {this.nextStep} 
		  							 latiP = {this.state.lati}
									 longiP = {this.state.longi}
      								 newLa = {this.state.newLati}
      								 newLo = {this.state.newLongi}
		        					 mark = {this.state.marker}
      								 onClickM = {this.onClickMap}
      								 g = {this.props.google} />
		   
		  showComponent2 = <MapPlan 
		  					idUser = {this.state.idUserOfPlan}
		  					idPlan = {this.state.idOfPlan}
    						/>
	  }
	  
    return (
    
    <div className="generatePlanPage">
      
	<NavigationBar/>
	
	<h1 className="generatePlan1-heading">Obtenha a Sua Solução</h1>
      
	{showComponent}
	
	{showComponent2}
      
    <Footer/>
    
    </div>
    );
  }

}


class SetThings extends Component {
  constructor(props) {
    super(props)
    this.state = {
		checkedTW: false,
		extensiveSearch: false
    }
    this.twCheckboxClick = this.twCheckboxClick.bind(this);
	this.esCheckboxClick = this.esCheckboxClick.bind(this);
  }

  twCheckboxClick() {
    this.setState({checkedTW: !this.state.checkedTW});
  }
  
  esCheckboxClick() {
    this.setState({extensiveSearch: !this.state.extensiveSearch});
  }

  render() {
	  
	  
    return (
       <div>
       <h2 class="headerSelectCenter">Seleccione o Centro Inicial</h2>
       <div class="input-group" id="inputCoordinates2">
    		    <input type="text" id="inputLati2" class="form-control" placeholder="Latitude" value={this.props.newLati}/>
    		    <span class="input-group-addon">-</span>
    		    <input type="text" id="inputLongi2" class="form-control" placeholder="Longitude" value={this.props.newLongi}/>
    	</div>
      <div class="google-maps">
            <Map
            google={this.props.g}
            className={"map"}
            initialCenter={this.props.mark.position}
            zoom={13}
            onClick={this.props.onClickM}
          >
          
          <Marker
              name = {this.props.mark.name}
              position={this.props.mark.position}
          />
          </Map>
      </div>
	<div>
	<section id="generatePlan1">
		
		<div className="container contB">
		
		<div className="row">
		
		<div className="col-md-6">
		<input className="form-control" id="maximumWorkInput" type="text" placeholder ="Insira o Trabalho Diário Máximo (Horas)"></input>
		</div>
		
		<div className="col-md-6">
		<input className="form-control" id="timeTasksInput" type="text" placeholder ="Insira o Tempo Máximo de Espera Entre Tarefas (Minutos)"></input>
		</div>
		
		</div>

		<div className="row">

		<div className="col-md-6">
		<select class="form-control selectpicker show-tick" id="lunchTimeInput">
		    <option selected>Hora de Almoço</option>
		    <option>11:00</option>
		    <option>11:30</option>
		    <option>12:00</option>
		    <option>12:30</option>
		    <option>13:00</option>
		    <option>13:30</option>
		    <option>14:00</option>
		    <option>14:30</option>
		    <option>15:00</option>
		</select>
		</div>

		<div className="col-md-6">
		<input className="form-control" id="timeoutInput" type="text" placeholder ="Timeout (Minutos)"></input>
		</div>

		</div>
		
		<div className="row top-buffer">
		
		<div className="col-md-4">
		<div className="[ form-group ]" id="check1">
            <input type="checkbox" name="fancy-checkbox-danger1" id="fancy-checkbox-danger1" autoComplete="off" />
            <div className="[ btn-group ]" id="btGr1">
                <label htmlFor="fancy-checkbox-danger1" className="[ btn btn-danger ]">
                    <span className="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label htmlFor="fancy-checkbox-danger1" className="[ btn btn-default active ]">
                    Mesmo Paciente - Mesma Equipa
                </label>
            </div>
        </div>
		</div>
		
		<div className="col-md-4">
		<div className="[ form-group ]" id="check2">
            <input onClick={this.twCheckboxClick} type="checkbox" name="fancy-checkbox-danger2" id="fancy-checkbox-danger2" autoComplete="off" />
            <div className="[ btn-group ]" id="btGr2">
                <label htmlFor="fancy-checkbox-danger2" className="[ btn btn-danger ]">
                    <span className="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label htmlFor="fancy-checkbox-danger2" className="[ btn btn-default active ]">
                    Alargar Janelas Temporais
                </label>
            </div>
        </div>

		{this.state.checkedTW === true &&
		<input className="form-control" id="checkedTWInput" type="text" placeholder ="Insira o Valor (Percentagem)"></input>
		}	
		</div>
		
		<div className="col-md-4">
		<div className="[ form-group ]" id="check3">
            <input onClick={this.esCheckboxClick} type="checkbox" name="fancy-checkbox-danger3" id="fancy-checkbox-danger3" autoComplete="off" />
            <div className="[ btn-group ]" id="btGr3">
                <label htmlFor="fancy-checkbox-danger3" className="[ btn btn-danger ]">
                    <span className="[ glyphicon glyphicon-ok ]"></span>
                    <span> </span>
                </label>
                <label htmlFor="fancy-checkbox-danger3" className="[ btn btn-default active ]">
                    Procura Exaustiva
                </label>
            </div>
        </div>

		
	
		
		</div>

		</div>
		
		</div>

		<div className="row">
		<div id="divGenerateButton">
		<button type="button" id="generateSoluButton" onClick={ this.props.stepPlan } className="btn btn-success">Solução</button>
		</div>
		</div>
		
	</section>
	</div>
	</div>
      );
  }
   
}


class MapPlan extends Component {
  constructor(props) {
    super(props)
    this.state = {
		timeURL : "totalTime.png",
		waitURL : "waitTime.png",
		fairURL : "balanceTime.png"
    }
  }
  
  render() {

  	var idU = this.props.idUser;
  	var idP = this.props.idPlan;
	var pathname1 = "/user/"+idU+"/plan/"+idP+"/solu/1";
	var pathname2 = "/user/"+idU+"/plan/"+idP+"/solu/2";
	var pathname3 = "/user/"+idU+"/plan/"+idP+"/solu/3";
	  
    return (
	
	<section id="mapPlan">
	
	<div className="container">
	
	<div className="gall col-lg-12 text-center">
		<h2 className="section-heading text-uppercase">Escolha Uma Solução</h2>
    </div>
		
	<div className="row">
	
	<div className="col-md-4" id="colId1">

		<Link to={{ pathname: pathname1}}>
			<a className="thumbnail">		
				<img className="img-responsive" alt="Fairness" src={require(`../imgs/${this.state.timeURL}`)} />
			</a>
		</Link>
		<div className="img-content">
			<h2>Minimização do Tempo Total de Trabalho</h2>
		</div>

	</div>
	
	<div className="col-md-4" id="colId2">

		<Link to={{ pathname: pathname2}}>
			<a className="thumbnail">		
				<img className="img-responsive" alt="Fairness" src={require(`../imgs/${this.state.waitURL}`)} />
			</a>
		</Link>
		<div className="img-content">
			<h2>Minimização do Tempo Total de Espera Entre Tarefas</h2>
		</div>

	</div>
	
	<div className="col-md-4" id="colId3">

		<Link to={{ pathname: pathname3}}>
			<a className="thumbnail">		
				<img className="img-responsive" alt="Fairness" src={require(`../imgs/${this.state.fairURL}`)} />
			</a>
		</Link>
		<div className="img-content">
			<h2>Maximização da Justiça do Tempo Total de Trabalho</h2>
		</div>

	</div>
	
	</div>
	
	</div>
	   
	</section>
      );
  }
   
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyBxc6paFHyMgKf_uvLL0aexnLGM6BA_J60"
})(GeneratePlan);