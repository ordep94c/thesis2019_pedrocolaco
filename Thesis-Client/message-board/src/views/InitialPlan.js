import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import $ from 'jquery'
import '../App.css';
import NavigationBar from '../general/NavigationBar.js';
import Footer from '../general/Footer.js';

export default class InitialPlan extends Component {
  constructor(props) {
    super(props)
    this.state = {
		step: 1,
		inputNamePlan: "",
		inputDays: "",
		transportCar: true,
		lunch: true,
		vansTotal: "",
		vansCapacity: [],

		searchID: ""
    }

    this.changeName = this.changeName.bind(this);
    this.changeDays = this.changeDays.bind(this);
    this.changeVans = this.changeVans.bind(this);
    this.changeTrans = this.changeTrans.bind(this);
    this.changeLunch = this.changeLunch.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  

    componentWillReceiveProps (nextProps) {
	    this.setUser(nextProps)
	}

	componentDidMount () {
	    this.setUser(this.props)
	}

	setUser(u){
		this.setState({ searchID: u.match.params.id });
	}

    nextStep = () => {
	this.setState({
    step : this.state.step + 1
	})
	}
	
	previousStep = () => {
	this.setState({
    step : this.state.step - 1
	})
	}

	changeName(event) {
	this.setState({ inputNamePlan: event.target.value });
	}

	changeDays(event) {
		const onlyNumber = (event.target.validity.valid) ? event.target.value : this.state.inputDays;
		this.setState({ inputDays: onlyNumber });
	}

	changeVans(event) {
		const onlyNumber = (event.target.validity.valid) ? event.target.value : this.state.vansTotal;
		this.setState({ vansTotal: onlyNumber });
	}

	changeTrans () {
	this.setState({ transportCar: !this.state.transportCar });
	}

	changeLunch () {
	this.setState({ lunch: !this.state.lunch });
	}		

	handleSubmit(e){
		let self = this

		e.preventDefault();

		var namePlan = this.state.inputNamePlan
		var transpCar = this.state.transportCar
		var days = this.state.inputDays
		var lunch = this.state.lunch
		var vans = this.state.vansTotal
		var vansCap = [];

		for(var i=0; i<vans; i++){
			var idSelect = "vanCapVal"+i;
			var vv = $( "#vanCapVal"+i+" option:selected" ).text();
			vansCap.push(vv);
		}

		var found = 0;
		for(var j=0; j<vansCap.length; j++){
			var n = vansCap[j];
			if( !isNaN(n) ){
			}
			else{
				found = 1;
			}
		}

		if(found === 1 || namePlan === "" || days === ""){  
      		alert("Por Favor Preencha Todos os Campos")
    	}

    	else{

	      let url = 'http://localhost:8080/users/'+self.state.searchID+'/plans'

	      fetch(url, {
	        method: 'POST',
	        headers: {
	        'Accept': 'application/json',
	        'Content-Type': 'application/json',
	        'Authorization': 'Basic'
	      },
	      body: JSON.stringify({
	        namePlan: namePlan,
	        transpCar: transpCar,
	        days: days,
	        lunch: lunch,
	        capacityVans: vansCap
	      })
	      }).then((response) => response.json())
	      .then(function(data){
	      	console.log('xxxxxxxxx')
	      	console.log(data)
	        if(data.namePlan === null || data.namePlan === undefined){

	        alert("Por Favor Tente Novamente.")

	        }
	        else{

	          self.setState({ step : 5 });

	        }  
	      });

    	}
	
	}
  
  render() {
	  
	  const step = this.state.step;
	  var showComponent = null;
	  
	  if(step === 1){
		  showComponent = <Plan1 stepPlan = {this.nextStep}
		  						 nameP = {this.state.inputNamePlan} 
		  						 changeN = {this.changeName} />
	  }
	  else if(step === 4){
		  showComponent = <Plan2 addIt = {this.handleSubmit}
								 previousPlan = {this.previousStep} 
								 transCar = {this.state.transportCar}
								 changeT = {this.changeTrans} 
								 vans = {this.state.vansTotal}
								 changeV = {this.changeVans} />
	  }
	  else if(step === 3){
		  showComponent = <Plan3 nextPlan = {this.nextStep}
								 previousPlan = {this.previousStep} 
								 days = {this.state.inputDays} 
		  						 changeD = {this.changeDays} />
	  }

	  else if(step === 2){
		  showComponent = <Plan5 nextPlan = {this.nextStep}
								 previousPlan = {this.previousStep} 
								 lunchYes = {this.state.lunch}
								 changeL = {this.changeLunch} />
	  }

	  else if(step === 5){
		   showComponent = <Plan8 idUser = {this.state.searchID} />
	  }
	  
	  
    return (
    
    <div class="initialPlanPage">
      
    <NavigationBar />
      
	 {showComponent}
      
    <Footer/>
    
    </div>
    );
  }

}


class Plan1 extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {
    return (
	
	<section id="quiz1">
	
		<h1 class="quiz1-heading">Insira um Nome para o seu Plano</h1>
		<input class="form-control" id="quiz1Input" type="text" value={this.props.nameP} onChange={this.props.changeN}></input>
		<button type="button" id="quiz1Button" onClick={ this.props.stepPlan } class="btn btn-success">Começar</button>
	   
	</section>
      );
  }
  

  
}


class Plan2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {

 	const viewL = this.props.transCar;
  	var listG = null;

  	if(viewL){

  		listG = [<ul class="list-group">
					<a href="#" class="list-group-item" onClick={this.props.changeT}>A pé (1km em 10 Minutos)</a>
					<a href="#" class="list-group-item active" onClick={this.props.changeT}>De carro (1km em 2 Minutos)</a>
				</ul>];
  	}
  	else {
  		listG = [
  				<ul class="list-group">
					<a href="#" class="list-group-item active"onClick={this.props.changeT}>A pé (1km em 10 Minutos)</a>
					<a href="#" class="list-group-item" onClick={this.props.changeT}>De carro (1km em 2 Minutos)</a>
				</ul>
				];
  	}

  	var textboxVans = null;
  	if(viewL){
  		textboxVans= [
		<input class="form-control" id="quiz1Input222" type="text" pattern="[0-9]*" placeholder ="Insira o Número de Veículos" value={this.props.vans} onChange={this.props.changeV}></input>
  		]

  	var totalV = this.props.vans;
  	var capacityVans = [];
  	for(var i = 0; i <totalV; i++){
  		var idVanC = "vanCapVal"+i;
  		var textVanC = "Seleccione os Lugares Disponíveis do Veículo "+(i+1)+"  ";
  		capacityVans.push(
		
			<select id={idVanC} name="state" class="form-control selectpicker" >
			  <option value=" " >{textVanC}</option>
			  <option value="1">1</option> <option>2</option> <option>3</option> <option>4</option> <option>5</option>
			  <option>6</option> <option>7</option> <option>8</option> <option>9</option> <option>10</option>
			  <option>11</option> <option>12</option> <option>13</option> <option>14</option> <option>15</option>
			  <option>16</option> <option>17</option> <option>18</option> <option>19</option> <option>20</option>
			  <option>21</option> <option>22</option> <option>23</option> <option>24</option> <option>25</option>
			  <option>26</option> <option>27</option> <option>28</option> <option>29</option> <option>30</option>
			  <option>31</option> <option>32</option> <option>33</option> <option>34</option> <option>35</option>
			  <option>36</option> <option>37</option> <option>38</option> <option>39</option> <option>40</option>
			  <option>41</option> <option>42</option> <option>43</option> <option>44</option> <option>45</option>
			  <option>46</option> <option>47</option> <option>48</option> <option>49</option> <option>50</option>
  			</select>
		)
  	}
  }

    return (   
	<section id="quiz2">
	
		<h1 class="quiz2-heading">Escolha um Modo de Transporte</h1>
		{listG}
		{textboxVans}
		{capacityVans}
		<button type="button" id="quiz2Button" onClick={ this.props.previousPlan } class="btn btn-danger">Anterior</button>
		<button type="button" id="quiz2Button2" onClick={ this.props.addIt } class="btn btn-success">Finalizar</button>
		
	   
	</section>
      );
  }
  
}


class Plan3 extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  
  render() {

    return (   
	<section id="quiz3">
	
		<h1 class="quiz3-heading">Defina o Horizonte de Planeamento</h1>


		<input class="form-control" id="quiz1Input" type="text" pattern="[0-9]*" placeholder ="Insira o Número de Dias" value={this.props.days} onChange={this.props.changeD}></input>

		<button type="button" id="quiz2Button" onClick={ this.props.previousPlan } class="btn btn-danger">Anterior</button>
		<button type="button" id="quiz2Button2" onClick={ this.props.nextPlan } class="btn btn-success">Seguinte</button>
	   
	</section>
      );
  }
  
}


class Plan5 extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {

 	const viewL2 = this.props.lunchYes;
  	var listG2 = null;

  	if(viewL2){

  		listG2 = [<ul class="list-group">
					<a href="#" class="list-group-item" onClick={this.props.changeL}>Não</a>
					<a href="#" class="list-group-item active" onClick={this.props.changeL}>Sim</a>
				</ul>];
  	}
  	else {

  		listG2 = [<ul class="list-group">
					<a href="#" class="list-group-item active"onClick={this.props.changeL}>Não</a>
					<a href="#" class="list-group-item" onClick={this.props.changeL}>Sim</a>
				</ul>];
  	}

    return (   
	<section id="quiz5">
	
		<h1 class="quiz2-heading">Irá Considerar Almoços Para os Seus Auxiliares?</h1>
		{listG2}
		<button type="button" id="quiz2Button" onClick={ this.props.previousPlan } class="btn btn-danger">Anterior</button>
		<button type="button" id="quiz2Button2" onClick={ this.props.nextPlan } class="btn btn-success">Seguinte</button>
	   
	</section>
      );
  }
  
}


class Plan8 extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  render() {

  	var nextPath = "/user/" + this.props.idUser; 

    return (   

    <div class="row">
	  <section id = "quiz8">

		<i class="fa fa-check-circle icon-resize-full" id="iconSuccess"></i>
		<h1>Já Está!</h1>
		<h3>O Seu Plano Foi Criado Com Sucesso!</h3>

		<Link to={{ pathname: nextPath }} id="quiz8Button" class="btn btn-success">Continuar</Link>

	  </section>
	</div>

      );
  }
  
}
