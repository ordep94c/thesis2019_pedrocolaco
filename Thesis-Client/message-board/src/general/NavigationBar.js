import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {withRouter} from 'react-router-dom'
import '../App.css';

class NavigationBar extends Component{
	constructor(props){
		super(props);
		this.state = {
            
        }

    }

    handleOnClick() {
        localStorage.removeItem("userID");

        window.location.assign("/home");
    }
    
	render(){

        var navItems;
        var itemLocal = localStorage.getItem("userID");

        if (itemLocal !== null) {
            navItems = [<li><Link to={{ pathname: "/user/"+itemLocal}}>Planos</Link></li>, 
                        <li><button class="btn btn-lg btn-danger btn-block" type="submit" onClick={this.handleOnClick} id="logoutButton">Logout</button></li>];
        }
        else {
        navItems = [    <li><a href="/login">Login</a></li>, 
                        <li><a href="/register">Registar</a></li>];
        }


        

		return(

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
        
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#navbar1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="navbar1">
                    <ul class="nav navbar-nav" >
                        <li><Link to={{ pathname: "/home"}}>Início</Link></li> 
                        {navItems} 
                    </ul>  
                </div>
            </div>
        </nav>
		);
	}

}

export default withRouter(NavigationBar);
