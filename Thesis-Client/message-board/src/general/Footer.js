import React from 'react';
import '../App.css';

export default class Footer extends React.Component{
	render(){
		return(
		<footer>
		<div id="footer">
			<div className="container">
				<div className="row">
					<div className="col-lg-12">
						<span className ="copyright">Copyright @ SISTEMA DE APOIO À DECISÃO PARA A OTIMIZAÇÃO DO PLANEAMENTO DE SERVIÇOS DE APOIO DOMICILIÁRIO 2018 | Pedro Colaço</span>
					</div>
				</div>
			</div>
		</div>
		</footer>
		);
	}
}