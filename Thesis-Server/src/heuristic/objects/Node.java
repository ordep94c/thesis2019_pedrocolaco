package heuristic.objects;


public class Node {

	private final int idNumber;
	private final int type; //0 -> centro; 1 -> acamado; 2 -> n acamado
	private final int typeTransport; //0 -> noTransport; 1 -> inicioTransporteNaoPrioritario; 2 -> inicioTransportePrioritario; 3 -> fimTransporteNaoPrioritario; 4 -> fimTransportePrioritario  
	private Patient patient;
	private final String name;
	private final double lati;
	private final double longi;
	private double iTW;
	private double fTW;
	private final double dur;
	private Team visitTeam;
	private boolean changeTW; // permite alargar a janela temporal inicial

	private int dayID;

	public Node(int idNumber, int type, int typeTransport, String name, double lati, double longi, double iTW, double fTW, double d,	int dayID) {
		this.idNumber = idNumber;
		this.type = type;
		this.typeTransport = typeTransport;
		this.name = name;
		this.lati = lati;
		this.longi = longi;
		this.iTW = iTW;
		this.fTW = fTW;
		this.dur = d;
		visitTeam = null;
		changeTW = false;

		this.dayID = dayID;
	}	

	public Node(int idNumber, int type, int typeTransport, Patient patient, double d, double e, double dur, boolean changeTW, int dayID) {
		this.idNumber = idNumber;
		this.type = type;
		this.typeTransport = typeTransport;
		this.patient = patient;
		name = patient.getName();
		this.lati = patient.getLati();
		this.longi = patient.getLongi();
		this.iTW = d;
		this.fTW = e;
		this.dur = dur;
		visitTeam = null;
		this.changeTW = changeTW;

		this.dayID = dayID;
	}

	public Node(Node initialNode) {
		this.idNumber = initialNode.idNumber;
		this.type = initialNode.type;
		this.typeTransport = initialNode.typeTransport;
		this.patient = initialNode.patient;
		this.name = initialNode.getName();
		this.lati = initialNode.getLati();
		this.longi = initialNode.getLongi();
		this.iTW = initialNode.iTW;
		this.fTW = initialNode.fTW;
		this.dur = initialNode.dur;
		this.visitTeam = initialNode.visitTeam;
		this.changeTW = initialNode.changeTW;
	}

	public int getID() {
		return idNumber;
	}

	public int getType() {
		return type;
	}

	public int getTypeTransport() {
		return typeTransport;
	}

	public String getName() {
		return name;
	}

	public double getLati() {
		return lati;
	}

	public double getLongi() {
		return longi;
	}

	public double getIniTW() {
		return iTW;
	}

	public double getFinTW() {
		return fTW;
	}

	public double getDuration() {
		return dur;
	}

	public boolean getChangeTW(){
		return changeTW;
	}

	public Patient getPatient(){
		return patient;
	}

	public Team getVisitTeam() {
		return visitTeam;
	}

	public void setVisitTeam(Team newTeam) {
		visitTeam = newTeam;
	}

	public int getDay(){
		return dayID;
	}


	public void resizeTW(int percentage, double morningPeriodStartTime, double afternoonPeriodFinishTime) {

		if(changeTW == true){

			double valueTW = fTW-iTW;
			double valueP = valueTW*percentage;
			double valueP2 = valueP/100;
			double valueResize = valueP2/2;

			iTW = iTW-valueResize;
			fTW = fTW+valueResize;

			if(iTW < morningPeriodStartTime){
				iTW = (int) morningPeriodStartTime;
			}
			if(fTW > afternoonPeriodFinishTime){
				fTW = (int) afternoonPeriodFinishTime;
			}

		}
	}

}
