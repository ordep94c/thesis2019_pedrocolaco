package heuristic.objects;


public class Arc {

	private int idArc;
	private final Node node1;
	private final Node node2;
	private Worker worker;
	private double bestPossibleTime;
	private double waitTime;
	private double extraTime;
	
	private Van van;
	private int capacity;
	
	private boolean car; 		//true -> car (2min/km) | false -> a pé (10min/km)
	private int minutesOneKm;


	public Arc(Node node1, Node node2, Worker worker, boolean carTransp) {
		this.node1 = node1;
		this.node2 = node2;
		this.worker = worker;
		
		minutesOneKm = 2;
		
		if(carTransp == false){
			if(node1.getTypeTransport() == 0){
				if(node2.getTypeTransport() == 0){
					minutesOneKm = 10;
				}
			}
		}

		bestPossibleTime = -1;
		waitTime = 0;
		extraTime = 0;
		setInitialBestTime();
		
		van = null;
		capacity = -1;
	}	
	
	public Arc(Node node1, Node node2, Worker worker, double distanceToCenter, boolean carTransp ) {
		this.node1 = node1;
		this.node2 = node2;
		this.worker = worker;

		minutesOneKm = 2;
		
		if(carTransp == false){
			if(node1.getTypeTransport() == 0){
				if(node2.getTypeTransport() == 0){
					minutesOneKm = 10;
				}
			}
		}
		
		bestPossibleTime = -1;
		waitTime = 0;
		extraTime = 0;
		setInitialBestTimeWithDistanceToCenter(distanceToCenter);
		
		van = null;
		capacity = -1;
	}	
	


	public Arc(Arc a){
		idArc = a.getID();
		node1 = a.getNode1();
		node2 = a.getNode2();
		bestPossibleTime = a.getBestTime();
		waitTime = a.getWaitTime();
		van = a.getVan();
		capacity = a.getCapacityAtTheTime();
	}
	
	public void resetValues(){
		bestPossibleTime = -1;
		waitTime = 0;
		setInitialBestTime();
	}


	public void setID(int id){
		idArc = id;
	}

	public int getID(){
		return idArc;
	}

	public Node getNode1(){
		return node1;
	}

	public Node getNode2(){
		return node2;
	}
	
	public Worker getWorker(){
		return worker;
	}

	public double getDistance(){

		double lat1 = node1.getLati();
		double longi1 = node1.getLongi();
		double lat2 = node2.getLati();
		double longi2 = node2.getLongi();

		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(longi2 - longi1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
				* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters


		distance = Math.pow(distance, 2);

		return Math.sqrt(distance);
	}

	public void setInitialBestTime() {

		
		double minutesTrip = (getDistance()/1000.0) * minutesOneKm; //metros para km. cada km equivale a 2 min de carro
		double bestTime = minutesTrip + getNode1().getIniTW() + getNode1().getDuration();

		if(bestTime < getNode2().getIniTW()){ //se for inferior ao valor da time window, o auxiliar tem de ficar à espera
			bestPossibleTime = getNode2().getIniTW();
		}
		else if( (bestTime >= getNode2().getIniTW()) && (bestTime <= getNode2().getFinTW()) ){ //dentro da time-window
			bestPossibleTime = bestTime;
		}
		else{
			bestPossibleTime = -1; //valor indicativo de que não é possível
		}

	}
	
	private void setInitialBestTimeWithDistanceToCenter(double distanceToCenter) {
		
		double tripComplete = getDistance() + distanceToCenter;
		double minutesTrip = (tripComplete/1000.0) * minutesOneKm; //metros para km. cada km equivale a 2 min de carro
		double bestTime = minutesTrip + getNode1().getIniTW() + getNode1().getDuration();

		if(bestTime < getNode2().getIniTW()){ //se for inferior ao valor da time window, o auxiliar tem de ficar à espera
			bestPossibleTime = getNode2().getIniTW();
		}
		else if( (bestTime >= getNode2().getIniTW()) && (bestTime <= getNode2().getFinTW()) ){ //dentro da time-window
			bestPossibleTime = bestTime;
		}
		else{
			bestPossibleTime = -1; //valor indicativo de que não é possível
		}
		
	}

	public void setNewBestTimeVal(double best, double extra) {
		bestPossibleTime = best;
		extraTime = extra;
		waitTime = waitTime + extraTime;
	}
	
	public void setNewBestTime(double currentTime) {

		
		double minutesTrip = (getDistance()/1000.0) * minutesOneKm;
		double bestTime = minutesTrip + currentTime;

		if(bestTime < getNode2().getIniTW()){ 
			waitTime = (getNode2().getIniTW()) - bestTime;
			bestPossibleTime = getNode2().getIniTW();
		}
		else if( (bestTime >= getNode2().getIniTW()) && (bestTime <= getNode2().getFinTW()) ){ 
			bestPossibleTime = bestTime;
		}
		else{
			bestPossibleTime = -1; 
		}

	}

	public double getBestTime(){
		return bestPossibleTime;
	}

	public double getMinutesTrip(){
		
		double minutesTrip = (getDistance()/1000.0) * minutesOneKm; //metros para km. cada km equivale a 2 min de carro
		return minutesTrip;
	}
	
	public double getWaitTime(){
		return waitTime + extraTime;
	}

	
	public Van getVan(){
		return van;
	}
	
	public int getCapacityAtTheTime(){
		return capacity;
	}
	
	public void setVanAndCapacityAtTheTime(Van v, int c){
		van = v;
		capacity = c;
	}

	public boolean getCar(){
		return car;
	}
}
