package heuristic.objects;
import java.util.ArrayList;
import java.util.List;


public class Team {

	private int idTeam;
	private double currentDayTime;
	private Node initialNode;
	private Node currentNode;
	private boolean hadLunch;
	private double startTime;
	private double finishTime;
	private int maxTime;
	private int periodType;//1 -> manha; 2 -> tarde
	private boolean routeFinished;

	
	private List<Worker> workers;

	public Team(int idTeam, Node currentN, boolean hadLunch, int periodType, double startTime, double finishTime, int maxTime, boolean routeFinished){
		this.idTeam = idTeam;
		currentDayTime = 0;
		initialNode = currentN;
		currentNode = currentN;
		this.hadLunch = hadLunch;
		this.periodType = periodType;
		this.startTime = startTime;
		this.maxTime = maxTime;
		this.finishTime = finishTime;
		this.routeFinished = routeFinished;
		
		workers = new ArrayList<Worker>();
	}
	
	public Team (Team t){
		idTeam = t.getIDTeam();
		currentDayTime = t.getCurrentDayTime();
		initialNode = t.getInitialNode();
		currentNode = t.getCurrentNode();
		hadLunch = t.getHadLunch();
		startTime = t.getStartTime();
		finishTime = t.getFinishTime();
		maxTime = t.getMaxTime();
		periodType = t.getPeriodType();
		routeFinished = t.isRouteFinished();
	}

	public int getIDTeam(){
		return idTeam;
	}

	public boolean getHadLunch(){
		return hadLunch;
	}

	public void setHadLunch(){
		hadLunch = true;
	}

	public int getPeriodType(){
		return periodType;
	}

	public double getStartTime(){
		return startTime;
	}

	public double getFinishTime(){
		return finishTime;
	}


	public double getCurrentDayTime(){
		return currentDayTime;
	}

	public void setCurrentDayTime(double time){
		currentDayTime = time;
	}
	
	public Node getInitialNode(){
		return initialNode;
	}

	public Node getCurrentNode(){
		return currentNode;
	}

	public void setCurrentNode(Node currentN){
		currentNode = currentN;
	}

	public int getMaxTime(){
		return maxTime;
	}

	public void setNewHigherMaxTime(int value){
		maxTime = maxTime + value;
		int val = value/2;

		startTime = startTime-val;
		if(startTime <0){
			startTime = 0;
		}

		finishTime = finishTime+val;
		if(finishTime > 720){
			finishTime = 720;
		}
	}

	public void setNewLowerMaxTime(int value){
		maxTime = maxTime - value;
		int val = value/2;

		startTime = startTime+val;

		finishTime = finishTime-val;

	}

	public boolean isRouteFinished(){
		return routeFinished;
	}

	public void setFinishedRoute(boolean routeF){
		routeFinished = routeF;
	}



	public void addWorker(Worker w){
		workers.add(w);
	}
	
	public int getWorkersSize(){
		return workers.size();
	}
	
	public List<Worker> getWorkers(){
		return workers;
	}

	public Worker getOther(Worker worker) {

		Worker other = null;
		
		for(int i=0;i<workers.size();i++){
			if(workers.get(i).getID() != worker.getID()){
				other = workers.get(i);
			}
		}
		return other;
	}
}
