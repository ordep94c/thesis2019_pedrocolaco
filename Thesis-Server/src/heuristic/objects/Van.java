package heuristic.objects;

public class Van {

	private int idVan;
	private int capacity;
	private boolean active;
	private double currentDayTime;
	private int currentCapacity;

	public Van(int idVan, int capacity, boolean active){
		this.idVan = idVan;
		this.capacity = capacity;
		this.active = active;
		currentDayTime = 0.0;
		currentCapacity = capacity;
	}

	public int getIDvan(){
		return idVan;
	}

	public int getCapacity(){
		return capacity;
	}

	public double getCurrentTime(){
		return currentDayTime;
	}

	public void setCurrentTime(double time){
		currentDayTime = time;
	}

	public int getCurrentCapacity(){
		return currentCapacity;
	}

	public boolean getActive(){
		return active;
	}

	public void setActive(boolean a){
		active = a;
	}

	public void patientEnter(){
		currentCapacity = currentCapacity - 1;
	}

	public void patientLeft(){
		currentCapacity = currentCapacity + 1;	
	}

	public void clearVan(){
		currentCapacity = capacity;
		setActive(false);
	}
	
	public void clearVanBigTime(){
		currentCapacity = capacity;
		currentDayTime = 0.0;
		setActive(false);
	}

}
