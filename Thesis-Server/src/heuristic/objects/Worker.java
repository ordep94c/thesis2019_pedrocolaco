package heuristic.objects;
import java.util.ArrayList;
import java.util.List;


public class Worker {

	private int IDworker;
	private String name;

	private Team team;
	private List<Arc> routeDay;
	private double currentDayTime;
	private Node initialNode;
	private Node currentNode;
	private boolean hadLunch;
	private double startTime;
	private double finishTime;
	private int maxTime;
	private boolean routeFinished;

	private boolean checkWorkerIteration;

	private Van currentVan;

	
	public Worker(int id, String name){
		IDworker = id;
		this.name = name;
	}

	public Worker(Worker w){
		IDworker = w.getID();
		name = w.getName();
		team = w.getTeam();
		routeDay = w.getRouteDay();
		currentDayTime = w.getCurrentDayTime();
		initialNode = w.getInitialNode();
		currentNode = w.getCurrentNode();
		hadLunch = w.getHadLunch();
		startTime = w.getStartTime();
		finishTime = w.getFinishTime();
		maxTime = w.getMaxTime();
		routeFinished = w.isRouteFinished();

		checkWorkerIteration = false;
		currentVan = w.getCurrentVan();
	}

	public int getID(){
		return IDworker;
	}

	public String getName(){
		return name;
	}

	public void setTeam(Team team){
		this.team = team;
		routeDay =  new ArrayList<Arc>();
		currentDayTime = team.getCurrentDayTime();
		initialNode = new Node(team.getInitialNode());
		currentNode = new Node(team.getCurrentNode());
		hadLunch = team.getHadLunch();
		startTime = team.getStartTime();
		finishTime = team.getFinishTime();
		maxTime = team.getMaxTime();
		routeFinished = team.isRouteFinished();

		checkWorkerIteration = false;
		currentVan = null;
	}

	public Team getTeam(){
		return team;
	}

	public boolean getHadLunch(){
		return hadLunch;
	}

	public void setHadLunch(){
		hadLunch = true;
	}

	public double getStartTime(){
		return startTime;
	}

	public double getFinishTime(){
		return finishTime;
	}

	public List<Arc> getRouteDay(){
		return routeDay;
	}

	public List<Node> getNodesVisitedRoute(){

		List<Node> nodesRoute = new ArrayList<Node>();

		for(int i=0;i<routeDay.size();i++){
			Node n = routeDay.get(i).getNode2();
			nodesRoute.add(n);
		}
		return nodesRoute;
	}
	
	public List<Node> getNodesVisitedRouteWithoutCenterNodes(){
		List<Node> nodesRoute2 = new ArrayList<Node>();

		for(int i=0;i<routeDay.size();i++){
			Node n = routeDay.get(i).getNode2();
			if(n.getName().contains("Centro Transporte Inicio")){

			}
			else if(n.getName().contains("Centro Transporte Final")){
				
			}
			else{
				nodesRoute2.add(n);
			}
		
		}
		return nodesRoute2;
	}

	public void addArcToRoute(Arc a){
		routeDay.add(a);
	}

	public void clearRoute(){
		routeDay.clear();
		routeFinished = false;
		hadLunch = false;
		setCurrentNode(initialNode);
		currentDayTime = 0;
	}

	public double getCurrentDayTime(){
		return currentDayTime;
	}

	public void setCurrentDayTime(double time){
		currentDayTime = time;
	}

	public Node getInitialNode(){
		return initialNode;
	}

	public Node getCurrentNode(){
		return currentNode;
	}

	public void setCurrentNode(Node currentN){
		currentNode = currentN;
	}

	public int getMaxTime(){
		return maxTime;
	}

	public void setNewHigherMaxTime(int value){
		maxTime = maxTime + value;
		int val = value/2;

		startTime = startTime-val;
		if(startTime <0){
			startTime = 0;
		}

		finishTime = finishTime+val;
		if(finishTime > 720){
			finishTime = 720;
		}
	}

	public void setNewLowerMaxTime(int value){
		maxTime = maxTime - value;
		int val = value/2;

		startTime = startTime+val;

		finishTime = finishTime-val;

	}

	public boolean isRouteFinished(){
		return routeFinished;
	}

	public void setFinishedRoute(boolean routeF){
		routeFinished = routeF;
	}

	public double timeWorked(){

		if(routeDay.size() > 0){
			double start = routeDay.get(0).getBestTime();
			double finish = (routeDay.get(routeDay.size()-1).getBestTime()) + (routeDay.get(routeDay.size()-1).getNode2().getDuration());

			return (finish-start);
		}

		return 0;
	}


	public double totalWaitTime(){
		double totalWait = 0;

		for(int i=1;i<routeDay.size();i++){
			totalWait = totalWait + routeDay.get(i).getWaitTime();
		}

		return totalWait;
	}


	public boolean getWorkerIterationCheck(){
		return checkWorkerIteration;
	}

	public void setWorkerIterationCheck(boolean check){
		checkWorkerIteration = check;
	}

	public Van getCurrentVan(){
		return currentVan;
	}

	public void setCurrentVan(Van v){
		currentVan = v;
	}

	public void clearVan() {

		Van v = getCurrentVan();
		
		if(v != null){
			getCurrentVan().clearVan();	
			setCurrentVan(null);
		}

	}

}
