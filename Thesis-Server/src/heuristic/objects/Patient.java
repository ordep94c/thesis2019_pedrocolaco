package heuristic.objects;
import java.util.ArrayList;
import java.util.List;


public class Patient {
	
	private int idPatient;
	private String name;
	private final double lati;
	private final double longi;
	private Team team;
	private List<Node> tasks;
	
	
	public Patient(int id, String name, double lati, double longi){
		idPatient = id;
		this.name = name;
		this.lati = lati;
		this.longi = longi;
		tasks = new ArrayList<Node>();
	}
	
	public int getIdPatient(){
		return idPatient;
	}
	
	public String getName(){
		return name;
	}
	
	public Team getTeam(){
		return team;
	}
	
	public double getLati(){
		return lati;
	}
	
	public double getLongi(){
		return longi;
	}
	
	public void setTeam(Team newTeam){
		team = newTeam;
	}
	
	public void addTask(Node task){
		tasks.add(task);
	}
	
	public List<Node> getListNodes(){
		return tasks;
	}

}
