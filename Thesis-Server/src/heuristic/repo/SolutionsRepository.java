package heuristic.repo;

import heuristic.algo.SolutionRoutes;

public class SolutionsRepository {

	SolutionRoutes firstSolution;
	SolutionRoutes totalTimeSolution;
	SolutionRoutes waitingTimeSolution;
	SolutionRoutes fairSolution;

	public SolutionsRepository() {
	
	}
	
	public SolutionsRepository(SolutionRoutes firstSolution){
		setFirstSolution(firstSolution);
		setTotalTimeSolution(firstSolution);
		setWaitingTimeSolution(firstSolution);
		setFairSolution(firstSolution);
	}
	
	public SolutionsRepository(SolutionsRepository repo){	
		this.firstSolution = repo.getFirstSolution();
		this.totalTimeSolution = repo.getTotalTimeSolution();
		this.waitingTimeSolution = repo.getWaitingTimeSolution();
		this.fairSolution = repo.getFairSolution();
	}
	

	public void clearSolutions(){
		firstSolution = null;
		totalTimeSolution = null;
		waitingTimeSolution = null;
		fairSolution = null;
	}

	public void setFirstSolution(SolutionRoutes firstSolution){
		this.firstSolution = firstSolution;
	}

	public SolutionRoutes getFirstSolution(){
		return firstSolution;
	}

	public void setTotalTimeSolution(SolutionRoutes totalTimeSolution){
		this.totalTimeSolution = totalTimeSolution;
	}

	public SolutionRoutes getTotalTimeSolution(){
		return totalTimeSolution;
	}

	public void setWaitingTimeSolution(SolutionRoutes waitingTimeSolution){
		this.waitingTimeSolution = waitingTimeSolution;
	}

	public SolutionRoutes getWaitingTimeSolution(){
		return waitingTimeSolution;
	}

	public void setFairSolution(SolutionRoutes fairSolution){
		this.fairSolution = fairSolution;
	}

	public SolutionRoutes getFairSolution(){
		return fairSolution;
	}

}
