package heuristic.repo;


import java.util.ArrayList;
import java.util.List;

import heuristic.objects.Node;
import heuristic.objects.Patient;

public class NodeRepository {

	ArrayList<Node> allNodes;

	public NodeRepository(){
		allNodes = new ArrayList<Node>();
	}

	public void clearNodes(){
		allNodes.clear();
	}

	public void addNode(int idNumber, int type, int typeTransport, String name, double lati, double longi, double iTW, double fTW, double d, int dayID){
		Node n = new Node(idNumber, type, typeTransport, name, lati, longi, iTW, fTW, d, dayID);
		allNodes.add(idNumber, n);
	}

	public void addTask(int idNumber, int type, int typeTransport, Patient patient, double d, double e, double f, boolean changeTW, int dayID){
		Node n = new Node(idNumber, type, typeTransport, patient, d, e, f, changeTW, dayID);
		allNodes.add(idNumber, n);
	}

	public Node getNode(int IDnumber){
		return allNodes.get(IDnumber);
	}

	public int getSize(){
		return allNodes.size();
	}

	public List<Node> getAll(){
		return allNodes;
	}

	public void resizeNodes(int tasksTWresize, double morningPeriodStartTime, double afternoonPeriodFinishTime){

		for(int i=0;i<allNodes.size();i++){
			allNodes.get(i).resizeTW(tasksTWresize, morningPeriodStartTime, afternoonPeriodFinishTime);
		}

	}

	public ArrayList<Node> getNodesTW(Node node) {

		ArrayList<Node> nodesTWsame = new ArrayList<Node>();

		for(int i=0;i<allNodes.size();i++){
			if( (allNodes.get(i).getIniTW() >= node.getIniTW()) && (allNodes.get(i).getFinTW() <= node.getFinTW()) ){
				if( allNodes.get(i).getID() != node.getID()){
					if(allNodes.get(i).getTypeTransport() == 1){
						Node n = new Node(allNodes.get(i));
						nodesTWsame.add(n);
					}
				}
			}
		}

		return nodesTWsame;
	}

	public ArrayList<Node> getNodesTW2(Node node) {

		ArrayList<Node> nodesTWsame = new ArrayList<Node>();

		for(int i=0;i<allNodes.size();i++){
			if( allNodes.get(i).getIniTW() >= node.getIniTW() && allNodes.get(i).getFinTW() <= node.getFinTW()){
				if( allNodes.get(i).getID() != node.getID()){
					if(allNodes.get(i).getTypeTransport() == 3){
						Node n = new Node(allNodes.get(i));
						nodesTWsame.add(n);
					}
				}
			}
		}

		return nodesTWsame;
	}

	public ArrayList<Node> getNodesTWOpt1(Node node, List<Node> list) {

		ArrayList<Node> nodesTWsame = new ArrayList<Node>();

		for(int i=0;i<list.size();i++){
			if( list.get(i).getIniTW() >= node.getIniTW()){
				if( list.get(i).getID() != node.getID()){
					if(list.get(i).getTypeTransport() == 1){
						Node n = new Node(list.get(i));
						nodesTWsame.add(n);
					}
				}
			}
		}

		return nodesTWsame;
	}

	public ArrayList<Node> getNodesTWOpt2(Node node, List<Node> list) {

		ArrayList<Node> nodesTWsame = new ArrayList<Node>();

		for(int i=0;i<list.size();i++){
			if( list.get(i).getIniTW() >= node.getIniTW() && list.get(i).getFinTW() <= node.getFinTW()){
				if( list.get(i).getID() != node.getID()){
					if(list.get(i).getTypeTransport() == 3){
						Node n = new Node(list.get(i));
						nodesTWsame.add(n);
					}
				}
			}
		}

		return nodesTWsame;

	}

	public void remove(Node node) {
		allNodes.remove(node);	
	}

	public ArrayList<Node> getAllNodesDay(int day) {

		ArrayList<Node> nodesDay = new ArrayList<Node>();

		for(int i=0; i<allNodes.size(); i++){
			if(allNodes.get(i).getDay() == day){
				nodesDay.add(allNodes.get(i));
			}
		}

		return nodesDay;
	}

	public boolean checkTransportTasks() {
		
		boolean found = false;
		
		for(int i=0; i<allNodes.size(); i++){
			if(allNodes.get(i).getTypeTransport() > 0){
				found = true;
			}
			
		}
		return found;
	}



}
