package heuristic.repo;
import java.util.ArrayList;

import heuristic.objects.Van;


public class VanRepository {

	ArrayList<Van> allVans;

	public VanRepository(int size){
		allVans = new ArrayList<Van>(size);
	}


	public void addVan(int idVan, int capacity, boolean active){
		Van v = new Van(idVan, capacity, active);
		allVans.add(v);
	}
	
	public void clearVans(){
		allVans.clear();
	}
	
	public void clearVansBigTime(){
		for(int i=0; i<allVans.size(); i++){
			allVans.get(i).clearVanBigTime();
		}
	}

	public Van getVan(int IDVan){
		return allVans.get(IDVan);
	}

	public Van getAvailableVan(double bestTime){

		Van mostCapacityAvailable = null;
		int capacity = -1;

		for(int i=0; i<allVans.size(); i++){

			if(allVans.get(i).getActive() == false){

				if(allVans.get(i).getCurrentTime() <= bestTime){

					if(allVans.get(i).getCapacity() > capacity){
						mostCapacityAvailable = allVans.get(i);
						capacity = allVans.get(i).getCapacity();
					}
				}
			}
		}

		return mostCapacityAvailable;

	}

}
