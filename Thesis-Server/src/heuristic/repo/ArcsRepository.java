package heuristic.repo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import heuristic.objects.Arc;
import heuristic.objects.Node;
import heuristic.objects.Worker;


public class ArcsRepository {

	ArrayList<Arc> allArcs;

	public ArcsRepository(){
		allArcs = new ArrayList<Arc>();
	}

	public void clearArcs(){
		allArcs.clear();
	}

	public void addArc(Node node1, Node node2, Worker worker, boolean car){
		Arc a = new Arc(node1,node2,worker, car);
		allArcs.add(a);
	}

	public void addArcTranspEnd(Node node1, Node node2, Worker worker, double distance, boolean car){
		Arc a = new Arc(node1,node2,worker,distance, car);
		allArcs.add(a);
	}

	public Arc getArc(int i){
		return allArcs.get(i);
	}

	public int getSize(){
		return allArcs.size();
	}

	public void sort() {

		Collections.sort(allArcs, new Comparator<Arc>(){ //ordenar todos os possiveis arcos dos acamados
			public int compare(Arc a1, Arc a2) {
				return Integer.compare(a1.getNode1().getID(), a2.getNode1().getID());
			}
		});

		for(int i=0;i<allArcs.size();i++){
			Arc a = allArcs.get(i);
			a.setID(i+1);
		}

	}

	public Arc getArcByInfo(Worker worker, Node startNode, Node finishNode) {

		Arc a = null;

		for(int i=0;i<allArcs.size();i++){

			if(allArcs.get(i).getWorker().getID() == worker.getID()){
				if(allArcs.get(i).getNode1().getID() == startNode.getID()){
					if(allArcs.get(i).getNode2().getID() == finishNode.getID()){
						a = allArcs.get(i);
					}
				}
			}

		}

		return a;
	}

	public ArrayList<Arc> getArcsTW(Node node2, ArrayList<Node> sameTWnodes, Worker worker) {

		ArrayList<Arc> arcsTWsame = new ArrayList<Arc>();

		for(int a=0; a<sameTWnodes.size(); a++){

			for(int i=0;i<allArcs.size();i++){

				if( (allArcs.get(i).getNode2().getID() == sameTWnodes.get(a).getID()) && (allArcs.get(i).getNode1().getID() == node2.getID()) && (allArcs.get(i).getWorker().getID() == worker.getID()) ){
					arcsTWsame.add(allArcs.get(i));
				}

			}


		}
		return arcsTWsame;
	}

	public Arc getMinBestTimeFromArcs(ArrayList<Arc> sameTWarcs) {

		Arc a = sameTWarcs.get(0);

		for(int i=1;i<sameTWarcs.size();i++){
			if(sameTWarcs.get(i).getBestTime() < a.getBestTime()){
				a = sameTWarcs.get(i);
			}
		}

		return a;
	}

	public double getDistanceNodes(Node node1, Node node2) {
		double lat1 = node1.getLati();
		double longi1 = node1.getLongi();
		double lat2 = node2.getLati();
		double longi2 = node2.getLongi();

		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(longi2 - longi1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
				* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters


		distance = Math.pow(distance, 2);

		return Math.sqrt(distance);
	}

	public void remove(Arc arc) {
		allArcs.remove(arc);
	}


}
