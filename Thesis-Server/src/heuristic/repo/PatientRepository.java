package heuristic.repo;
import java.util.ArrayList;

import heuristic.objects.Patient;


public class PatientRepository {

	ArrayList<Patient> allPatients;
	
	public PatientRepository(int size){
		allPatients = new ArrayList<Patient>(size);
	}
	
	public void addPatient(int id, String name, double lati, double longi){
		Patient p = new Patient(id, name, lati, longi);
		allPatients.add(p);
	}
	
	public Patient getPatient(int IDTeam){
		return allPatients.get(IDTeam);
	}


	
}
