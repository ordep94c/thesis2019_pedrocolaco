package heuristic.repo;
import java.util.ArrayList;

import heuristic.objects.Team;
import heuristic.objects.Worker;


public class WorkerRepository {
	
	ArrayList<Worker> allWorkers;
	
	public WorkerRepository(int size){
		allWorkers = new ArrayList<Worker>(size);
	}
	
	public void clearWorkers(){
		allWorkers.clear();
	}

	public void addWorker(int id, String name){
		Worker worker = new Worker(id, name);
		allWorkers.add(worker);
	}
	
	public Worker getWorker(int pos){
		return allWorkers.get(pos);
	}
	
	public int getSize(){
		return allWorkers.size();
	}
	
	public ArrayList<Worker> getAllWorkers(){
		return allWorkers;
	}
	
	public ArrayList<Worker> getWorkersByPeriod(int period) { //retornar equipas do periodo period
		ArrayList<Worker> workersType = new ArrayList<Worker>();

		for(int i=0;i<allWorkers.size();i++){
			if( allWorkers.get(i).getTeam().getPeriodType() == period ){
				workersType.add(allWorkers.get(i));
			}
		}

		return workersType;
	}
	
	public Worker getMostDurationWorker(){
		Worker worker = null;
		double duration = 0;

		for(int i=0;i<allWorkers.size();i++){
			if(allWorkers.get(i).timeWorked() > duration){
				duration = allWorkers.get(i).timeWorked();
				worker = allWorkers.get(i);
			}
		}

		return worker;
	}

	public Worker getLeastDurationWorker(){
		Worker worker = null;
		double duration = 1500;

		for(int i=0;i<allWorkers.size();i++){
				if(allWorkers.get(i).timeWorked() < duration){
					duration = allWorkers.get(i).timeWorked();
					worker = allWorkers.get(i);
				}
		}
		return worker;
	}

	public Worker getOtherWorker(int i) {

		Worker other = null;
		Worker worker = allWorkers.get(i);
		Team t = worker.getTeam();
		
		if(t != null){
		if(t.getOther(worker) != null){
			other = t.getOther(worker); 
		}
		}
		
		return other;
	}

	public boolean checkSameTeam(Worker workerSendOrigi, Worker workerReceiveOrigi) {
		
		boolean sameTeam = false;
		
		int id1 = workerSendOrigi.getTeam().getIDTeam();
		int id2 = workerReceiveOrigi.getTeam().getIDTeam();
		
		if(id1 == id2){
			sameTeam = true;
		}
		
		return sameTeam;
	}
}
