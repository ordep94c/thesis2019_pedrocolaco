package heuristic.repo;
import java.util.ArrayList;
import java.util.List;

import heuristic.objects.Node;
import heuristic.objects.Team;


public class TeamRepository {

	ArrayList<Team> allTeams;

	List<Node> morningPeriod = new ArrayList<Node>();			//nos que pertencem ao perido da manha
	List<Node> afternoonPeriod = new ArrayList<Node>();		//nos que pertencem ao perido da tarde

	public TeamRepository(int size){
		allTeams = new ArrayList<Team>(size);
	}

	public void clearTeams(){
		allTeams.clear();

		morningPeriod.clear();
		afternoonPeriod.clear();
	}

	public void addTeam(int idTeam, Node node, boolean hadL, int periodType, double startT, double finishT, int maxTime, boolean routeF){
		Team t = new Team(idTeam, node, hadL, periodType, startT, finishT, maxTime, routeF);
		allTeams.add(t);
	}

	public Team getTeam(int IDTeam){
		return allTeams.get(IDTeam);
	}

	
	public List<Node> getMorningNodes(){
		return morningPeriod;
	}

	public void addMorningNode(Node n){
		morningPeriod.add(n);
	}


	public List<Node> getAfternoonNodes(){
		return afternoonPeriod;
	}

	public void addAfternoonNode(Node n){
		afternoonPeriod.add(n);
	}
	
	public double durationTasksMorning(){	//dura��o das tarefas da manh�
		double totalDur = 0;

		for(int i=0;i<morningPeriod.size();i++){
			totalDur = totalDur + morningPeriod.get(i).getDuration();
		}

		return totalDur;
	}
	
	public double durationTasksAfternoon(){	//dura��o das tarefas da tarde
		double totalDur = 0;

		for(int i=0;i<afternoonPeriod.size();i++){
			totalDur = totalDur + afternoonPeriod.get(i).getDuration();
		}

		return totalDur;
	}
	
	

	public ArrayList<Team> getTeamsByPeriod(int period) { //retornar equipas do tipo type e periodo period
		ArrayList<Team> teamsType = new ArrayList<Team>();

		for(int i=0;i<allTeams.size();i++){
			if( allTeams.get(i).getPeriodType() == period ){
				teamsType.add(allTeams.get(i));
			}
		}

		return teamsType;
	}

	public int getSize(){
		return allTeams.size();
	}

	public String getTeamType() {
		String teamToReturn = null;
		double totalDurationTasksType = 0;

		String teamM = "M";
		String teamT = "T";

		double M = durationTasksMorning()/(getTeamsByPeriod(1).size());
		double T = durationTasksAfternoon()/(getTeamsByPeriod(2).size());

		teamToReturn = teamM;
		totalDurationTasksType = M; //Inicializar Manh� como tipo de team a retornar. Verificar depois qual a que tem menos equipas associado

		if(T > totalDurationTasksType){
			teamToReturn = teamT;
			totalDurationTasksType = T;
		}

		return teamToReturn;
	}


}
