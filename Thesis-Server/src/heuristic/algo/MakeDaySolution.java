package heuristic.algo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import heuristic.objects.Arc;
import heuristic.objects.Node;
import heuristic.objects.Patient;
import heuristic.objects.Van;
import heuristic.objects.Worker;
import heuristic.repo.ArcsRepository;
import heuristic.repo.NodeRepository;
import heuristic.repo.SolutionsRepository;
import heuristic.repo.TeamRepository;
import heuristic.repo.VanRepository;
import heuristic.repo.WorkerRepository;

import com.google.common.collect.Sets;


public class MakeDaySolution {

	public static int day;

	public static int totalTeams;
	public static int totalWorkers;

	public static boolean sameTeam_samePatient_constraint;				//restri��o que determina se um paciente tem de ser visitado pela mesma equipa
	public static int maxTime;											//maximo trabalho di�rio(minutos)
	public static boolean doLunch;
	public static boolean car;
	public static int lunchTime;									//hora de almo�o
	public static int lunchConsideredToTeamsStartingWorkingBeforeEqual;	//v�o ser considerados almo�os para as equipas que come�am a trabalhar antes de...(em minutos)
	public static int maxWaitTimeBetweenTasks; 							//tempo maximo de espera permitido antes da realiza��o de uma tarefa
	public static int tasksTWresize;									//aumentar (em percentagem) as janelas temporais. cada n� tem uma variavel booleana que permite, ou n�o, esta mudan�a
	public static boolean extensiveSearch;

	public static double centerLati;
	public static double centerLongi;

	public static WorkerRepository workers;
	public static TeamRepository teams;
	public static VanRepository vans;
	public static NodeRepository nodesDay;
	public static ArcsRepository arcs;

	public static SolutionsRepository solutions;

	public static double firstTaskTime;				//hora em que come�a a primeira tarefa
	public static Node firstTask;					//primeira tarefa
	public static double lastTaskTime;				//hora em que come�a a ultima tarefa
	public static Node lastTask;					//ultima tarefa
	public static double morningPeriodStartTime;	//hora em que come�a o periodo da manha
	public static double morningPeriodFinishTime;	//hora em que termina o periodo da manha
	public static double afternoonPeriodStartTime;	//hora em que come�a o periodo da tarde
	public static double afternoonPeriodFinishTime;	//hora em que termina o periodo da manha


	MakeDaySolution(int d, ArrayList<Integer> vansCap, int TTeams, int TWorkers, boolean TPconstraint, int mTime, boolean carTransp, boolean doL,  int lunch, int lunchBE, int maxWTTasks, int resizeTW, boolean extSearch, ArrayList<Node> nodesDayList, double centerLa, double centerLo) throws InterruptedException{

		try{

			day = d;

			totalTeams = TTeams;
			totalWorkers = TWorkers;

			centerLati = centerLa;
			centerLongi = centerLo;

			vans = new VanRepository(vansCap.size());
			workers = new WorkerRepository(totalWorkers);
			teams = new TeamRepository(totalTeams);
			nodesDay = new NodeRepository();
			arcs = new ArcsRepository();

			for(int i=0; i<vansCap.size(); i++){
				vans.addVan(i, vansCap.get(i), false);
			}

			sameTeam_samePatient_constraint = TPconstraint;
			maxTime = mTime;
			car = carTransp;
			doLunch = doL;
			lunchTime = lunch;
			lunchConsideredToTeamsStartingWorkingBeforeEqual = lunchBE;
			maxWaitTimeBetweenTasks = maxWTTasks;
			tasksTWresize = resizeTW;
			extensiveSearch = extSearch;

			solutions = new SolutionsRepository();
			solutions.clearSolutions();

			int index = 0;
			Node cNode = nodesDayList.get(0);
			nodesDay.addNode(index, cNode.getType(), cNode.getTypeTransport(), "Centro Inicial", cNode.getLati(), cNode.getLongi(), cNode.getIniTW(), cNode.getFinTW(), cNode.getDuration(), cNode.getDay());
			index = index + 1;
			for(int i=1; i<nodesDayList.size(); i++){
				Node n = nodesDayList.get(i);
				nodesDay.addTask(index, n.getType(), n.getTypeTransport(), n.getPatient(), n.getIniTW(), n.getFinTW(), n.getDuration(), n.getChangeTW(), n.getDay());
				index = index + 1;
			}

			preSetup();

			nodesDay.resizeNodes(tasksTWresize,morningPeriodStartTime,afternoonPeriodFinishTime);

			setup(doLunch);

			List<Node> nodesInSolution = makeInitialSolution(sameTeam_samePatient_constraint);

			if(extensiveSearch == false){
				saveFirstSolu(nodesInSolution);
				clearDay();
			}

			if(extensiveSearch == true){
				optimizeSolution(nodesInSolution);

				if(day > 0){
					clearDay();
				}
			}

		}catch(InterruptedException e){
			System.out.println("");
			System.out.println("------------------------------------------------------------------------------");
			System.out.println("TIMEOUT EXCEPTION CATCHED");
			System.out.println("------------------------------------------------------------------------------");
			System.out.println("");
			getSolus();	
		}


	}

	private void saveFirstSolu(List<Node> nodesInSolution) {

		//PRIMEIRA SOLUÇÃO
		System.out.println("");
		System.out.println("-----FIRST SOLUTION-----");

		if( nodesInSolution.size()+1 == nodesDay.getSize() ){
			System.out.println("ALL TASKS MADE");

			double totalTimeWorkedRoutes = 0;
			double totalWaitingTimeRoutes = 0;
			double fairnessValueRoutes;
			List<Worker> workersS = new ArrayList<Worker>();
			List<List<Arc>> routes = new ArrayList<List<Arc>>();
			Map<Worker,Double> valuesTimeWorkedByWorker = new HashMap<Worker, Double>();
			Map<Worker,Double> valuesTimeWaitedByWorker = new HashMap<Worker, Double>();

			System.out.println("");
			for(int i=0;i<totalWorkers;i++){

				List<Arc> listArcsFirstSolutionCopy = new ArrayList<Arc>();
				List<Arc> listArcsFirstSolution = workers.getWorker(i).getRouteDay();

				for(int h=0;h<listArcsFirstSolution.size();h++){
					Arc arcCopy = new Arc(listArcsFirstSolution.get(h));
					listArcsFirstSolutionCopy.add(arcCopy);
					System.out.println("Worker: "+i+" and arcID: "+listArcsFirstSolution.get(h).getID()+" with nodes: "+listArcsFirstSolution.get(h).getNode1().getName()+"("+listArcsFirstSolution.get(h).getNode1().getID()+")"+" - "+listArcsFirstSolution.get(h).getNode2().getName()+"("+listArcsFirstSolution.get(h).getNode2().getID()+")"+" started at: "+listArcsFirstSolution.get(h).getBestTime()+" until: "+(listArcsFirstSolution.get(h).getBestTime()+listArcsFirstSolution.get(h).getNode2().getDuration())+". Wait Time: "+listArcsFirstSolution.get(h).getWaitTime());
					if(listArcsFirstSolution.get(h).getVan() != null){
						System.out.println("	Van: "+listArcsFirstSolution.get(h).getVan().getIDvan()+". Initial Capacity: "+listArcsFirstSolution.get(h).getVan().getCapacity()+". Current Capacity: "+listArcsFirstSolution.get(h).getCapacityAtTheTime());
					}
				}
				System.out.println("Worker: "+i+". Total time worked: "+workers.getWorker(i).timeWorked()+". Total time wait: "+workers.getWorker(i).totalWaitTime());


				Worker workerCopy = new Worker(workers.getWorker(i));
				workersS.add(workerCopy);
				routes.add(listArcsFirstSolutionCopy);

				totalTimeWorkedRoutes = totalTimeWorkedRoutes + workerCopy.timeWorked();
				valuesTimeWorkedByWorker.put(workerCopy, workerCopy.timeWorked());

				totalWaitingTimeRoutes = totalWaitingTimeRoutes + workerCopy.totalWaitTime();
				valuesTimeWaitedByWorker.put(workerCopy, workerCopy.totalWaitTime());

				System.out.println("");
			}

			checkEquality();
			fairnessValueRoutes = getFairnessBed();

			SolutionRoutes firstSolution = new SolutionRoutes(workersS, routes, totalTimeWorkedRoutes, valuesTimeWorkedByWorker, totalWaitingTimeRoutes,valuesTimeWaitedByWorker, fairnessValueRoutes);
			solutions = new SolutionsRepository(firstSolution);

			System.out.println("");
			System.out.println("");
			System.out.println("-----First Solution-----");
			Map<Worker,ArrayList<Arc>> routesArcsFirstSolution = firstSolution.getRoutesArcs();
			List<Worker> workersFirtSolution = firstSolution.getWorkersRoutes();
			for(int i=0;i<workersFirtSolution.size();i++){
				Worker worker = workersFirtSolution.get(i);
				List<Arc> arcs = routesArcsFirstSolution.get(worker);
				System.out.println("Worker ID: "+worker.getID());
				for(int j=0;j<arcs.size();j++){
					System.out.println("ArcID: "+arcs.get(j).getID()+" with nodes: "+arcs.get(j).getNode1().getName()+"("+arcs.get(j).getNode1().getID()+")"+" - "+arcs.get(j).getNode2().getName()+"("+arcs.get(j).getNode2().getID()+")"+" started at: "+arcs.get(j).getBestTime()+" until: "+(arcs.get(j).getBestTime()+arcs.get(j).getNode2().getDuration())+". Wait Time: "+arcs.get(j).getWaitTime());
				}
			}
			System.out.println("Total Time Worked: "+firstSolution.getTotalTimeWorked()+". Total time wait: "+firstSolution.getTotalTimeWaiting()+". Fairness: "+firstSolution.getFairnessValue());


		}

		else{
			System.out.println("NOT ALL TASKS MADE: "+(nodesInSolution.size()+1)+" vs "+nodesDay.getSize());

			List<Node> nodesMissing = new ArrayList<Node>();

			for(int j=0;j<nodesDay.getAll().size();j++){ //Determinar que tarefas ficaram por fazer

				boolean found = false;
				for(int g=0;g<nodesInSolution.size();g++){		
					if(nodesDay.getAll().get(j).getID() == nodesInSolution.get(g).getID()){
						found = true;
					}
				}
				if(found == false){
					nodesMissing.add(nodesDay.getAll().get(j));
				}
			}

			for(int jj=1;jj<nodesMissing.size();jj++){	
				System.out.println(nodesMissing.get(jj).getName()+" and ID: "+nodesMissing.get(jj).getID()+" and Type: "+nodesMissing.get(jj).getType()+" and TW: "+nodesMissing.get(jj).getIniTW()+"--"+nodesMissing.get(jj).getFinTW());
			}
		}

	}

	public SolutionsRepository getSolus(){
		return solutions;
	}

	private void clearDay() {		
		workers.clearWorkers();
		teams.clearTeams();
		vans.clearVansBigTime();
		nodesDay.clearNodes();
		arcs.clearArcs();
	}


	private static void optimizeSolution(List<Node> nodesInSolution) throws InterruptedException {

		//PRIMEIRA SOLUÇÃO
		System.out.println("");
		System.out.println("-----FIRST SOLUTION-----");

		if( nodesInSolution.size()+1 == nodesDay.getSize() ){
			System.out.println("ALL TASKS MADE");

			double totalTimeWorkedRoutes = 0;
			double totalWaitingTimeRoutes = 0;
			double fairnessValueRoutes;
			List<Worker> workersS = new ArrayList<Worker>();
			List<List<Arc>> routes = new ArrayList<List<Arc>>();
			Map<Worker,Double> valuesTimeWorkedByWorker = new HashMap<Worker, Double>();
			Map<Worker,Double> valuesTimeWaitedByWorker = new HashMap<Worker, Double>();

			System.out.println("");
			for(int i=0;i<totalWorkers;i++){

				List<Arc> listArcsFirstSolutionCopy = new ArrayList<Arc>();
				List<Arc> listArcsFirstSolution = workers.getWorker(i).getRouteDay();

				for(int h=0;h<listArcsFirstSolution.size();h++){
					Arc arcCopy = new Arc(listArcsFirstSolution.get(h));
					listArcsFirstSolutionCopy.add(arcCopy);
					System.out.println("Worker: "+i+" and arcID: "+listArcsFirstSolution.get(h).getID()+" with nodes: "+listArcsFirstSolution.get(h).getNode1().getName()+"("+listArcsFirstSolution.get(h).getNode1().getID()+")"+" - "+listArcsFirstSolution.get(h).getNode2().getName()+"("+listArcsFirstSolution.get(h).getNode2().getID()+")"+" started at: "+listArcsFirstSolution.get(h).getBestTime()+" until: "+(listArcsFirstSolution.get(h).getBestTime()+listArcsFirstSolution.get(h).getNode2().getDuration())+". Wait Time: "+listArcsFirstSolution.get(h).getWaitTime());
					if(listArcsFirstSolution.get(h).getVan() != null){
						System.out.println("	Van: "+listArcsFirstSolution.get(h).getVan().getIDvan()+". Initial Capacity: "+listArcsFirstSolution.get(h).getVan().getCapacity()+". Current Capacity: "+listArcsFirstSolution.get(h).getCapacityAtTheTime());
					}
				}
				System.out.println("Worker: "+i+". Total time worked: "+workers.getWorker(i).timeWorked()+". Total time wait: "+workers.getWorker(i).totalWaitTime());


				Worker workerCopy = new Worker(workers.getWorker(i));
				workersS.add(workerCopy);
				routes.add(listArcsFirstSolutionCopy);

				totalTimeWorkedRoutes = totalTimeWorkedRoutes + workerCopy.timeWorked();
				valuesTimeWorkedByWorker.put(workerCopy, workerCopy.timeWorked());

				totalWaitingTimeRoutes = totalWaitingTimeRoutes + workerCopy.totalWaitTime();
				valuesTimeWaitedByWorker.put(workerCopy, workerCopy.totalWaitTime());

				System.out.println("");
			}

			checkEquality();
			fairnessValueRoutes = getFairnessBed();

			if(extensiveSearch == true){

				SolutionRoutes firstSolution = new SolutionRoutes(workersS, routes, totalTimeWorkedRoutes, valuesTimeWorkedByWorker, totalWaitingTimeRoutes,valuesTimeWaitedByWorker, fairnessValueRoutes);

				solutions = new SolutionsRepository(firstSolution);

				System.out.println("");
				System.out.println("");

				improveSolution();
			}
		}

		else{
			System.out.println("NOT ALL TASKS MADE: "+(nodesInSolution.size()+1)+" vs "+nodesDay.getSize());

			List<Node> nodesMissing = new ArrayList<Node>();

			for(int j=0;j<nodesDay.getAll().size();j++){ //Determinar que tarefas ficaram por fazer
				boolean found = false;
				for(int g=0;g<nodesInSolution.size();g++){		
					if(nodesDay.getAll().get(j).getID() == nodesInSolution.get(g).getID()){
						found = true;
					}
				}
				if(found == false){
					nodesMissing.add(nodesDay.getAll().get(j));
				}
			}

			for(int jj=1;jj<nodesMissing.size();jj++){	
				System.out.println(nodesMissing.get(jj).getName()+" and ID: "+nodesMissing.get(jj).getID()+" and Type: "+nodesMissing.get(jj).getType()+" and TW: "+nodesMissing.get(jj).getIniTW()+"--"+nodesMissing.get(jj).getFinTW());
			}

			System.out.println("");
			for(int i=0;i<totalWorkers;i++){
				List<Arc> aa = workers.getWorker(i).getRouteDay();
				for(int h=0;h<aa.size();h++){
					System.out.println("Worker: "+i+" and arcID: "+aa.get(h).getID()+" with nodes: "+aa.get(h).getNode1().getName()+"("+aa.get(h).getNode1().getID()+")"+" - "+aa.get(h).getNode2().getName()+"("+aa.get(h).getNode2().getID()+")"+" started at: "+aa.get(h).getBestTime()+" until: "+(aa.get(h).getBestTime()+aa.get(h).getNode2().getDuration())+". Wait Time: "+aa.get(h).getWaitTime());
					if(aa.get(h).getVan() != null){
						System.out.println("Van: "+aa.get(h).getVan().getIDvan()+". Initial Capacity: "+aa.get(h).getVan().getCapacity()+". Current Capacity: "+aa.get(h).getCapacityAtTheTime());
					}
				}
				System.out.println("Total time worked: "+workers.getWorker(i).timeWorked()+". Total time wait: "+workers.getWorker(i).totalWaitTime());
				System.out.println("");
			}

		}
	}



	public static void improveSolution() throws InterruptedException{


		SolutionRoutes firstSolu = solutions.getFirstSolution();
		List<List<Worker>> comboWorkers = getComboWorkers(solutions.getFirstSolution().getWorkersRoutes());
		int index = 0;

		for(int i=0;i<comboWorkers.size();i++){ //combinações de pares das equipas envolventes

			Worker workerSend = comboWorkers.get(i).get(0);
			Worker workerReceive = comboWorkers.get(i).get(1);;

			if( firstSolu.getRoutesNodes().get(workerReceive).size() > firstSolu.getRoutesNodes().get(workerSend).size() ){
				Worker aux = workerSend;
				workerSend = workerReceive;
				workerReceive = aux;
			}

			for(int j=1; j < firstSolu.getRoutesNodes().get(workerSend).size(); j++){ //cada indice significa o numero de trocas

				int numberOfSwipes = j;
				ArrayList<ArrayList<ArrayList<Node>>> nodesToSwipe = getNodesToSwipe(firstSolu, workerSend, workerReceive, numberOfSwipes);

				for(int h=0;h<nodesToSwipe.size();h++){

					List<Node> beforeSwipeNodesSendWorker = firstSolu.getRoutesNodes().get(workerSend);
					List<Node> beforeSwipeNodesReceiveWorker = firstSolu.getRoutesNodes().get(workerReceive);

					List<Node> afterSwipeNodesSendWorker = new ArrayList<Node>(beforeSwipeNodesSendWorker);
					List<Node> afterSwipeNodesReceiveWorker = new ArrayList<Node>(beforeSwipeNodesReceiveWorker);

					List<Node> nodeToSend = nodesToSwipe.get(h).get(0);
					List<Node> nodeToReceive = nodesToSwipe.get(h).get(1);

					System.out.println("Going To Swap Nodes:");
					for(int k=0;k<nodeToSend.size();k++){
						afterSwipeNodesSendWorker.remove(nodeToSend.get(k));
						afterSwipeNodesReceiveWorker.add(nodeToSend.get(k));
						System.out.println("	"+nodeToSend.get(k).getName()+"("+nodeToSend.get(k).getID()+") From Worker: "+workerSend.getName());
					}

					System.out.println("With Nodes:"); 
					for(int l=0;l<nodeToReceive.size();l++){
						afterSwipeNodesReceiveWorker.remove(nodeToReceive.get(l));
						afterSwipeNodesSendWorker.add(nodeToReceive.get(l));
						System.out.println("	"+nodeToReceive.get(l).getName()+"("+nodeToReceive.get(l).getID()+") From Worker: "+workerReceive.getName());
					}

					List<Node> afterSwipeNodesSendWorkerFinal = new ArrayList<Node>();
					for(int l=0;l<afterSwipeNodesSendWorker.size();l++){
						if(afterSwipeNodesSendWorker.get(l).getName().contains("Centro Transporte Inicio") || afterSwipeNodesSendWorker.get(l).getName().contains("Centro Transporte Final") ){

						}
						else{
							afterSwipeNodesSendWorkerFinal.add(afterSwipeNodesSendWorker.get(l));
						}
					}


					List<Node> afterSwipeNodesReceiveWorkerFinal = new ArrayList<Node>();
					for(int l=0;l<afterSwipeNodesReceiveWorker.size();l++){
						if(afterSwipeNodesReceiveWorker.get(l).getName().contains("Centro Transporte Inicio") || afterSwipeNodesReceiveWorker.get(l).getName().contains("Centro Transporte Final") ){

						}
						else{
							afterSwipeNodesReceiveWorkerFinal.add(afterSwipeNodesReceiveWorker.get(l));
						}
					}

					Thread.sleep(1);
					testNewSolution(solutions.getFirstSolution(), workerSend, workerReceive, afterSwipeNodesSendWorkerFinal, afterSwipeNodesReceiveWorkerFinal, nodeToSend, nodeToReceive, index);
					index = index + 1;

				}
			}
		}

		SolutionRoutes firstSolution = solutions.getFirstSolution();
		SolutionRoutes totalTimeSolution = solutions.getTotalTimeSolution();
		SolutionRoutes waitingTimeSolution = solutions.getWaitingTimeSolution();
		SolutionRoutes fairSolution = solutions.getFairSolution();

		System.out.println("");
		System.out.println("");
		System.out.println("-----First Solution-----");
		Map<Worker,ArrayList<Arc>> routesArcsFirstSolution = firstSolution.getRoutesArcs();
		List<Worker> workersFirtSolution = firstSolution.getWorkersRoutes();
		for(int i=0;i<workersFirtSolution.size();i++){
			Worker worker = workersFirtSolution.get(i);
			List<Arc> arcs = routesArcsFirstSolution.get(worker);
			System.out.println("Worker ID: "+worker.getID());
			for(int j=0;j<arcs.size();j++){
				System.out.println("ArcID: "+arcs.get(j).getID()+" with nodes: "+arcs.get(j).getNode1().getName()+"("+arcs.get(j).getNode1().getID()+")"+" - "+arcs.get(j).getNode2().getName()+"("+arcs.get(j).getNode2().getID()+")"+" started at: "+arcs.get(j).getBestTime()+" until: "+(arcs.get(j).getBestTime()+arcs.get(j).getNode2().getDuration())+". Wait Time: "+arcs.get(j).getWaitTime());
				if(arcs.get(j).getVan() != null){
					System.out.println("	Van: "+arcs.get(j).getVan().getIDvan()+". Initial Capacity: "+arcs.get(j).getVan().getCapacity()+". Current Capacity: "+arcs.get(j).getCapacityAtTheTime());
				}
			}
		}
		System.out.println("Total Time Worked: "+firstSolution.getTotalTimeWorked()+". Total time wait: "+firstSolution.getTotalTimeWaiting()+". Fairness: "+firstSolution.getFairnessValue());


		System.out.println("");
		System.out.println("-----Total Time Solution-----");
		Map<Worker,ArrayList<Arc>> routesArcsTimeSolution = totalTimeSolution.getRoutesArcs();
		List<Worker> workersTimeSolution = totalTimeSolution.getWorkersRoutesOrdered();

		for(int i=0;i<workersTimeSolution.size();i++){
			Worker worker = workersTimeSolution.get(i);
			List<Arc> arcs = routesArcsTimeSolution.get(worker);
			System.out.println("Worker ID: "+worker.getID());
			for(int j=0;j<arcs.size();j++){
				System.out.println("ArcID: "+arcs.get(j).getID()+" with nodes: "+arcs.get(j).getNode1().getName()+"("+arcs.get(j).getNode1().getID()+")"+" - "+arcs.get(j).getNode2().getName()+"("+arcs.get(j).getNode2().getID()+")"+" started at: "+arcs.get(j).getBestTime()+" until: "+(arcs.get(j).getBestTime()+arcs.get(j).getNode2().getDuration())+". Wait Time: "+arcs.get(j).getWaitTime());
				if(arcs.get(j).getVan() != null){
					System.out.println("	Van: "+arcs.get(j).getVan().getIDvan()+". Initial Capacity: "+arcs.get(j).getVan().getCapacity()+". Current Capacity: "+arcs.get(j).getCapacityAtTheTime());
				}
			}
		}
		System.out.println("TOTAL TIME WORKED: "+totalTimeSolution.getTotalTimeWorked()+". Total time wait: "+totalTimeSolution.getTotalTimeWaiting()+". Fairness: "+totalTimeSolution.getFairnessValue());


		System.out.println("");
		System.out.println("-----Total Waiting Time Solution-----");
		Map<Worker,ArrayList<Arc>> routesArcsWaitingTimeSolution = waitingTimeSolution.getRoutesArcs();
		List<Worker> workersWaitingTimeSolution = waitingTimeSolution.getWorkersRoutesOrdered();

		for(int i=0;i<workersWaitingTimeSolution.size();i++){
			Worker worker = workersWaitingTimeSolution.get(i);
			List<Arc> arcs = routesArcsWaitingTimeSolution.get(worker);
			System.out.println("Worker ID: "+worker.getID());
			for(int j=0;j<arcs.size();j++){
				System.out.println("ArcID: "+arcs.get(j).getID()+" with nodes: "+arcs.get(j).getNode1().getName()+"("+arcs.get(j).getNode1().getID()+")"+" - "+arcs.get(j).getNode2().getName()+"("+arcs.get(j).getNode2().getID()+")"+" started at: "+arcs.get(j).getBestTime()+" until: "+(arcs.get(j).getBestTime()+arcs.get(j).getNode2().getDuration())+". Wait Time: "+arcs.get(j).getWaitTime());
				if(arcs.get(j).getVan() != null){
					System.out.println("	Van: "+arcs.get(j).getVan().getIDvan()+". Initial Capacity: "+arcs.get(j).getVan().getCapacity()+". Current Capacity: "+arcs.get(j).getCapacityAtTheTime());
				}
			}
		}
		System.out.println("Total Time Worked: "+waitingTimeSolution.getTotalTimeWorked()+". TOTAL TIME WAITING: "+waitingTimeSolution.getTotalTimeWaiting()+". Fairness: "+waitingTimeSolution.getFairnessValue());


		System.out.println("");
		System.out.println("-----Fair Solution-----");
		Map<Worker,ArrayList<Arc>> routesArcsFairSolution = fairSolution.getRoutesArcs();
		List<Worker> workersFairSolution = fairSolution.getWorkersRoutesOrdered();
		for(int i=0;i<workersFairSolution.size();i++){
			Worker worker = workersFairSolution.get(i);
			List<Arc> arcs = routesArcsFairSolution.get(worker);
			System.out.println("Worker ID: "+worker.getID());
			for(int j=0;j<arcs.size();j++){
				System.out.println("ArcID: "+arcs.get(j).getID()+" with nodes: "+arcs.get(j).getNode1().getName()+"("+arcs.get(j).getNode1().getID()+")"+" - "+arcs.get(j).getNode2().getName()+"("+arcs.get(j).getNode2().getID()+")"+" started at: "+arcs.get(j).getBestTime()+" until: "+(arcs.get(j).getBestTime()+arcs.get(j).getNode2().getDuration())+". Wait Time: "+arcs.get(j).getWaitTime());
				if(arcs.get(j).getVan() != null){
					System.out.println("	Van: "+arcs.get(j).getVan().getIDvan()+". Initial Capacity: "+arcs.get(j).getVan().getCapacity()+". Current Capacity: "+arcs.get(j).getCapacityAtTheTime());
				}
			}
		}
		System.out.println("Total Time Worked: "+fairSolution.getTotalTimeWorked()+". Total Time Waiting: "+fairSolution.getTotalTimeWaiting()+". FAIRNESS: "+fairSolution.getFairnessValue());

	}

	private static <E> List<E> pickNRandomElements(List<E> list, int n, Random r) {
		int length = list.size();

		if (length < n) return null;

		for (int i = length - 1; i >= length - n; --i)
		{
			Collections.swap(list, i , r.nextInt(i + 1));
		}
		return list.subList(length - n, length);
	}

	private static ArrayList<ArrayList<ArrayList<Node>>> getNodesToSwipe(SolutionRoutes firstSolu, Worker workerSend, Worker workerReceive, int numberOfSwipes) {

		List<Node> initialNodesSendWorker = firstSolu.getRoutesNodes().get(workerSend);
		List<Node> initialNodesReceiveWorker = firstSolu.getRoutesNodes().get(workerReceive);

		ArrayList<ArrayList<ArrayList<Node>>> nodesToSwipeCombo = new ArrayList<ArrayList<ArrayList<Node>>>();


		ArrayList<ArrayList<Node>> combosInitialNodesSendWorker = new ArrayList<ArrayList<Node>>();

		int nNumber = 12;

		if(initialNodesSendWorker.size() > nNumber ){
			initialNodesSendWorker = pickNRandomElements(initialNodesSendWorker, nNumber, ThreadLocalRandom.current());
		}
		if(initialNodesReceiveWorker.size() > nNumber ){
			initialNodesReceiveWorker = pickNRandomElements(initialNodesReceiveWorker, nNumber, ThreadLocalRandom.current());
		}

		Set<Node> nodesSet = new HashSet<Node>(initialNodesSendWorker);

		Set<Set<Node>> combinationsSet = Sets.powerSet(nodesSet);
		Iterator<Set<Node>> combinationsIterator = combinationsSet.iterator();
		while (combinationsIterator.hasNext()) {
			ArrayList<Node> nodesList = new ArrayList<Node>(combinationsIterator.next());
			combosInitialNodesSendWorker.add(nodesList);
		}

		ArrayList<ArrayList<Node>> combosInitialNodesReceiveWorker = new ArrayList<ArrayList<Node>>();
		Set<Node> nodesSet2 = new HashSet<Node>(initialNodesReceiveWorker);
		Set<Set<Node>> combinationsSet2 = Sets.powerSet(nodesSet2);
		Iterator<Set<Node>> combinationsIterator2 = combinationsSet2.iterator();
		while (combinationsIterator2.hasNext()) {
			ArrayList<Node> nodesList2 = new ArrayList<Node>(combinationsIterator2.next());
			combosInitialNodesReceiveWorker.add(nodesList2);
		}


		ArrayList<ArrayList<Node>> comboNodesSendWorkerIndexSwipes = new ArrayList<ArrayList<Node>>();
		for(int a=0;a<combosInitialNodesSendWorker.size();a++){
			if(combosInitialNodesSendWorker.get(a).size() == numberOfSwipes){
				comboNodesSendWorkerIndexSwipes.add(combosInitialNodesSendWorker.get(a));
			}
		}

		ArrayList<ArrayList<Node>> comboNodesReceiveWorkerIndexSwipes = new ArrayList<ArrayList<Node>>();
		for(int b=0;b<combosInitialNodesReceiveWorker.size();b++){
			if(combosInitialNodesReceiveWorker.get(b).size() == numberOfSwipes){
				comboNodesReceiveWorkerIndexSwipes.add(combosInitialNodesReceiveWorker.get(b));
			}
		}


		for(int i=0; i<comboNodesSendWorkerIndexSwipes.size(); i++){

			for(int j=0; j<comboNodesReceiveWorkerIndexSwipes.size(); j++){

				ArrayList<Node> nodesToSend = new ArrayList<Node>(comboNodesSendWorkerIndexSwipes.get(i));
				ArrayList<Node> nodesToReceive = new ArrayList<Node>(comboNodesReceiveWorkerIndexSwipes.get(j));

				boolean foundLunchOrCenter = false;//impedir swipes que envolvem almoços

				for(int h=0;h<nodesToSend.size();h++){
					if( (nodesToSend.get(h).getName().contains("Almoço")) || (nodesToSend.get(h).getName().contains("Centro Transporte Inicio")) || (nodesToSend.get(h).getName().contains("Centro Transporte Final")) ){
						foundLunchOrCenter = true;
					}
				}

				for(int h=0;h<nodesToReceive.size();h++){
					if( (nodesToReceive.get(h).getName().contains("Almoço")) || (nodesToReceive.get(h).getName().contains("Centro Transporte Inicio")) || (nodesToReceive.get(h).getName().contains("Centro Transporte Final")) ){
						foundLunchOrCenter = true;
					}
				}

				if(foundLunchOrCenter == false){

					ArrayList<ArrayList<Node>> nodesToSwipe = new ArrayList<ArrayList<Node>>();
					nodesToSwipe.add(nodesToSend);
					nodesToSwipe.add(nodesToReceive);

					nodesToSwipeCombo.add(nodesToSwipe);

				}

			}
		}


		return nodesToSwipeCombo;
	}



	private static List<List<Worker>> getComboWorkers(List<Worker> workersRoutes) {

		List<List<Worker>> comboWorkers = new ArrayList<List<Worker>>();

		for(int i=0; i<workersRoutes.size(); i++){
			for(int j=0; j<workersRoutes.size();j++){

				List<Worker> pairTeams = new ArrayList<Worker>();
				List<Worker> pairTeamsInv = new ArrayList<Worker>();

				if(workersRoutes.get(i).getID() != workersRoutes.get(j).getID()){
					pairTeams.add(workersRoutes.get(i));
					pairTeams.add(workersRoutes.get(j));

					pairTeamsInv.add(workersRoutes.get(j));
					pairTeamsInv.add(workersRoutes.get(i));

					if( (!comboWorkers.contains(pairTeams)) && (!comboWorkers.contains(pairTeamsInv)) ){
						comboWorkers.add(pairTeams);
					}

				}	
			}
		}

		return comboWorkers;
	}



	private static void testNewSolution(SolutionRoutes firstSolution,
			Worker workerSendOrigi, Worker workerReceiveOrigi,
			List<Node> afterSwipeNodesSendWorker,
			List<Node> afterSwipeNodesReceiveWorker, 
			List<Node> nodeToSend, 
			List<Node> nodeToReceive, int index) {


		for(int i=0;i<arcs.getSize();i++){
			arcs.getArc(i).resetValues();
		}

		vans.clearVansBigTime();

		List<Node> garbageNodes = new ArrayList<Node>();
		List<Arc> garbageArcs = new ArrayList<Arc>();

		Map<Worker,List<Node>> mapNodesWorkerSolu = new HashMap<Worker,List<Node>>();

		List<Worker> workersToReRoute = new ArrayList<Worker>();
		Worker workerSend = new Worker(workerSendOrigi);
		Worker workerReceive = new Worker(workerReceiveOrigi);

		List<Node> extendedList = new ArrayList<Node>();
		List<Node> extendedList2 = new ArrayList<Node>();

		workerSend.clearRoute();
		workerReceive.clearRoute();

		workersToReRoute.add(workerSend);
		workersToReRoute.add(workerReceive);


		if(sameTeam_samePatient_constraint == true){

			for(int iii=0; iii<nodeToSend.size(); iii++){
				for(int jjj=0; jjj<afterSwipeNodesSendWorker.size(); jjj++){
					if(afterSwipeNodesSendWorker.get(jjj).getName().equalsIgnoreCase(nodeToSend.get(iii).getName())){
						extendedList.add(afterSwipeNodesSendWorker.get(jjj));
					}
				}
			}

			for(int iii=0; iii<nodeToReceive.size(); iii++){
				for(int jjj=0; jjj<afterSwipeNodesReceiveWorker.size(); jjj++){
					if(afterSwipeNodesReceiveWorker.get(jjj).getName().equalsIgnoreCase(nodeToReceive.get(iii).getName())){
						extendedList2.add(afterSwipeNodesReceiveWorker.get(jjj));
					}
				}
			}


			for(int a=0; a<extendedList.size(); a++){
				for(int b=0;b<afterSwipeNodesSendWorker.size();b++){
					if(extendedList.get(a).getID() == afterSwipeNodesSendWorker.get(b).getID()){
						Node n = extendedList.get(a);
						afterSwipeNodesSendWorker.remove(n);
					}
				}
				afterSwipeNodesReceiveWorker.add(extendedList.get(a));
			}

			for(int a=0; a<extendedList2.size(); a++){
				for(int b=0;b<afterSwipeNodesReceiveWorker.size();b++){
					if(extendedList2.get(a).getID() == afterSwipeNodesReceiveWorker.get(b).getID()){
						Node n = extendedList2.get(a);
						afterSwipeNodesReceiveWorker.remove(n);
					}
				}
				afterSwipeNodesSendWorker.add(extendedList2.get(a));
			}


		}


		mapNodesWorkerSolu.put(workerSend, afterSwipeNodesSendWorker);
		mapNodesWorkerSolu.put(workerReceive, afterSwipeNodesReceiveWorker);


		boolean sameTeam = workers.checkSameTeam(workerSend, workerReceive);

		if(sameTeam == false){

			Worker partnerWorkerSend = null;
			Worker partnerWorkerReceive = null;

			if(workers.getOtherWorker(workerSend.getID()) != null){
				partnerWorkerSend = new Worker(workers.getOtherWorker(workerSend.getID()));
			}
			if(workers.getOtherWorker(workerReceive.getID()) != null){
				partnerWorkerReceive = new Worker(workers.getOtherWorker(workerReceive.getID()));
			}


			if(partnerWorkerSend != null){

				List<Node> nodesSoluBegin = new ArrayList<Node>(firstSolution.getRouteNodesByWorker(partnerWorkerSend));

				for(int i=0;i<nodeToSend.size();i++){
					for(int j=0;j<nodesSoluBegin.size();j++){
						if(nodeToSend.get(i).getType() == 1){
							if(nodesSoluBegin.get(j).getID() == nodeToSend.get(i).getID()){
								nodesSoluBegin.remove(nodesSoluBegin.get(j));
							}
						}
					}
				}

				for(int i=0;i<nodeToReceive.size();i++){
					if(nodeToReceive.get(i).getType() == 1){
						nodesSoluBegin.add(nodeToReceive.get(i));
					}
				}


				for(int a=0; a<extendedList.size(); a++){
					for(int b=0;b<nodesSoluBegin.size();b++){
						if(extendedList.get(a).getID() == nodesSoluBegin.get(b).getID()){
							Node n = extendedList.get(a);
							nodesSoluBegin.remove(n);
						}
					}
				}

				for(int a=0; a<extendedList2.size(); a++){
					nodesSoluBegin.add(extendedList2.get(a));
				}

				partnerWorkerSend.clearRoute();

				workersToReRoute.add(partnerWorkerSend);
				mapNodesWorkerSolu.put(partnerWorkerSend,nodesSoluBegin);
			}

			if(partnerWorkerReceive != null){

				List<Node> nodesSoluBegin = new ArrayList<Node>(firstSolution.getRouteNodesByWorker(partnerWorkerReceive));


				for(int i=0;i<nodeToReceive.size();i++){
					for(int j=0;j<nodesSoluBegin.size();j++){
						if(nodeToReceive.get(i).getType() == 1){
							if(nodesSoluBegin.get(j).getID() == nodeToReceive.get(i).getID()){
								nodesSoluBegin.remove(nodesSoluBegin.get(j));
							}
						}
					}
				}

				for(int i=0;i<nodeToSend.size();i++){
					if(nodeToSend.get(i).getType() == 1){
						nodesSoluBegin.add(nodeToSend.get(i));
					}
				}


				for(int a=0; a<extendedList2.size(); a++){
					for(int b=0;b<nodesSoluBegin.size();b++){
						if(extendedList2.get(a).getID() == nodesSoluBegin.get(b).getID()){
							Node n = extendedList2.get(a);
							nodesSoluBegin.remove(n);
						}
					}
				}

				for(int a=0; a<extendedList.size(); a++){
					nodesSoluBegin.add(extendedList.get(a));
				}

				partnerWorkerReceive.clearRoute();

				workersToReRoute.add(partnerWorkerReceive);
				mapNodesWorkerSolu.put(partnerWorkerReceive, nodesSoluBegin);
			}

		}



		boolean stop = false;
		int noArc = 0;			//Contador para verificar se não foram feitas alterações numa equipa
		boolean[] checkWorkerIteration = new boolean[workersToReRoute.size()]; //Verificar que um worker não escolhe duas visitas na mesma virada
		List<Arc> lastArcsTried = new ArrayList<Arc>(workersToReRoute.size()); //evitar que se itere infinitamente sobre o mesmo arco

		for(int i=0;i<workersToReRoute.size();i++){
			lastArcsTried.add(i, null);
		}

		while(stop != true) {	//Enquanto houver alterações a serem feitas, o ciclo continua

			if(noArc == workersToReRoute.size()){ //Parar ciclo quando todas as rotas tiverem sido geradas
				stop = true;
				break;
			}


			for(int i=0;i<workersToReRoute.size();i++){
				workersToReRoute.get(i).setWorkerIterationCheck(false);
			}

			for(int i=0;i<workersToReRoute.size();i++){

				if( (workersToReRoute.get(i).isRouteFinished() == false) && (checkWorkerIteration[i] == false) ){ //Se a rota do worker não tiver terminado

					if( ( (workersToReRoute.get(i).getCurrentDayTime()) - (workersToReRoute.get(i).getStartTime()) ) >= workersToReRoute.get(i).getMaxTime()){
						System.out.println("Ultrapassado o máximo de horas definido, na team: "+i);
						stop = true;
						break;
					}

					else{ //Encontrar arco para a team 

						double bestCurrentTime = 999999; //Valor inicial propositadamente exagerado. O objectivo é encontrar o melhor arco (aquele cuja tarefa seja a mais próxima de puder ser efectuada)
						Arc aChosen = null;	

						for(int ii=0;ii<arcs.getSize();ii++){

							if((arcs.getArc(ii).getWorker().getID() == workersToReRoute.get(i).getID()) && (arcs.getArc(ii).getNode1().getID() == workersToReRoute.get(i).getCurrentNode().getID() )){ //Procurar arcos que partem do actual nó, e que estão associados à equipa em questão

								if(checkNodeExists(arcs.getArc(ii).getNode2(),mapNodesWorkerSolu.get(workersToReRoute.get(i))) == true){

									if( (workersToReRoute.get(i).getHadLunch() == false) || ((workersToReRoute.get(i).getHadLunch() == true) && (!arcs.getArc(ii).getNode2().getName().contains("Almoço")))){ //Garantir que uma equipa almoça no máximo uma vez

										if(arcs.getArc(ii).getWaitTime() <= maxWaitTimeBetweenTasks){

											if( (arcs.getArc(ii).getBestTime() >= 0) && (arcs.getArc(ii).getBestTime() <= bestCurrentTime) && (arcs.getArc(ii).getBestTime() >= workersToReRoute.get(i).getStartTime()) && (arcs.getArc(ii).getBestTime() <= workersToReRoute.get(i).getFinishTime()) ){ //Procurar arco com melhor tempo, entre o start e finish time da equipa	

												if (checkAlreadyExists(arcs.getArc(ii),workersToReRoute.get(i).getNodesVisitedRoute()) == false ){ //Verificar se o nó a visitar, já se encontra na solução

													bestCurrentTime = arcs.getArc(ii).getBestTime();
													aChosen = new Arc(arcs.getArc(ii));	

												}

											}
										}
									}
								}
							}
						}


						if(aChosen == null){
							noArc = noArc+1;
							workersToReRoute.get(i).setFinishedRoute(true);
						}
						else{

							if(aChosen.getNode2().getType() == 1 && aChosen.getNode2().getTypeTransport() == 0){ //acamados(sem transporte)

								if(lastArcsTried.get(i) != null){
									if(aChosen.getID() == lastArcsTried.get(i).getID()){
										noArc = noArc+1;
										workersToReRoute.get(i).setFinishedRoute(true);		
									}
								}


								lastArcsTried.add(i, aChosen);

								Worker otherWorkerAux = workers.getOtherWorker(workersToReRoute.get(i).getID());
								Worker otherWorker = null;

								if(otherWorkerAux != null){

									for(int w=0;w<workersToReRoute.size();w++){
										if(workersToReRoute.get(w).getID() == otherWorkerAux.getID()){
											otherWorker = workersToReRoute.get(w);
										}
									}


									if(otherWorker.getWorkerIterationCheck() == false){
										otherWorker.setWorkerIterationCheck(true);

										Node startNode = otherWorker.getCurrentNode();
										Node finishNode = aChosen.getNode2();

										Arc arcOther = arcs.getArcByInfo(otherWorker,startNode,finishNode);

										if(arcOther != null){
											if(arcOther.getWaitTime() <= maxWaitTimeBetweenTasks){

												if( (arcOther.getBestTime() >= 0) && (arcOther.getBestTime() <= bestCurrentTime) && (arcOther.getBestTime() >= otherWorker.getStartTime()) && (arcOther.getBestTime() <= otherWorker.getFinishTime()) ){ 

													double bestTime1 = aChosen.getBestTime();
													double bestTime2 = arcOther.getBestTime();
													double currentTime = 0;

													if(bestTime1 >= bestTime2){
														double extra = bestTime1 - bestTime2;
														arcOther.setNewBestTimeVal(bestTime1, extra);
														currentTime = bestTime1 + aChosen.getNode2().getDuration();	
													}
													else if(bestTime1 < bestTime2){
														double extra = bestTime2 - bestTime1;
														aChosen.setNewBestTimeVal(bestTime2, extra);
														currentTime = bestTime2 + aChosen.getNode2().getDuration();		
													}

													if(workersToReRoute.get(i).getCurrentVan() == null){
														Van v = setVan(workersToReRoute.get(i), aChosen.getBestTime());
														if(v==null){
															break;
														}
													}

													if(otherWorker.getCurrentVan() == null){
														Van v = setVan(otherWorker, arcOther.getBestTime());
														if(v==null){
															break;
														}
													}

													aChosen.setVanAndCapacityAtTheTime(workersToReRoute.get(i).getCurrentVan(), workersToReRoute.get(i).getCurrentVan().getCurrentCapacity());
													arcOther.setVanAndCapacityAtTheTime(otherWorker.getCurrentVan(), otherWorker.getCurrentVan().getCurrentCapacity());


													workersToReRoute.get(i).addArcToRoute(aChosen); //Adicionar arco à solução da equipa
													otherWorker.addArcToRoute(arcOther);

													workersToReRoute.get(i).setCurrentDayTime(currentTime); //Actualizar tempo da rota da equipa
													workersToReRoute.get(i).setCurrentNode(aChosen.getNode2()); //Adicionar nó actual à equipa

													otherWorker.setCurrentDayTime(currentTime);
													otherWorker.setCurrentNode(arcOther.getNode2());

												}
											}


											for(int u=0;u<arcs.getSize();u++){
												if( (arcs.getArc(u).getWorker().getID() == workersToReRoute.get(i).getID()) && (arcs.getArc(u).getNode1().getID()==aChosen.getNode2().getID()) ){
													double newBestTime = workersToReRoute.get(i).getCurrentDayTime(); 
													arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
												}
												if( (arcs.getArc(u).getWorker().getID() == otherWorker.getID()) && (arcs.getArc(u).getNode1().getID()==aChosen.getNode2().getID()) ){
													double newBestTime = otherWorker.getCurrentDayTime(); 
													arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
												}
											}
										}

									}
								}

							}

							else if(aChosen.getNode2().getType() != 1 && aChosen.getNode2().getTypeTransport() == 0){ //almoços e não acamados(sem transporte)

								if(aChosen.getNode2().getName().contains("Almoço")){
									workersToReRoute.get(i).setHadLunch();
								}

								workersToReRoute.get(i).addArcToRoute(aChosen); //Adicionar arco à solução da equipa

								double currentTime = aChosen.getBestTime()+aChosen.getNode2().getDuration(); 
								workersToReRoute.get(i).setCurrentDayTime(currentTime); //Actualizar tempo da rota da equipa
								workersToReRoute.get(i).setCurrentNode(aChosen.getNode2()); //Adicionar nó actual à equipa


								for(int u=0;u<arcs.getSize();u++){
									if( (arcs.getArc(u).getWorker().getID() == workersToReRoute.get(i).getID()) && (arcs.getArc(u).getNode1().getID()==aChosen.getNode2().getID()) ){
										double newBestTime = workersToReRoute.get(i).getCurrentDayTime(); 
										arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
									}
								}

							}
							else if(aChosen.getNode2().getTypeTransport() != 0){

								if(aChosen.getNode2().getTypeTransport() == 1){

									if(workersToReRoute.get(i).getCurrentVan() == null){
										Van v = setVan(workersToReRoute.get(i), aChosen.getBestTime());
										if(v==null){
											break;
										}
									}

									workersToReRoute.get(i).getCurrentVan().patientEnter();
									aChosen.setVanAndCapacityAtTheTime(workersToReRoute.get(i).getCurrentVan(), workersToReRoute.get(i).getCurrentVan().getCurrentCapacity());


									workersToReRoute.get(i).addArcToRoute(aChosen); //Adicionar arco à solução da equipa

									double currentTime = aChosen.getBestTime()+aChosen.getNode2().getDuration(); 
									workersToReRoute.get(i).setCurrentDayTime(currentTime); //Actualizar tempo da rota da equipa
									workersToReRoute.get(i).setCurrentNode(aChosen.getNode2()); //Adicionar nó actual à equipa


									for(int u=0;u<arcs.getSize();u++){
										if( (arcs.getArc(u).getWorker().getID() == workersToReRoute.get(i).getID()) && (arcs.getArc(u).getNode1().getID()==aChosen.getNode2().getID()) ){
											double newBestTime = workersToReRoute.get(i).getCurrentDayTime(); 
											arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
										}
									}


									ArrayList<Node> sameTWnodes = nodesDay.getNodesTWOpt1(aChosen.getNode2(),mapNodesWorkerSolu.get(workersToReRoute.get(i)) );
									boolean fullVan = false;

									while(fullVan == false){

										if(sameTWnodes.size() == 0){
											fullVan = true;
											break;
										}
										if(workersToReRoute.get(i).getCurrentVan().getCurrentCapacity() == 0){
											fullVan = true;
											break;
										}

										ArrayList<Arc> sameTWarcs = arcs.getArcsTW(workersToReRoute.get(i).getCurrentNode(),sameTWnodes,workersToReRoute.get(i));

										Arc arcMin = arcs.getMinBestTimeFromArcs(sameTWarcs);
										for(int j=0; j<sameTWnodes.size(); j++){
											if(sameTWnodes.get(j).getID() == arcMin.getNode2().getID()){
												sameTWnodes.remove(sameTWnodes.get(j));
											}
										}

										sameTWnodes.remove(arcMin.getNode2());

										if (checkAlreadyExists(arcMin,workersToReRoute.get(i).getNodesVisitedRoute()) == false ){

											if(workersToReRoute.get(i).getCurrentVan().getCurrentCapacity() > 0){

												workersToReRoute.get(i).getCurrentVan().patientEnter();
												arcMin.setVanAndCapacityAtTheTime(workersToReRoute.get(i).getCurrentVan(), workersToReRoute.get(i).getCurrentVan().getCurrentCapacity());


												workersToReRoute.get(i).addArcToRoute(arcMin); //Adicionar arco à solução da equipa

												double currentTime2 = arcMin.getBestTime()+arcMin.getNode2().getDuration(); 
												workersToReRoute.get(i).setCurrentDayTime(currentTime2); //Actualizar tempo da rota da equipa
												workersToReRoute.get(i).setCurrentNode(arcMin.getNode2()); //Adicionar nó actual à equipa

												for(int u=0;u<arcs.getSize();u++){
													if( (arcs.getArc(u).getWorker().getID() == workersToReRoute.get(i).getID()) && (arcs.getArc(u).getNode1().getID()==arcMin.getNode2().getID()) ){
														double newBestTime = workersToReRoute.get(i).getCurrentDayTime(); 
														arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
													}
												}
											}
										}
									}

									nodesDay.addNode(nodesDay.getSize(), 0, 0, "Centro Transporte Inicio", centerLati, centerLongi,0,720,0,day);
									Arc arcToCenter = new Arc(workersToReRoute.get(i).getCurrentNode(), nodesDay.getNode(nodesDay.getSize()-1), workersToReRoute.get(i), car);
									arcs.addArc(arcToCenter.getNode1(), arcToCenter.getNode2(), arcToCenter.getWorker(),car);	

									garbageNodes.add(nodesDay.getNode(nodesDay.getSize()-1));
									garbageArcs.add(arcToCenter);

									arcToCenter.setNewBestTime(workersToReRoute.get(i).getCurrentDayTime());

									workersToReRoute.get(i).addArcToRoute(arcToCenter); //Adicionar arco à solução da equipa

									double currentTime2 = arcToCenter.getBestTime()+arcToCenter.getNode2().getDuration(); 
									workersToReRoute.get(i).setCurrentDayTime(currentTime2); //Actualizar tempo da rota da equipa
									workersToReRoute.get(i).setCurrentNode(arcToCenter.getNode2()); //Adicionar nó actual à equipa


									ArrayList<Arc> arcsAdded = generateMoreArcs(arcToCenter, workersToReRoute.get(i),workersToReRoute.get(i).getNodesVisitedRoute());
									for(int a=0; a<arcsAdded.size();a++){
										garbageArcs.add(arcsAdded.get(a));
									}


									for(int u=0;u<arcs.getSize();u++){
										if( (arcs.getArc(u).getWorker().getID() == workersToReRoute.get(i).getID()) && (arcs.getArc(u).getNode1().getID()==arcToCenter.getNode2().getID()) ){
											double newBestTime = workersToReRoute.get(i).getCurrentDayTime(); 
											arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
										}
									}

								}

								else if(aChosen.getNode2().getTypeTransport() == 2){

									if(workersToReRoute.get(i).getCurrentVan() == null){
										Van v = setVan(workersToReRoute.get(i), aChosen.getBestTime());
										if(v==null){
											break;
										}
									}	

									workersToReRoute.get(i).getCurrentVan().patientEnter();
									aChosen.setVanAndCapacityAtTheTime(workers.getWorker(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());


									workersToReRoute.get(i).addArcToRoute(aChosen); //Adicionar arco à solução da equipa

									double currentTime = aChosen.getBestTime()+aChosen.getNode2().getDuration(); 
									workersToReRoute.get(i).setCurrentDayTime(currentTime); //Actualizar tempo da rota da equipa
									workersToReRoute.get(i).setCurrentNode(aChosen.getNode2()); //Adicionar nó actual à equipa


									for(int u=0;u<arcs.getSize();u++){
										if( (arcs.getArc(u).getWorker().getID() == workersToReRoute.get(i).getID()) && (arcs.getArc(u).getNode1().getID()==aChosen.getNode2().getID()) ){
											double newBestTime = workersToReRoute.get(i).getCurrentDayTime(); 
											arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
										}
									}

									nodesDay.addNode(nodesDay.getSize(), 0, 0, "Centro Transporte Inicio", centerLati, centerLongi,0,720,0,day);
									Arc arcToCenter = new Arc(workersToReRoute.get(i).getCurrentNode(), nodesDay.getNode(nodesDay.getSize()-1), workersToReRoute.get(i),car);
									arcs.addArc(arcToCenter.getNode1(), arcToCenter.getNode2(), arcToCenter.getWorker(),car);	

									garbageNodes.add(nodesDay.getNode(nodesDay.getSize()-1));
									garbageArcs.add(arcToCenter);

									arcToCenter.setNewBestTime(workersToReRoute.get(i).getCurrentDayTime());

									workersToReRoute.get(i).addArcToRoute(arcToCenter); //Adicionar arco à solução da equipa

									double currentTime2 = arcToCenter.getBestTime()+arcToCenter.getNode2().getDuration(); 
									workersToReRoute.get(i).setCurrentDayTime(currentTime2); //Actualizar tempo da rota da equipa
									workersToReRoute.get(i).setCurrentNode(arcToCenter.getNode2()); //Adicionar nó actual à equipa

									ArrayList<Arc> arcsAdded = generateMoreArcs(arcToCenter, workersToReRoute.get(i),workersToReRoute.get(i).getNodesVisitedRoute());

									for(int a=0; a<arcsAdded.size();a++){
										garbageArcs.add(arcsAdded.get(a));
									}


									for(int u=0;u<arcs.getSize();u++){
										if( (arcs.getArc(u).getWorker().getID() == workersToReRoute.get(i).getID()) && (arcs.getArc(u).getNode1().getID() == arcToCenter.getNode2().getID()) ){
											double newBestTime = workersToReRoute.get(i).getCurrentDayTime(); 
											arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
										}
									}

								}

								else if(aChosen.getNode2().getTypeTransport() == 3){

									nodesDay.addNode(nodesDay.getSize(), 0, 0, "Centro Transporte Final", centerLati, centerLongi,aChosen.getNode2().getIniTW(),720,0,day);
									Arc arcToCenter = new Arc(workersToReRoute.get(i).getCurrentNode(), nodesDay.getNode(nodesDay.getSize()-1), workersToReRoute.get(i),car);
									arcs.addArc(arcToCenter.getNode1(), arcToCenter.getNode2(), arcToCenter.getWorker(),car);	


									garbageNodes.add(nodesDay.getNode(nodesDay.getSize()-1));
									garbageArcs.add(arcToCenter);

									arcToCenter.setNewBestTime(workersToReRoute.get(i).getCurrentDayTime());		
									arcToCenter.setVanAndCapacityAtTheTime(workersToReRoute.get(i).getCurrentVan(), workersToReRoute.get(i).getCurrentVan().getCurrentCapacity());

									workersToReRoute.get(i).addArcToRoute(arcToCenter); //Adicionar arco à solução da equipa

									double currentTime2 = arcToCenter.getBestTime()+arcToCenter.getNode2().getDuration(); 
									workersToReRoute.get(i).setCurrentDayTime(currentTime2); //Actualizar tempo da rota da equipa
									workersToReRoute.get(i).setCurrentNode(arcToCenter.getNode2()); //Adicionar nó actual à equipa


									Arc centerToHome = new Arc(workersToReRoute.get(i).getCurrentNode(), aChosen.getNode2(), workersToReRoute.get(i),car);
									arcs.addArc(centerToHome.getNode1(), centerToHome.getNode2(), centerToHome.getWorker(),car);	

									garbageArcs.add(centerToHome);

									centerToHome.setNewBestTime(workersToReRoute.get(i).getCurrentDayTime());

									if(workersToReRoute.get(i).getCurrentVan() == null){
										Van v = setVan(workersToReRoute.get(i), centerToHome.getBestTime());
										if(v==null){
											break;
										}
									}
									workersToReRoute.get(i).getCurrentVan().patientEnter();

									ArrayList<Node> sameTWnodes = nodesDay.getNodesTWOpt2(aChosen.getNode2(), mapNodesWorkerSolu.get(workersToReRoute.get(i)) );
									ArrayList<Node> sameTWnodesPossible = new ArrayList<Node>();

									for(int ii=0; ii<sameTWnodes.size(); ii++){
										if(workersToReRoute.get(i).getCurrentVan().getCurrentCapacity() > 0){
											sameTWnodesPossible.add(sameTWnodes.get(ii));
											workersToReRoute.get(i).getCurrentVan().patientEnter();
										}
									}

									workersToReRoute.get(i).getCurrentVan().patientLeft();
									centerToHome.setVanAndCapacityAtTheTime(workersToReRoute.get(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());


									workersToReRoute.get(i).addArcToRoute(centerToHome); //Adicionar arco à solução da equipa

									double currentTime3 = centerToHome.getBestTime()+centerToHome.getNode2().getDuration(); 
									workersToReRoute.get(i).setCurrentDayTime(currentTime3); //Actualizar tempo da rota da equipa
									workersToReRoute.get(i).setCurrentNode(centerToHome.getNode2()); //Adicionar nó actual à equipa

									for(int u=0;u<arcs.getSize();u++){
										if( (arcs.getArc(u).getWorker().getID() == workersToReRoute.get(i).getID()) && (arcs.getArc(u).getNode1().getID()==centerToHome.getNode2().getID()) ){
											double newBestTime = workersToReRoute.get(i).getCurrentDayTime(); 
											arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
										}
									}

									boolean emptyVan = false;

									while(emptyVan == false){

										if(sameTWnodesPossible.size() == 0){
											emptyVan = true;
											break;
										}
										if(workersToReRoute.get(i).getCurrentVan().getCurrentCapacity() == workersToReRoute.get(i).getCurrentVan().getCapacity()){
											emptyVan = true;
											break;
										}

										ArrayList<Arc> sameTWarcs = arcs.getArcsTW(workersToReRoute.get(i).getCurrentNode(),sameTWnodesPossible,workersToReRoute.get(i));

										Arc arcMin = arcs.getMinBestTimeFromArcs(sameTWarcs);
										for(int j=0; j<sameTWnodesPossible.size(); j++){
											if(sameTWnodesPossible.get(j).getID() == arcMin.getNode2().getID()){
												sameTWnodesPossible.remove(sameTWnodesPossible.get(j));
											}
										}

										sameTWnodesPossible.remove(arcMin.getNode2());

										if (checkAlreadyExists(arcMin, workersToReRoute.get(i).getNodesVisitedRoute()) == false ){

											workersToReRoute.get(i).getCurrentVan().patientLeft();
											arcMin.setVanAndCapacityAtTheTime(workersToReRoute.get(i).getCurrentVan(), workersToReRoute.get(i).getCurrentVan().getCurrentCapacity());


											workersToReRoute.get(i).addArcToRoute(arcMin); //Adicionar arco à solução da equipa

											double currentTime4 = arcMin.getBestTime()+arcMin.getNode2().getDuration(); 
											workersToReRoute.get(i).setCurrentDayTime(currentTime4); //Actualizar tempo da rota da equipa
											workersToReRoute.get(i).setCurrentNode(arcMin.getNode2()); //Adicionar nó actual à equipa

											for(int u=0;u<arcs.getSize();u++){
												if( (arcs.getArc(u).getWorker().getID() == workersToReRoute.get(i).getID()) && (arcs.getArc(u).getNode1().getID()==arcMin.getNode2().getID()) ){
													double newBestTime = workersToReRoute.get(i).getCurrentDayTime(); 
													arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
												}
											}

										}

									}

								}

								else if(aChosen.getNode2().getTypeTransport() == 4){

									nodesDay.addNode(nodesDay.getSize(), 0, 0, "Centro Transporte Final", centerLati, centerLongi,aChosen.getNode2().getIniTW(),720,0,day);

									Arc arcToCenter = new Arc(workersToReRoute.get(i).getCurrentNode(), nodesDay.getNode(nodesDay.getSize()-1), workersToReRoute.get(i),car);
									arcs.addArc(arcToCenter.getNode1(), arcToCenter.getNode2(), arcToCenter.getWorker(),car);	


									garbageNodes.add(nodesDay.getNode(nodesDay.getSize()-1));
									garbageArcs.add(arcToCenter);

									arcToCenter.setNewBestTime(workersToReRoute.get(i).getCurrentDayTime());


									workersToReRoute.get(i).addArcToRoute(arcToCenter); //Adicionar arco à solução da equipa

									double currentTime2 = arcToCenter.getBestTime()+arcToCenter.getNode2().getDuration(); 
									workersToReRoute.get(i).setCurrentDayTime(currentTime2); //Actualizar tempo da rota da equipa
									workersToReRoute.get(i).setCurrentNode(arcToCenter.getNode2()); //Adicionar nó actual à equipa

									workersToReRoute.get(i).clearVan();
									Arc centerToHome = new Arc(workersToReRoute.get(i).getCurrentNode(), aChosen.getNode2(), workersToReRoute.get(i),car);
									arcs.addArc(centerToHome.getNode1(), centerToHome.getNode2(), centerToHome.getWorker(),car);	


									garbageArcs.add(centerToHome);


									centerToHome.setNewBestTime(workersToReRoute.get(i).getCurrentDayTime());	

									if(workersToReRoute.get(i).getCurrentVan() == null){
										Van v = setVan(workersToReRoute.get(i),centerToHome.getBestTime());
										if(v==null){
											break;
										}
									}
									workersToReRoute.get(i).getCurrentVan().patientEnter();
									workersToReRoute.get(i).getCurrentVan().patientLeft();

									centerToHome.setVanAndCapacityAtTheTime(workers.getWorker(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());


									workersToReRoute.get(i).addArcToRoute(centerToHome); //Adicionar arco à solução da equipa

									double currentTime3 = centerToHome.getBestTime()+centerToHome.getNode2().getDuration(); 
									workersToReRoute.get(i).setCurrentDayTime(currentTime3); //Actualizar tempo da rota da equipa
									workersToReRoute.get(i).setCurrentNode(centerToHome.getNode2()); //Adicionar nó actual à equipa


									for(int u=0;u<arcs.getSize();u++){
										if( (arcs.getArc(u).getWorker().getID() == workersToReRoute.get(i).getID()) && (arcs.getArc(u).getNode1().getID()==centerToHome.getNode2().getID()) ){
											double newBestTime = workersToReRoute.get(i).getCurrentDayTime(); 
											arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
										}
									}

								}

							}
						}
					}	
				}
			}
		}

		for(int b=0;b<garbageNodes.size();b++){
			for(int a=0;a<nodesDay.getSize();a++){	
				if(nodesDay.getNode(a).getID() == garbageNodes.get(b).getID()){
					nodesDay.remove(nodesDay.getNode(a));
				}
			}
		}

		for(int b=0;b<garbageArcs.size();b++){
			for(int a=0;a<arcs.getSize();a++){	
				if(arcs.getArc(a).getID() == garbageArcs.get(b).getID()){
					arcs.remove(arcs.getArc(a));
				}
			}
		}


		int checkR = 0;
		for(int i=0;i<workersToReRoute.size();i++){
			int sizeToCheck = mapNodesWorkerSolu.get(workersToReRoute.get(i)).size();
			int sizeGet = workersToReRoute.get(i).getNodesVisitedRouteWithoutCenterNodes().size();

			if(sizeToCheck == sizeGet){
				checkR = checkR + 1;
			}
		}


		if( checkR == workersToReRoute.size()){

			List<List<Arc>> routeWorkers = new ArrayList<List<Arc>>();
			List<Worker> workersSolu = new ArrayList<Worker>();
			List<Worker> workersAll = new ArrayList<Worker>(firstSolution.getWorkersRoutes());
			Map<Worker,ArrayList<Arc>> mapFirstSolu = new HashMap<Worker,ArrayList<Arc>>( firstSolution.getRoutesArcs());
			double totalTimeWorked = 0.0;
			Map<Worker, Double> valuesTimeWorkedByTeam = new HashMap<Worker, Double>();
			double totalTimeWaiting = 0.0;
			Map<Worker, Double> valuesTimeWaitedByTeam = new HashMap<Worker, Double>();
			double fairnessValue = 0.0;
			List<Double> timeWorkedValues = new ArrayList<Double>();

			for(int i=0;i<workersAll.size();i++){
				boolean checkW = checkWorkerWasWorked(workersAll.get(i), workersToReRoute);
				if( checkW == false ){

					List<Arc> listArcsCopy = new ArrayList<Arc>();
					List<Arc> listArcs = new ArrayList<Arc>(mapFirstSolu.get(workersAll.get(i)));

					for(int j=0;j<listArcs.size();j++){
						Arc arcCopy = new Arc(listArcs.get(j));
						listArcsCopy.add(arcCopy);
					}

					routeWorkers.add(listArcsCopy);
					workersSolu.add(workersAll.get(i));
					totalTimeWorked = totalTimeWorked + firstSolution.getValuesTimeWorkedByWorker().get(workersAll.get(i));
					timeWorkedValues.add(firstSolution.getValuesTimeWorkedByWorker().get(workersAll.get(i)));

					valuesTimeWorkedByTeam.put(workersAll.get(i), totalTimeWorked);		
					totalTimeWaiting = totalTimeWaiting + firstSolution.getValuesTimeWaitingByWorker().get(workersAll.get(i));
					valuesTimeWaitedByTeam.put(workersAll.get(i), totalTimeWaiting);
				}
			}


			for(int h=0;h<workersToReRoute.size();h++){

				workersSolu.add(workersToReRoute.get(h));

				List<Arc> listArcsCopy2 = new ArrayList<Arc>();
				List<Arc> listArcs2 = new ArrayList<Arc>(workersToReRoute.get(h).getRouteDay());

				for(int j=0;j<listArcs2.size();j++){
					Arc arcCopy2 = new Arc(listArcs2.get(j));
					listArcsCopy2.add(arcCopy2);
				}
				routeWorkers.add(listArcsCopy2);

				totalTimeWorked = totalTimeWorked + workersToReRoute.get(h).timeWorked();
				timeWorkedValues.add(workersToReRoute.get(h).timeWorked());
				valuesTimeWorkedByTeam.put(workersToReRoute.get(h), workersToReRoute.get(h).timeWorked());	

				totalTimeWaiting = totalTimeWaiting + workersToReRoute.get(h).totalWaitTime();
				valuesTimeWaitedByTeam.put(workersToReRoute.get(h), workersToReRoute.get(h).totalWaitTime());	

			}

			fairnessValue = getFairness(timeWorkedValues);


			if( totalTimeWorked < solutions.getTotalTimeSolution().getTotalTimeWorked()){	
				SolutionRoutes solutionTotalTimeWorked = new SolutionRoutes(workersSolu, routeWorkers, totalTimeWorked, valuesTimeWorkedByTeam, totalTimeWaiting, valuesTimeWaitedByTeam, fairnessValue);
				solutions.setTotalTimeSolution(solutionTotalTimeWorked);

				printCurrentSolus();
			}

			if( totalTimeWaiting < solutions.getWaitingTimeSolution().getTotalTimeWaiting()){	
				SolutionRoutes solutionTotalTimeWaiting = new SolutionRoutes(workersSolu, routeWorkers, totalTimeWorked, valuesTimeWorkedByTeam, totalTimeWaiting, valuesTimeWaitedByTeam, fairnessValue);
				solutions.setWaitingTimeSolution(solutionTotalTimeWaiting);

				printCurrentSolus();
			}

			if( fairnessValue < solutions.getFairSolution().getFairnessValue()){	
				SolutionRoutes solutionFairness = new SolutionRoutes(workersSolu, routeWorkers, totalTimeWorked, valuesTimeWorkedByTeam, totalTimeWaiting, valuesTimeWaitedByTeam, fairnessValue);
				solutions.setFairSolution(solutionFairness);

				printCurrentSolus();
			}
			System.out.println("	ADMISSIBLE SOLUTION. Total Worked Time: "+totalTimeWorked+". Total Waiting Time: "+totalTimeWaiting+". Fairness: "+fairnessValue);
			System.out.println("");
		}	

		else{
			System.out.println("	Not admissible solution.");
			System.out.println("");
		}


	}



	private static double getFairness(List<Double> timeWorkedValues) {

		double maxValue = timeWorkedValues.get(0);
		double minValue = timeWorkedValues.get(0);

		for(int i=1;i<timeWorkedValues.size();i++){
			if(timeWorkedValues.get(i) > maxValue){
				maxValue = timeWorkedValues.get(i);
			}
			if(timeWorkedValues.get(i) < minValue){
				minValue = timeWorkedValues.get(i);
			}
		}

		double percentMin = (minValue*100.0)/maxValue;
		double value = 100.0 - percentMin;

		return value;
	}



	private static boolean checkWorkerWasWorked(Worker worker,
			List<Worker> workersToReRoute) {

		boolean found = false;

		for(int i=0; i<workersToReRoute.size(); i++){
			if(workersToReRoute.get(i).getID() == worker.getID()){
				found = true;
			}
		}

		return found;
	}



	private static boolean checkNodeExists(Node node2, List<Node> list) {

		boolean found = false;

		for(int i=0; i<list.size();i++){
			if(list.get(i).getID() == node2.getID()){
				found = true;
				return found;
			}
		}

		return false;
	}



	private static double getFairnessBed() {

		double value = 0.0;

		double mostValueBedTeamValue = workers.getMostDurationWorker().timeWorked();
		double leastValueBedTeamValue = workers.getLeastDurationWorker().timeWorked();

		double valuePercentageLeastBed = (leastValueBedTeamValue*100.0)/mostValueBedTeamValue;
		value = 100.0 - valuePercentageLeastBed;

		return value;
	}



	private static void checkEquality() {

		double mostValueBedTeamValue = workers.getMostDurationWorker().timeWorked();
		double leastValueBedTeamValue = workers.getLeastDurationWorker().timeWorked();

		double valuePercentageLeastBed = (leastValueBedTeamValue*100.0)/mostValueBedTeamValue;

		System.out.println("Values Worked: Most: "+mostValueBedTeamValue+". Least: "+leastValueBedTeamValue);



		System.out.println("ROUTES FAIRNESS -> Percentage: "+(100.0 - valuePercentageLeastBed));

	}



	private static List<Node> makeInitialSolution(boolean sameTeamCond) throws InterruptedException {

		List<Node> nodesSolution = new ArrayList<Node>();
		nodesSolution.clear();

		boolean stop = false;
		int noArc = 0;			//Contador para verificar se não foram feitas alterações num worker

		boolean[] checkWorkerIteration = new boolean[totalWorkers]; //Verificar que um worker não escolhe duas visitas na mesma virada
		List<Arc> lastArcsTried = new ArrayList<Arc>(totalWorkers); //evitar que se itere infinitamente sobre o mesmo arco

		for(int i=0;i<totalWorkers;i++){
			lastArcsTried.add(i, null);
		}


		while(stop != true) {	//Enquanto houver alterações a serem feitas, o ciclo continua

			if(noArc == totalWorkers){ //Parar ciclo quando todas as rotas tiverem sido geradas
				stop = true;
				break;
			}


			for(int i=0;i<totalWorkers;i++){
				checkWorkerIteration[i] = false;
			}


			for(int i=0;i<totalWorkers;i++){
				Thread.sleep(1);

				if( (workers.getWorker(i).isRouteFinished() == false) && (checkWorkerIteration[i] == false) ){ //Se a rota do worker não tiver terminado

					if( ( (workers.getWorker(i).getCurrentDayTime()) - (workers.getWorker(i).getStartTime()) ) > workers.getWorker(i).getMaxTime()){
						System.out.println("Ultrapassado o máximo de horas definido, no worker: "+i);
						stop = true;
						break;
					}

					else{ //Encontrar arco para a team 

						double bestCurrentTime = 999999; //Valor inicial propositadamente exagerado. O objectivo é encontrar o melhor arco (aquele cuja tarefa seja a mais próxima de puder ser efectuada)
						Arc aChosen = null;	

						Worker otherWorker = workers.getOtherWorker(i);
						boolean checkOnlyType2 = false;

						if(otherWorker != null){
							if(checkWorkerIteration[otherWorker.getID()] == true){
								checkOnlyType2 = true;
							}
						}

						for(int a=0;a<arcs.getSize();a++){

							if((arcs.getArc(a).getWorker().getID() == i) && (arcs.getArc(a).getNode1().getID() == workers.getWorker(i).getCurrentNode().getID() )){ //Procurar arcos que partem do actual nó, e que estão associados à equipa em questão

								if( (workers.getWorker(i).getHadLunch() == false) || ((workers.getWorker(i).getHadLunch() == true) && (!arcs.getArc(a).getNode2().getName().contains("Almoço")))){ //Garantir que uma equipa almoça no máximo uma vez

									if(arcs.getArc(a).getWaitTime() <= maxWaitTimeBetweenTasks){

										if( (arcs.getArc(a).getBestTime() >= 0) && (arcs.getArc(a).getBestTime() <= bestCurrentTime) && (arcs.getArc(a).getBestTime() >= workers.getWorker(i).getStartTime()) && (arcs.getArc(a).getBestTime() <= workers.getWorker(i).getFinishTime()) ){ //Procurar arco com melhor tempo, entre o start e finish time da equipa

											if (checkAlreadyExists(arcs.getArc(a),nodesSolution) == false ){ //Verificar se o nó a visitar, já se encontra na solução

												Patient patientNode = arcs.getArc(a).getNode2().getPatient();//Utente associado ao arco
												if(patientNode == null){ //Quando não tem um paciente associado(almoço)
													bestCurrentTime = arcs.getArc(a).getBestTime();
													aChosen = arcs.getArc(a);	

												}
												else{ //Tarefas normais
													if( (sameTeamCond == true && patientNode.getTeam() != null && arcs.getArc(a).getNode2().getPatient().getTeam() == workers.getWorker(i).getTeam()) || (sameTeamCond == false) || ( (sameTeamCond == true) && (patientNode.getTeam() == null) ) ){ 

														if(checkOnlyType2 == true){
															if(arcs.getArc(a).getNode2().getType() == 2){
																bestCurrentTime = arcs.getArc(a).getBestTime();
																aChosen = arcs.getArc(a);
															}
														}
														else{
															bestCurrentTime = arcs.getArc(a).getBestTime();
															aChosen = arcs.getArc(a);	
														}
													}
												}
											}
										}
									}
								}
							}

						}

						if(aChosen == null){
							System.out.println("No arc chosen for worker "+workers.getWorker(i).getID());
							noArc = noArc+1;
							workers.getWorker(i).setFinishedRoute(true);
						}
						else{

							if(aChosen.getNode2().getType() != 1 && aChosen.getNode2().getTypeTransport() == 0){ //almoços e não acamados(sem transporte)
								System.out.println("Arc chosen for worker "+workers.getWorker(i).getID()+"--"+aChosen.getID()+"--"+aChosen.getBestTime());
								checkWorkerIteration[i] = true;

								aChosen.getNode2().setVisitTeam(workers.getWorker(i).getTeam());

								setTeamByPatient(aChosen.getNode2().getPatient(), workers.getWorker(i));

								if(aChosen.getNode2().getName().contains("Almoço")){
									workers.getWorker(i).setHadLunch();

									workers.getWorker(i).clearVan();
								}

								if(workers.getWorker(i).getCurrentVan() == null){
									Van v = setVan(workers.getWorker(i),aChosen.getBestTime());
									if(v==null){
										break;
									}
								}

								addArcToSolution(aChosen, workers.getWorker(i));								
								nodesSolution.add(aChosen.getNode2()); //Adicionar nó à solução de nós

								updateArcsBestTimes(aChosen, workers.getWorker(i));

							}

							else if(aChosen.getNode2().getType() == 1 && aChosen.getNode2().getTypeTransport() == 0){ //acamados(sem transporte)

								System.out.println("Arc chosen for worker "+workers.getWorker(i).getID()+"--"+aChosen.getID()+"--"+aChosen.getBestTime());

								if(lastArcsTried.get(i) != null){
									if(aChosen.getID() == lastArcsTried.get(i).getID()){
										noArc = noArc+1;
										workers.getWorker(i).setFinishedRoute(true);		
									}
								}

								lastArcsTried.add(i, aChosen);

								if(otherWorker != null){
									if(checkWorkerIteration[otherWorker.getID()] == false){
										checkWorkerIteration[otherWorker.getID()] = true;
										Node startNode = otherWorker.getCurrentNode();
										Node finishNode = aChosen.getNode2();

										Arc arcOther = arcs.getArcByInfo(otherWorker,startNode,finishNode);

										if(arcOther != null){
											if(arcOther.getWaitTime() <= maxWaitTimeBetweenTasks){

												if( (arcOther.getBestTime() >= 0) && (arcOther.getBestTime() <= bestCurrentTime) && (arcOther.getBestTime() >= otherWorker.getStartTime()) && (arcOther.getBestTime() <= otherWorker.getFinishTime()) ){ 

													double bestTime1 = aChosen.getBestTime();
													double bestTime2 = arcOther.getBestTime();

													if(bestTime1 >= bestTime2){
														double extra = bestTime1 - bestTime2;
														arcOther.setNewBestTimeVal(bestTime1, extra);
													}
													else if(bestTime1 < bestTime2){
														double extra = bestTime2 - bestTime1;
														aChosen.setNewBestTimeVal(bestTime2, extra);	
													}

													if(workers.getWorker(i).getCurrentVan() == null){
														Van v = setVan(workers.getWorker(i), aChosen.getBestTime());
														if(v==null){
															break;
														}
													}

													if(otherWorker.getCurrentVan() == null){

														Van v = setVan(otherWorker, arcOther.getBestTime());
														if(v==null){
															break;
														}

													}

													aChosen.setVanAndCapacityAtTheTime(workers.getWorker(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());
													arcOther.setVanAndCapacityAtTheTime(otherWorker.getCurrentVan(), otherWorker.getCurrentVan().getCurrentCapacity());

													addArcToSolution(aChosen, workers.getWorker(i));	
													addArcToSolution(arcOther, otherWorker);	

													aChosen.getNode2().setVisitTeam(workers.getWorker(i).getTeam());

													setTeamByPatient(aChosen.getNode2().getPatient(), workers.getWorker(i));

													nodesSolution.add(aChosen.getNode2()); //Adicionar nó à solução de nós
												}
											}

											updateArcsBestTimes(aChosen, workers.getWorker(i));
											updateArcsBestTimes(aChosen, workers.getWorker(otherWorker.getID()));

										}
									}
								}
							}

							else if(aChosen.getNode2().getTypeTransport() != 0){

								if(aChosen.getNode2().getTypeTransport() == 1){

									if(workers.getWorker(i).getCurrentVan() == null){
										Van v = setVan(workers.getWorker(i), aChosen.getBestTime());
										if(v==null){
											break;
										}
									}

									workers.getWorker(i).getCurrentVan().patientEnter();
									aChosen.setVanAndCapacityAtTheTime(workers.getWorker(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());

									addArcToSolution(aChosen, workers.getWorker(i));

									aChosen.getNode2().setVisitTeam(workers.getWorker(i).getTeam());

									setTeamByPatient(aChosen.getNode2().getPatient(), workers.getWorker(i));

									nodesSolution.add(aChosen.getNode2()); //Adicionar nó à solução de nós

									updateArcsBestTimes(aChosen, workers.getWorker(i));
									if(otherWorker != null){		//TALVEZ NAO SEJA PRECISO
										updateArcsBestTimes(aChosen, workers.getWorker(otherWorker.getID()));
									}

									ArrayList<Node> sameTWnodes = nodesDay.getNodesTW(aChosen.getNode2());
									boolean fullVan = false;

									while(fullVan == false){

										if(sameTWnodes.size() == 0){
											fullVan = true;
											break;
										}
										if(workers.getWorker(i).getCurrentVan().getCurrentCapacity() == 0){
											fullVan = true;
											break;
										}

										ArrayList<Arc> sameTWarcs = arcs.getArcsTW(workers.getWorker(i).getCurrentNode(),sameTWnodes,workers.getWorker(i));

										Arc arcMin = arcs.getMinBestTimeFromArcs(sameTWarcs);
										for(int j=0; j<sameTWnodes.size(); j++){
											if(sameTWnodes.get(j).getID() == arcMin.getNode2().getID()){
												sameTWnodes.remove(sameTWnodes.get(j));
											}
										}

										sameTWnodes.remove(arcMin.getNode2());//TALVEZ NAO SEJA PRECISO

										if (checkAlreadyExists(arcMin,nodesSolution) == false ){

											if(workers.getWorker(i).getCurrentVan().getCurrentCapacity() > 0){

												workers.getWorker(i).getCurrentVan().patientEnter();
												arcMin.setVanAndCapacityAtTheTime(workers.getWorker(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());

												addArcToSolution(arcMin, workers.getWorker(i));

												nodesSolution.add(arcMin.getNode2()); //Adicionar nó à solução de nós

												updateArcsBestTimes(arcMin, workers.getWorker(i));

											}

										}
									}

									nodesDay.addNode(nodesDay.getSize(), 0, 0, "Centro Transporte Inicio", centerLati, centerLongi,0,720,0,day);
									Arc arcToCenter = new Arc(workers.getWorker(i).getCurrentNode(), nodesDay.getNode(nodesDay.getSize()-1), workers.getWorker(i),car);
									arcs.addArc(arcToCenter.getNode1(), arcToCenter.getNode2(), arcToCenter.getWorker(),car);	

									arcToCenter.setNewBestTime(workers.getWorker(i).getCurrentDayTime());


									addArcToSolution(arcToCenter, workers.getWorker(i));
									workers.getWorker(i).clearVan();

									nodesSolution.add(arcToCenter.getNode2()); //Adicionar nó à solução de nós

									generateMoreArcs(arcToCenter, workers.getWorker(i),nodesSolution);

									updateArcsBestTimes(arcToCenter, workers.getWorker(i));

								}

								else if(aChosen.getNode2().getTypeTransport() == 2){


									if(workers.getWorker(i).getCurrentVan() == null){
										Van v = setVan(workers.getWorker(i), aChosen.getBestTime());
										if(v==null){
											break;
										}
									}	

									workers.getWorker(i).getCurrentVan().patientEnter();
									aChosen.setVanAndCapacityAtTheTime(workers.getWorker(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());

									addArcToSolution(aChosen, workers.getWorker(i));

									aChosen.getNode2().setVisitTeam(workers.getWorker(i).getTeam());

									setTeamByPatient(aChosen.getNode2().getPatient(), workers.getWorker(i));

									nodesSolution.add(aChosen.getNode2()); //Adicionar nó à solução de nós

									updateArcsBestTimes(aChosen, workers.getWorker(i));
									if(otherWorker != null){
										updateArcsBestTimes(aChosen, workers.getWorker(otherWorker.getID()));
									}


									nodesDay.addNode(nodesDay.getSize(), 0, 0, "Centro Transporte Inicio", centerLati, centerLongi,0,720,0,day);
									Arc arcToCenter = new Arc(workers.getWorker(i).getCurrentNode(), nodesDay.getNode(nodesDay.getSize()-1), workers.getWorker(i),car);
									arcs.addArc(arcToCenter.getNode1(), arcToCenter.getNode2(), arcToCenter.getWorker(),car);	

									arcToCenter.setNewBestTime(workers.getWorker(i).getCurrentDayTime());


									addArcToSolution(arcToCenter, workers.getWorker(i));
									workers.getWorker(i).clearVan();

									nodesSolution.add(arcToCenter.getNode2()); //Adicionar nó à solução de nós

									generateMoreArcs(arcToCenter, workers.getWorker(i),nodesSolution);

									updateArcsBestTimes(arcToCenter, workers.getWorker(i));

								}
								else if(aChosen.getNode2().getTypeTransport() == 3){

									nodesDay.addNode(nodesDay.getSize(), 0, 0, "Centro Transporte Final", centerLati, centerLongi,aChosen.getNode2().getIniTW(),720,0,day);
									Arc arcToCenter = new Arc(workers.getWorker(i).getCurrentNode(), nodesDay.getNode(nodesDay.getSize()-1), workers.getWorker(i),car);
									arcs.addArc(arcToCenter.getNode1(), arcToCenter.getNode2(), arcToCenter.getWorker(),car);	

									arcToCenter.setNewBestTime(workers.getWorker(i).getCurrentDayTime());	

									arcToCenter.setVanAndCapacityAtTheTime(workers.getWorker(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());

									addArcToSolution(arcToCenter, workers.getWorker(i));

									nodesSolution.add(arcToCenter.getNode2()); //Adicionar nó à solução de nós

									workers.getWorker(i).clearVan();

									ArrayList<Node> sameTWnodes = nodesDay.getNodesTW2(aChosen.getNode2());
									ArrayList<Node> sameTWnodesPossible = new ArrayList<Node>();

									Arc centerToHome = new Arc(workers.getWorker(i).getCurrentNode(), aChosen.getNode2(), workers.getWorker(i),car);
									arcs.addArc(centerToHome.getNode1(), centerToHome.getNode2(), centerToHome.getWorker(),car);	

									centerToHome.setNewBestTime(workers.getWorker(i).getCurrentDayTime());

									if(workers.getWorker(i).getCurrentVan() == null){
										Van v = setVan(workers.getWorker(i), centerToHome.getBestTime());
										if(v==null){
											break;
										}
									}
									workers.getWorker(i).getCurrentVan().patientEnter();

									for(int ii=0; ii<sameTWnodes.size(); ii++){
										if(workers.getWorker(i).getCurrentVan().getCurrentCapacity() > 0){
											sameTWnodesPossible.add(sameTWnodes.get(ii));
											workers.getWorker(i).getCurrentVan().patientEnter();
										}
									}

									workers.getWorker(i).getCurrentVan().patientLeft();
									centerToHome.setVanAndCapacityAtTheTime(workers.getWorker(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());

									addArcToSolution(centerToHome, workers.getWorker(i));

									nodesSolution.add(centerToHome.getNode2()); //Adicionar nó à solução de nós

									updateArcsBestTimes(centerToHome, workers.getWorker(i));


									boolean emptyVan = false;

									while(emptyVan == false){

										if(sameTWnodesPossible.size() == 0){
											emptyVan = true;
											break;
										}
										if(workers.getWorker(i).getCurrentVan().getCurrentCapacity() == workers.getWorker(i).getCurrentVan().getCapacity()){
											emptyVan = true;
											break;
										}

										ArrayList<Arc> sameTWarcs = arcs.getArcsTW(workers.getWorker(i).getCurrentNode(),sameTWnodesPossible,workers.getWorker(i));

										Arc arcMin = arcs.getMinBestTimeFromArcs(sameTWarcs);
										for(int j=0; j<sameTWnodesPossible.size(); j++){

											if(sameTWnodesPossible.get(j).getID() == arcMin.getNode2().getID()){
												sameTWnodesPossible.remove(sameTWnodesPossible.get(j));
											}
										}

										if (checkAlreadyExists(arcMin,nodesSolution) == false ){

											workers.getWorker(i).getCurrentVan().patientLeft();
											arcMin.setVanAndCapacityAtTheTime(workers.getWorker(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());

											addArcToSolution(arcMin, workers.getWorker(i));

											nodesSolution.add(arcMin.getNode2()); //Adicionar nó à solução de nós

											updateArcsBestTimes(arcMin, workers.getWorker(i));

											sameTWnodesPossible.remove(arcMin.getNode2()); 

										}
									}

								}
								else if(aChosen.getNode2().getTypeTransport() == 4){

									nodesDay.addNode(nodesDay.getSize(), 0, 0, "Centro Transporte Final", centerLati, centerLongi,aChosen.getNode2().getIniTW(),720,0,day);

									Arc arcToCenter = new Arc(workers.getWorker(i).getCurrentNode(), nodesDay.getNode(nodesDay.getSize()-1), workers.getWorker(i),car);

									arcs.addArc(arcToCenter.getNode1(), arcToCenter.getNode2(), arcToCenter.getWorker(),car);	

									arcToCenter.setNewBestTime(workers.getWorker(i).getCurrentDayTime());

									arcToCenter.setVanAndCapacityAtTheTime(workers.getWorker(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());

									addArcToSolution(arcToCenter, workers.getWorker(i));

									nodesSolution.add(arcToCenter.getNode2()); //Adicionar nó à solução de nós


									workers.getWorker(i).clearVan();
									Arc centerToHome = new Arc(workers.getWorker(i).getCurrentNode(), aChosen.getNode2(), workers.getWorker(i),car);
									arcs.addArc(centerToHome.getNode1(), centerToHome.getNode2(), centerToHome.getWorker(),car);	

									centerToHome.setNewBestTime(workers.getWorker(i).getCurrentDayTime());	

									if(workers.getWorker(i).getCurrentVan() == null){
										Van v = setVan(workers.getWorker(i),centerToHome.getBestTime());
										if(v==null){
											break;
										}
									}
									workers.getWorker(i).getCurrentVan().patientEnter();
									workers.getWorker(i).getCurrentVan().patientLeft();

									centerToHome.setVanAndCapacityAtTheTime(workers.getWorker(i).getCurrentVan(), workers.getWorker(i).getCurrentVan().getCurrentCapacity());

									addArcToSolution(centerToHome, workers.getWorker(i));

									nodesSolution.add(centerToHome.getNode2()); //Adicionar nó à solução de nós
									workers.getWorker(i).getCurrentVan().patientLeft();

									updateArcsBestTimes(centerToHome, workers.getWorker(i));

								}
							}

						}
					}
				}
			}
		}
		return nodesSolution;
	}



	private static ArrayList<Arc> generateMoreArcs(Arc arcToCenter, Worker w, List<Node> nodesSolution ) {

		List<Worker> workersTeam = w.getTeam().getWorkers();
		for(int i=0; i<workersTeam.size();i++){
		}

		ArrayList<Arc> arcsAdded = new ArrayList<Arc>();

		for(int i=1; i<nodesDay.getSize(); i++){
			for(int kk=0;kk<workersTeam.size();kk++){
				Arc a = new Arc(arcToCenter.getNode2(),nodesDay.getNode(i),workersTeam.get(kk),car);
				if (checkAlreadyExists(a,nodesSolution) == false ){
					arcs.addArc(a.getNode1(),a.getNode2(),a.getWorker(),car);
					arcsAdded.add(a);
				}
			}
		}
		return arcsAdded;
	}



	private static void updateArcsBestTimes(Arc aChosen, Worker worker) {

		for(int u=0;u<arcs.getSize();u++){
			if( (arcs.getArc(u).getWorker().getID() == worker.getID()) && (arcs.getArc(u).getNode1().getID()==aChosen.getNode2().getID()) ){
				double newBestTime = workers.getWorker(worker.getID()).getCurrentDayTime(); 
				arcs.getArc(u).setNewBestTime(newBestTime); //Actualizar melhores tempos possiveis dos próximos possiveis nos a visitar
			}
		}

	}



	private static void addArcToSolution(Arc aChosen, Worker worker) {

		workers.getWorker(worker.getID()).addArcToRoute(aChosen); //Adicionar arco à solução da equipa
		double currentTime = aChosen.getBestTime()+aChosen.getNode2().getDuration(); 
		workers.getWorker(worker.getID()).setCurrentDayTime(currentTime); //Actualizar tempo da rota da equipa
		workers.getWorker(worker.getID()).setCurrentNode(aChosen.getNode2()); //Adicionar nó actual à equipa	


		workers.getWorker(worker.getID()).getCurrentVan().setCurrentTime(currentTime);
		aChosen.setVanAndCapacityAtTheTime(workers.getWorker(worker.getID()).getCurrentVan(), workers.getWorker(worker.getID()).getCurrentVan().getCurrentCapacity());

	}



	private static Van setVan(Worker worker, double bestTime) {

		Van v = vans.getAvailableVan(bestTime);
		if(v == null){
			return null;
		}
		else{
			vans.getVan(v.getIDvan()).setActive(true);
			worker.setCurrentVan(v);
			return v;
		}

	}



	private static void setTeamByPatient(Patient patient, Worker worker) {

		if(patient != null){
			patient.setTeam(workers.getWorker(worker.getID()).getTeam()); //Associar equipa ao paciente visitado (so para os nos que nao são almoço e preparação)
		}

	}



	private static boolean checkAlreadyExists(Arc arc, List<Node> nodesC) {
		boolean check = false;

		for(int i=0;i<nodesC.size();i++){
			if(nodesC.get(i).getID() == arc.getNode2().getID()){
				check = true;
				return check;
			}
		}
		return check;
	}



	private void setup(boolean doLunch) {

		setupTimesAndTeams();

		for(int i=0;i<totalWorkers;i++){
			workers.addWorker(i,"Worker"+i);
		}

		Queue<Worker> queueWorkers = new LinkedList<Worker>(workers.getAllWorkers());

		for(int i=0;i<totalTeams;i++){
			Worker w1 = queueWorkers.poll();
			Worker w2 = queueWorkers.poll();

			if(w1 != null){
				teams.getTeam(i).addWorker(w1);	
				workers.getWorker(w1.getID()).setTeam(teams.getTeam(i));
			}

			if(w2 != null){
				teams.getTeam(i).addWorker(w2);
				workers.getWorker(w2.getID()).setTeam(teams.getTeam(i));
			}

		}


		//Adicionar nós relativos ao almoço (dependente das equipas geradas)
		if(doLunch == true){ 

			int iniCycle = nodesDay.getSize();
			int finCycle;
			if(afternoonPeriodStartTime < lunchConsideredToTeamsStartingWorkingBeforeEqual){ //Se os trabalhadores da tarde começarem antes das 13h, irão ser considerados para almoços
				finCycle = nodesDay.getSize()+totalWorkers;
			}
			else{
				finCycle = nodesDay.getSize()+workers.getWorkersByPeriod(1).size();	
			}
			int indexAlmoco = 1;

			for(int i=iniCycle;i<finCycle;i++){
				int startLunch = lunchTime;
				int finishLuch = startLunch + 120;
				if(afternoonPeriodFinishTime > startLunch){
					nodesDay.addNode(i, 0, 0, "Almoço"+indexAlmoco, centerLati, centerLongi,startLunch,finishLuch,60,day);
					indexAlmoco++;
				}
			}

		}

		System.out.println("--------------------------------------");
		System.out.println("ALL POSSIBLE NODES");
		System.out.println("--------------------------------------");

		for(int i=0;i<nodesDay.getSize();i++){

			System.out.println("Node with id: "+nodesDay.getNode(i).getID()+". Name: "+nodesDay.getNode(i).getName()+". Type: "+nodesDay.getNode(i).getType()
					+". TW: "+nodesDay.getNode(i).getIniTW()+" to "+nodesDay.getNode(i).getFinTW());

		}	

		addArcs();


	}



	private void addArcs() {

		for(int i = 0; i<nodesDay.getSize(); i++){

			for(int j = 1; j<nodesDay.getSize(); j++){

				if(i != j){

					double twi_duNode1 = nodesDay.getNode(i).getIniTW() + nodesDay.getNode(i).getDuration();
					double twfNode2 = nodesDay.getNode(j).getFinTW();

					if(twi_duNode1 < twfNode2){ //Verificar admissibilidade do arco a criar, tendo em conta time-windows dos dois nós

						for(int k=0;k<teams.getSize();k++){
							if(teams.getTeam(k).getPeriodType() == 1){ 
								if((nodesDay.getNode(j).getIniTW() + nodesDay.getNode(j).getDuration()) >= morningPeriodStartTime  && (nodesDay.getNode(j).getIniTW() + nodesDay.getNode(j).getDuration()) <= morningPeriodFinishTime) {
									List<Worker> workersTeam = teams.getTeam(k).getWorkers();
									for(int kk=0;kk<workersTeam.size();kk++){
										if(nodesDay.getNode(i).getTypeTransport() != 3 && nodesDay.getNode(i).getTypeTransport() != 4 && (nodesDay.getNode(j).getTypeTransport() == 3 || nodesDay.getNode(j).getTypeTransport() == 4) ){
											double distanceToCenter = arcs.getDistanceNodes(nodesDay.getNode(0), nodesDay.getNode(j));
											arcs.addArcTranspEnd(nodesDay.getNode(i),nodesDay.getNode(j),workersTeam.get(kk),distanceToCenter, car);	
										}
										else{
											arcs.addArc(nodesDay.getNode(i),nodesDay.getNode(j),workersTeam.get(kk),car);	
										}
									}
								}
							}
							else if(teams.getTeam(k).getPeriodType() == 2){
								if((nodesDay.getNode(j).getIniTW() + nodesDay.getNode(j).getDuration()) >= afternoonPeriodStartTime  && (nodesDay.getNode(j).getIniTW() + nodesDay.getNode(j).getDuration()) <= afternoonPeriodFinishTime) {
									List<Worker> workersTeam = teams.getTeam(k).getWorkers();
									for(int kk=0;kk<workersTeam.size();kk++){
										if(nodesDay.getNode(i).getTypeTransport() != 3 && nodesDay.getNode(i).getTypeTransport() != 4 && (nodesDay.getNode(j).getTypeTransport() == 3 ||nodesDay.getNode(j).getTypeTransport() == 4) ){
											double distanceToCenter = arcs.getDistanceNodes(nodesDay.getNode(0), nodesDay.getNode(j));
											arcs.addArcTranspEnd(nodesDay.getNode(i),nodesDay.getNode(j),workersTeam.get(kk),distanceToCenter, car);	
										}
										else{
											arcs.addArc(nodesDay.getNode(i),nodesDay.getNode(j),workersTeam.get(kk),car);	
										}
									}
								}
							}		
						}

					}

				}
			}
		}

		arcs.sort();

		//				System.out.println("--------------------------------------");
		//				System.out.println("ALL POSSIBLE ARCS:");
		//				System.out.println("--------------------------------------");
		//		
		//				for(int i=0;i<arcs.getSize();i++){
		//					System.out.println("Arc n." + arcs.getArc(i).getID() + " with nodes: " + arcs.getArc(i).getNode1().getName() +"("+arcs.getArc(i).getNode1().getID()+")" + " and " + arcs.getArc(i).getNode2().getName() +"("+arcs.getArc(i).getNode2().getID()+")"
		//							+ " with distance: " + arcs.getArc(i).getDistance() + " and time window from " + arcs.getArc(i).getNode2().getIniTW() 
		//							+ " to " + arcs.getArc(i).getNode2().getFinTW() + " with duration of: " + arcs.getArc(i).getNode2().getDuration() + " and best time: " + arcs.getArc(i).getBestTime()
		//							+ ". Worker to visit: " + arcs.getArc(i).getWorker().getName());
		//				}

	}



	private void setupTimesAndTeams() {

		for(int j=0;j<nodesDay.getSize();j++){
			double iniTW = nodesDay.getNode(j).getIniTW();
			double finTW = nodesDay.getNode(j).getFinTW();

			if( ((iniTW >= morningPeriodStartTime) &&(finTW <= morningPeriodFinishTime)) || ((finTW >= morningPeriodStartTime) &&(finTW <= morningPeriodFinishTime)) || ((iniTW >= morningPeriodStartTime) &&(iniTW <= morningPeriodFinishTime)) || ((iniTW <= morningPeriodStartTime) &&(finTW >= morningPeriodFinishTime))  ){
				if(!(nodesDay.getNode(j).getName().contains("Centro"))){
					teams.addMorningNode(nodesDay.getNode(j)); //distribuir nos pelo periodo de manha
				}
			}

			if( ((iniTW >= afternoonPeriodStartTime) &&(finTW <= afternoonPeriodFinishTime)) || ((finTW >= afternoonPeriodStartTime) &&(finTW <= afternoonPeriodFinishTime)) || ((iniTW >= afternoonPeriodStartTime) &&(iniTW <= afternoonPeriodFinishTime)) || ((iniTW <= afternoonPeriodStartTime) &&(finTW >= afternoonPeriodFinishTime))  ){
				if(!(nodesDay.getNode(j).getName().contains("Centro"))){
					teams.addAfternoonNode(nodesDay.getNode(j)); //distribuir nos pelo periodo de tarde
				}
			}
		}

		System.out.println("");
		System.out.println("Msize: "+teams.getMorningNodes().size()+"("+teams.durationTasksMorning()+")"+"__Tsize: "+teams.getAfternoonNodes().size()+"("+teams.durationTasksAfternoon()+")");
		System.out.println("Morning Period: "+morningPeriodStartTime+" - "+morningPeriodFinishTime+". Afternoon Period: "+afternoonPeriodStartTime+" - "+afternoonPeriodFinishTime);

		int IDteam = 0;

		//adicionar equipas
		if(teams.getMorningNodes().size() > 0){
			teams.addTeam(IDteam, nodesDay.getNode(0), false,1, morningPeriodStartTime,morningPeriodFinishTime,maxTime,false);

			IDteam = IDteam+1;
		}

		if(teams.getAfternoonNodes().size() > 0){
			teams.addTeam(IDteam, nodesDay.getNode(0),false,2,afternoonPeriodStartTime,afternoonPeriodFinishTime,maxTime,false);

			IDteam = IDteam+1;
		}

		if( (totalTeams-teams.getSize()) > 0 ){ //Se sobrarem equipas, distribuir as restantes pelos peridos onde a duração total de tarefas é maior
			int teamsToSelect = totalTeams-teams.getSize();

			for(int h=0;h<teamsToSelect;h++){ //Metodo que verifica para todas as 4 possibilidades com o mesmo numero de nos a que tem mais tarefas para realizar...a que tiver mais, é atribuida mais uma equipa a esse tipo
				String teamm = teams.getTeamType();

				if(teamm == "M"){
					teams.addTeam(IDteam, nodesDay.getNode(0),false,1,morningPeriodStartTime,morningPeriodFinishTime,maxTime,false);
					IDteam = IDteam+1;
				}

				else if(teamm == "T"){
					teams.addTeam(IDteam, nodesDay.getNode(0),false,2,afternoonPeriodStartTime,afternoonPeriodFinishTime,maxTime,false);
					IDteam = IDteam+1;
				}

			}

		}


		System.out.println("");
		for(int t=0;t<teams.getSize();t++){
			System.out.println("Team with ID: "+teams.getTeam(t).getIDTeam()+" with PeriodType: "+teams.getTeam(t).getPeriodType());
		}

	}



	private void preSetup() {

		firstTaskTime = 1440;
		lastTaskTime = 0;

		for(int i=0;i<nodesDay.getSize();i++){ //Descobrir a primeira e ultima tarefa
			if( (nodesDay.getNode(i).getIniTW()) < firstTaskTime ){
				firstTaskTime = nodesDay.getNode(i).getIniTW();
				firstTask = nodesDay.getNode(i);
			}
			if( (nodesDay.getNode(i).getFinTW()) > lastTaskTime ){
				if(!(nodesDay.getNode(i).getName().contains("Centro"))){
					lastTaskTime = nodesDay.getNode(i).getFinTW();
					lastTask = nodesDay.getNode(i);
				}
			}
		}

		if(firstTaskTime == 0){ //Se a primeira tarefa é às 8.00h(0), o periodo da manha começa às 8, porque é a hora minima
			morningPeriodStartTime = 0;
		}
		else{ //Se a primeira tarefa é mais tarde, retornar o tempo que leva do centro ao nó da tarefa, e começar o periodo matinal 
			double newMorningPeriodStartTime = 0; //com base nisso (neste caso so faz sentido se for depois das 8.00h(0)
			Arc centerToFirstTask = getArcByNameAndNode("Centro Inicial",firstTask);
			newMorningPeriodStartTime = firstTaskTime - centerToFirstTask.getMinutesTrip();
			if(newMorningPeriodStartTime > 0){
				morningPeriodStartTime = newMorningPeriodStartTime;
			}
		}
		morningPeriodFinishTime = morningPeriodStartTime + maxTime; //atribuir tempos(iniciais(podem ser alterados mais tarde)) de inicio e fim dos horarios das equipas que trabalham de manhã e tarde

		afternoonPeriodFinishTime = lastTaskTime; //talvez otimizar aqui..
		afternoonPeriodStartTime = lastTaskTime - maxTime;

		if(afternoonPeriodStartTime < 0){
			afternoonPeriodStartTime = 0;
		}

		if(morningPeriodFinishTime > afternoonPeriodFinishTime){
			morningPeriodFinishTime = afternoonPeriodFinishTime;
		}

	}



	private Arc getArcByNameAndNode(String nameNode1, Node node2){

		for(int i=0;i<arcs.getSize();i++){
			String nameToCompareNode1 = arcs.getArc(i).getNode1().getName();
			int idToCompare = arcs.getArc(i).getNode2().getID();

			if( (nameNode1.equalsIgnoreCase(nameToCompareNode1)) && (node2.getID() == idToCompare) ) {
				return arcs.getArc(i);
			}
		}

		return null;
	}



	public static void printCurrentSolus() {

		SolutionRoutes firstSolution = solutions.getFirstSolution();
		SolutionRoutes totalTimeSolution = solutions.getTotalTimeSolution();
		SolutionRoutes waitingTimeSolution = solutions.getWaitingTimeSolution();
		SolutionRoutes fairSolution = solutions.getFairSolution();

		System.out.println("");
		System.out.println("");
		System.out.println("-----First Solution-----");
		Map<Worker,ArrayList<Arc>> routesArcsFirstSolution = firstSolution.getRoutesArcs();
		List<Worker> workersFirtSolution = firstSolution.getWorkersRoutes();
		for(int i=0;i<workersFirtSolution.size();i++){
			Worker worker = workersFirtSolution.get(i);
			List<Arc> arcs = routesArcsFirstSolution.get(worker);
			System.out.println("Worker ID: "+worker.getID());
			for(int j=0;j<arcs.size();j++){
				System.out.println("ArcID: "+arcs.get(j).getID()+" with nodes: "+arcs.get(j).getNode1().getName()+"("+arcs.get(j).getNode1().getID()+")"+" - "+arcs.get(j).getNode2().getName()+"("+arcs.get(j).getNode2().getID()+")"+" started at: "+arcs.get(j).getBestTime()+" until: "+(arcs.get(j).getBestTime()+arcs.get(j).getNode2().getDuration())+". Wait Time: "+arcs.get(j).getWaitTime());
			}
		}
		System.out.println("Total Time Worked: "+firstSolution.getTotalTimeWorked()+". Total time wait: "+firstSolution.getTotalTimeWaiting()+". Fairness: "+firstSolution.getFairnessValue());


		System.out.println("");
		System.out.println("-----Total Time Solution-----");
		Map<Worker,ArrayList<Arc>> routesArcsTimeSolution = totalTimeSolution.getRoutesArcs();
		List<Worker> workersTimeSolution = totalTimeSolution.getWorkersRoutesOrdered();

		for(int i=0;i<workersTimeSolution.size();i++){
			Worker worker = workersTimeSolution.get(i);
			List<Arc> arcs = routesArcsTimeSolution.get(worker);
			System.out.println("Worker ID: "+worker.getID());
			for(int j=0;j<arcs.size();j++){
				System.out.println("ArcID: "+arcs.get(j).getID()+" with nodes: "+arcs.get(j).getNode1().getName()+"("+arcs.get(j).getNode1().getID()+")"+" - "+arcs.get(j).getNode2().getName()+"("+arcs.get(j).getNode2().getID()+")"+" started at: "+arcs.get(j).getBestTime()+" until: "+(arcs.get(j).getBestTime()+arcs.get(j).getNode2().getDuration())+". Wait Time: "+arcs.get(j).getWaitTime());
			}
		}
		System.out.println("TOTAL TIME WORKED: "+totalTimeSolution.getTotalTimeWorked()+". Total time wait: "+totalTimeSolution.getTotalTimeWaiting()+". Fairness: "+totalTimeSolution.getFairnessValue());


		System.out.println("");
		System.out.println("-----Total Waiting Time Solution-----");
		Map<Worker,ArrayList<Arc>> routesArcsWaitingTimeSolution = waitingTimeSolution.getRoutesArcs();
		List<Worker> workersWaitingTimeSolution = waitingTimeSolution.getWorkersRoutesOrdered();

		for(int i=0;i<workersWaitingTimeSolution.size();i++){
			Worker worker = workersWaitingTimeSolution.get(i);
			List<Arc> arcs = routesArcsWaitingTimeSolution.get(worker);
			System.out.println("Worker ID: "+worker.getID());
			for(int j=0;j<arcs.size();j++){
				System.out.println("ArcID: "+arcs.get(j).getID()+" with nodes: "+arcs.get(j).getNode1().getName()+"("+arcs.get(j).getNode1().getID()+")"+" - "+arcs.get(j).getNode2().getName()+"("+arcs.get(j).getNode2().getID()+")"+" started at: "+arcs.get(j).getBestTime()+" until: "+(arcs.get(j).getBestTime()+arcs.get(j).getNode2().getDuration())+". Wait Time: "+arcs.get(j).getWaitTime());
			}
		}
		System.out.println("Total Time Worked: "+waitingTimeSolution.getTotalTimeWorked()+". TOTAL TIME WAITING: "+waitingTimeSolution.getTotalTimeWaiting()+". Fairness: "+waitingTimeSolution.getFairnessValue());


		System.out.println("");
		System.out.println("-----Fair Solution-----");
		Map<Worker,ArrayList<Arc>> routesArcsFairSolution = fairSolution.getRoutesArcs();
		List<Worker> workersFairSolution = fairSolution.getWorkersRoutesOrdered();
		for(int i=0;i<workersFairSolution.size();i++){
			Worker worker = workersFairSolution.get(i);
			List<Arc> arcs = routesArcsFairSolution.get(worker);
			System.out.println("Worker ID: "+worker.getID());
			for(int j=0;j<arcs.size();j++){
				System.out.println("ArcID: "+arcs.get(j).getID()+" with nodes: "+arcs.get(j).getNode1().getName()+"("+arcs.get(j).getNode1().getID()+")"+" - "+arcs.get(j).getNode2().getName()+"("+arcs.get(j).getNode2().getID()+")"+" started at: "+arcs.get(j).getBestTime()+" until: "+(arcs.get(j).getBestTime()+arcs.get(j).getNode2().getDuration())+". Wait Time: "+arcs.get(j).getWaitTime());
			}
		}
		System.out.println("Total Time Worked: "+fairSolution.getTotalTimeWorked()+". Total Time Waiting: "+fairSolution.getTotalTimeWaiting()+". FAIRNESS: "+fairSolution.getFairnessValue());


	}
}
