package heuristic.algo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import heuristic.objects.Arc;
import heuristic.objects.Node;
import heuristic.objects.Worker;


public class SolutionRoutes {

	List<Worker> workersRoutes;

	Map<Worker,ArrayList<Node>> routesNodes = new HashMap<Worker,ArrayList<Node>>();
	Map<Worker,ArrayList<Arc>> routesArcs = new HashMap<Worker,ArrayList<Arc>>();

	double totalTimeWorked;
	double totalTimeWaiting;
	double fairnessValue;
	
	Map<Worker, Double> valuesTimeWorkedByWorker = new HashMap<Worker, Double>();
	Map<Worker, Double> valuesTimeWaitingByWorker = new HashMap<Worker, Double>();


	public SolutionRoutes(List<Worker> workersRoutes, List<List<Arc>> routeWorkers, double totalTimeWorked, Map<Worker, Double> valuesTimeWorkedByWorker, double totalTimeWaiting, Map<Worker, Double> valuesTimeWaitingByWorker, double fairnessValue){

		this.workersRoutes = workersRoutes;
		this.totalTimeWorked = totalTimeWorked;
		this.valuesTimeWorkedByWorker = valuesTimeWorkedByWorker;
		this.totalTimeWaiting = totalTimeWaiting;
		this.valuesTimeWaitingByWorker = valuesTimeWaitingByWorker;
		this.fairnessValue = fairnessValue;


		for(int i=0;i<workersRoutes.size();i++){
			Worker worker = workersRoutes.get(i);
			ArrayList<Arc> solutionTeam = (ArrayList<Arc>) routeWorkers.get(i);

			routesArcs.put(worker, solutionTeam);

			ArrayList<Node> solutionTeamNodes = new ArrayList<Node>();
			for(int j=0;j<solutionTeam.size();j++){
				solutionTeamNodes.add(solutionTeam.get(j).getNode2());
			}
			routesNodes.put(worker, solutionTeamNodes);
		}

	}



	public List<Worker> getWorkersRoutes(){
		return workersRoutes;
	}
	
	public List<Worker> getWorkersRoutesOrdered(){
		List<Worker> workersRoutesOrdered = new ArrayList<Worker>(workersRoutes.size());
		int size = 0;
		int indexToFind = 0;

		while(size < workersRoutes.size()){
			for(int i=0;i<workersRoutes.size();i++){
				if(workersRoutes.get(i).getID() == indexToFind){
					workersRoutesOrdered.add(workersRoutes.get(i));
					size = size + 1;
					indexToFind = indexToFind + 1;
				}
			}
		}

		return workersRoutesOrdered;
	}

	public Map<Worker,ArrayList<Node>> getRoutesNodes(){
		return routesNodes;
	}
	
	public ArrayList<Node> getRouteNodesByWorker(Worker w){
		
		Worker worker = null;
		ArrayList<Node> ya = new ArrayList<Node>();
		
		for(int i=0; i<workersRoutes.size(); i++){
			if(workersRoutes.get(i).getID() == w.getID()){
				worker = workersRoutes.get(i);
			}
		}
		
		ya = routesNodes.get(worker);
		
		return ya;
				
	}

	public Map<Worker,ArrayList<Arc>> getRoutesArcs(){
		return routesArcs;
	}

	public double getTotalTimeWorked(){
		return totalTimeWorked;
	}
	
	public Map<Worker, Double> getValuesTimeWorkedByWorker(){
		return valuesTimeWorkedByWorker;
	}

	public double getTotalTimeWaiting(){
		return totalTimeWaiting;
	}
	
	public Map<Worker, Double> getValuesTimeWaitingByWorker(){
		return valuesTimeWaitingByWorker;
	}

	public double getFairnessValue(){
		return fairnessValue;
	}
}
