package heuristic.algo;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import heuristic.objects.Node;
import heuristic.repo.NodeRepository;
import heuristic.repo.PatientRepository;
import heuristic.repo.SolutionsRepository;
import heuristic.repo.VanRepository;
import main.model.Patient;
import main.model.Plan;
import main.model.Task;



public class MainAlgo {

	public static int days = 0;

	public static int totalVans = 0;
	public static int totalPatients = 0;
	public static int totalWorkers = 0;
	public static int totalTeams = 0;

	public static boolean sameTeam_samePatient_constraint;						//restri��o que determina se um paciente tem de ser visitado pela mesma equipa
	public static int maxTime;													//maximo trabalho di�rio(minutos)
	public static boolean car; 													//true -> car (2min/km) | false -> a pé (10min/km)
	public static boolean doLunch;
	public static int lunchTime;												//hora de almo�o
	public static int lunchConsideredToTeamsStartingWorkingBeforeEqual;			//v�o ser considerados almo�os para as equipas que come�am a trabalhar antes de...(em minutos)
	public static int maxWaitTimeBetweenTasks;		 							//tempo maximo de espera permitido antes da realiza��o de uma tarefa
	public static int tasksTWresize;											//aumentar (em percentagem) as janelas temporais. cada n� tem uma variavel booleana que permite, ou n�o, esta mudan�a

	public static boolean extensiveSearch;
	public static int timeoutMinutes; //minutes

	public static double centerLati;
	public static double centerLongi;

	public static PatientRepository patients;
	public static NodeRepository nodes;						//n�s(tarefas)
	public static VanRepository vans;
	public static List<Integer> vansCap = new ArrayList<Integer>();

	public static List<SolutionsRepository> solus;


	public MainAlgo(Plan p) throws InterruptedException, ExecutionException {


		MainAlgo.days = p.getDays();
		MainAlgo.totalPatients = p.getPatients().size();
		MainAlgo.totalWorkers = p.getWorkers().size();
		MainAlgo.totalVans = p.getVans().size();
		if(totalWorkers % 2 == 0){
			MainAlgo.totalTeams = totalWorkers / 2;
		}
		else{
			MainAlgo.totalTeams = (totalWorkers / 2) + 1;
		}


		MainAlgo.sameTeam_samePatient_constraint = p.isSameTeam_samePatient_constraint();
		MainAlgo.maxTime = p.getMaxTime();
		MainAlgo.car = p.getTranspCar();
		MainAlgo.doLunch = p.getLunch();
		MainAlgo.lunchTime = p.getLunchTime();
		MainAlgo.lunchConsideredToTeamsStartingWorkingBeforeEqual = 240; //Almoços para as equipas que começem a trabalhar até às 12h00
		MainAlgo.maxWaitTimeBetweenTasks = p.getMaxWaitTimeBetweenTasks();
		MainAlgo.tasksTWresize = p.getTasksTWresize();
		MainAlgo.centerLati = p.getCenterLati();
		MainAlgo.centerLongi = p.getCenterLongi();

		//ADICIONAR CARRINHAS
		for(int i=0; i<p.getVans().size(); i++){
			vansCap.add(p.getVans().get(i).getCapacity());
		}


		//ADICIONAR PACIENTES
		patients = new PatientRepository(totalPatients);
		List<Patient> patientsPlan = new LinkedList<Patient>(p.getPatients());
		for(int i=0;i<patientsPlan.size();i++){
			patients.addPatient(i, patientsPlan.get(i).getName(), patientsPlan.get(i).getLati(), patientsPlan.get(i).getLongi());
		}


		//ADICIONAR NOS
		nodes = new NodeRepository();
		int index = 0;
		for(int i = 0; i<days; i++){
			nodes.addNode(0, 0, 0, "Centro Inicial", centerLati, centerLongi,0,0,0,i);
			index = index+1;
		}

		for(int i=0;i<patientsPlan.size();i++){
			List<Task> tasksPatients = new LinkedList<Task>(patientsPlan.get(i).getTasks());
			for(int j=0; j<tasksPatients.size(); j++){
				Task t = tasksPatients.get(j);
				nodes.addTask(index, t.getTypeTask(), t.getTypeTransportTask(), patients.getPatient(patientsPlan.get(i).getIDpatient()-1), t.getIniTWTask(), t.getFinTWTask(), t.getDurationTask(), t.getChangeTWTask(), t.getDayTask());
				index = index + 1;
			}
		}


		MainAlgo.extensiveSearch = p.getExtensiveSearch();
		MainAlgo.timeoutMinutes = p.getTimeoutValue();

		solus = new ArrayList<SolutionsRepository>(MainAlgo.days);


	}

	public List<SolutionsRepository> getAllDaysSolutions(){	
		return solus;
	}


	public void doIt() throws InterruptedException, ExecutionException {


		ExecutorService service = Executors.newFixedThreadPool(1);
		MyCallable myCallable = new MyCallable();
		Future<String> futureResult = service.submit(myCallable);

		try{
			futureResult.get(timeoutMinutes, TimeUnit.MINUTES);
		}catch(TimeoutException e){
			futureResult.cancel(true);
		}
		service.shutdown();

	}

	private static final class MyCallable implements Callable<String>{

		@Override
		public String call() throws Exception {
			String builder = "run";

			try{
				boolean checkTransportTasks = nodes.checkTransportTasks();

				if(checkTransportTasks == true){
					car = true;
				}

				if(car = false){				//Adicionar Carrinhas Fictícias, no caso do tipo de transporte ser a pé
					for(int i=0; i<1000; i++){
						vansCap.add(1000);
					}
				}

				for(int i=0; i<days; i++){

					System.out.println("DAY: "+i);
					System.out.println("");
					ArrayList<Node> nodesDay = nodes.getAllNodesDay(i);

					SolutionsRepository dayS = new SolutionsRepository(new MakeDaySolution(i, (ArrayList<Integer>) vansCap, totalTeams, totalWorkers, sameTeam_samePatient_constraint, maxTime, car,  doLunch,  lunchTime, lunchConsideredToTeamsStartingWorkingBeforeEqual, maxWaitTimeBetweenTasks, tasksTWresize, extensiveSearch, nodesDay, centerLati, centerLongi).getSolus());
					solus.add(dayS);

					System.out.println("");
					System.out.println("");

				}


			}catch(InterruptedException e){

				System.out.println("");
				System.out.println("------------------------------------------------------------------------------");
				System.out.println("TIMEOUT EXCEPTION");
				System.out.println("------------------------------------------------------------------------------");
				System.out.println("");

			}



			return builder;
		}


	}


}