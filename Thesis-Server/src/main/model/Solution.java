package main.model;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Solution {

    @Id
    @GeneratedValue
	int idSolution;
	
    @ElementCollection
	Map<Integer,Route> routesArcs = new HashMap<Integer,Route>();
	
    @ElementCollection
	Map<Integer, Double> valuesTimeWorkedByWorker = new HashMap<Integer, Double>();
    
    @ElementCollection
	Map<Integer, Double> valuesTimeWaitingByWorker = new HashMap<Integer, Double>();
	
	double totalTimeWorked;
	double totalTimeWaiting;
	double fairnessValue;
	
    @OneToOne(cascade=CascadeType.MERGE)
    @JsonIgnore
	private SolutionsDay solusDay;
	
	public Solution(){
		
	}
	
	public Solution(Map<Integer, Route> routesArcs, Map<Integer, Double> valuesTimeWorkedByWorker,
			Map<Integer, Double> valuesTimeWaitingByWorker, double totalTimeWorked, double totalTimeWaiting,
			double fairnessValue) {
		this.routesArcs = routesArcs;
		this.valuesTimeWorkedByWorker = valuesTimeWorkedByWorker;
		this.valuesTimeWaitingByWorker = valuesTimeWaitingByWorker;
		this.totalTimeWorked = totalTimeWorked;
		this.totalTimeWaiting = totalTimeWaiting;
		this.fairnessValue = fairnessValue;
	}

	public Map<Integer, Route> getRoutesArcs() {
		return routesArcs;
	}

	public void setRoutesArcs(Map<Integer, Route> routesArcs) {
		this.routesArcs = routesArcs;
	}

	public Map<Integer, Double> getValuesTimeWorkedByWorker() {
		return valuesTimeWorkedByWorker;
	}

	public void setValuesTimeWorkedByWorker(Map<Integer, Double> valuesTimeWorkedByWorker) {
		this.valuesTimeWorkedByWorker = valuesTimeWorkedByWorker;
	}

	public Map<Integer, Double> getValuesTimeWaitingByWorker() {
		return valuesTimeWaitingByWorker;
	}

	public void setValuesTimeWaitingByWorker(Map<Integer, Double> valuesTimeWaitingByWorker) {
		this.valuesTimeWaitingByWorker = valuesTimeWaitingByWorker;
	}

	public double getTotalTimeWorked() {
		return totalTimeWorked;
	}

	public void setTotalTimeWorked(double totalTimeWorked) {
		this.totalTimeWorked = totalTimeWorked;
	}

	public double getTotalTimeWaiting() {
		return totalTimeWaiting;
	}

	public void setTotalTimeWaiting(double totalTimeWaiting) {
		this.totalTimeWaiting = totalTimeWaiting;
	}

	public double getFairnessValue() {
		return fairnessValue;
	}

	public void setFairnessValue(double fairnessValue) {
		this.fairnessValue = fairnessValue;
	}

	public int getIdSolution() {
		return idSolution;
	}

	public void setIdSolution(int idSolution) {
		this.idSolution = idSolution;
	}

	public SolutionsDay getSolusDay() {
		return solusDay;
	}

	public void setSolusDay(SolutionsDay solusDay) {
		this.solusDay = solusDay;
	}

	
}
