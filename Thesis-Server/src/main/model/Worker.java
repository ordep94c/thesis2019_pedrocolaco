package main.model;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Worker {

    @Id
    @GeneratedValue
	private int IDworker;
    
	private String name;
	private String dirPic;
	private int contact;
	private String email;
	private String info;
	
    @ManyToOne(cascade=CascadeType.MERGE)
    private Plan workerPlan;

    
   

	public Worker(){

	}
    
	public Worker(String name, String dirPic, int contact, String email, String info, Plan workerPlan){
		this.name = name;
		this.dirPic = dirPic;
		this.contact = contact;
		this.email = email;
		this.info = info;
		this.workerPlan = workerPlan;
	}
	
	
	
    public int getWorkerId(){
    	return IDworker;
    }
    
    public void setWorkerId(int IDworker){
    	this.IDworker = IDworker;
    }
    
    public String getNameWorker(){
    	return name;
    }

    public void setNameWorker(String name){
    	this.name = name;
    }
    
    public int getWorkerContact(){
    	return contact;
    }
    
    public void setWorkerContact(int contact){
    	this.contact = contact;
    }
    
    public String getEmailWorker(){
    	return email;
    }

    public void setEmailWorker(String email){
    	this.email = email;
    }
    
    public String getInfoWorker(){
    	return info;
    }

    public void setInfoWorker(String info){
    	this.info = info;
    }
    
    public Plan getPlanWorker(){
    	return workerPlan;
    }

    public void setPlanWorker(Plan workerPlan){
    	this.workerPlan = workerPlan;
    }

	public String getDirPic() {
		return dirPic;
	}

	public void setDirPic(String dirPic) {
		this.dirPic = dirPic;
	}

}