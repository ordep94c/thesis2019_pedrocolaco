package main.model;

import org.springframework.data.repository.CrudRepository;

public interface VanRepository extends CrudRepository<Van,Integer> {

}
