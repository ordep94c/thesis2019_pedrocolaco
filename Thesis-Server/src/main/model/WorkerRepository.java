package main.model;

import org.springframework.data.repository.CrudRepository;

public interface WorkerRepository  extends CrudRepository<Worker,Integer> {
	Worker[] findByName(String name);
}
