package main.model;

import org.springframework.data.repository.CrudRepository;

public interface ArcSoluRepository extends CrudRepository<ArcSolu,Integer>{

}
