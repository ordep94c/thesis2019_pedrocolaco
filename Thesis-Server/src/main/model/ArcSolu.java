package main.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ArcSolu {

	@Id
	@GeneratedValue
	int idArcSolu;

	String node1;
	String node2;
	double startTime;
	double finishTime;
	double waitTime;
	int type;
	int typeTransp;

	@ManyToOne(cascade=CascadeType.MERGE)
	private Route routeArc;

	ArcSolu(){

	}

	public ArcSolu(String node1, String node2, double startTime, double finishTime, double waitTime, int type, int typeTransp) {
		this.node1 = node1;
		this.node2 = node2;
		this.startTime = startTime;
		this.finishTime = finishTime;
		this.waitTime = waitTime;
		this.type = type;
		this.typeTransp = typeTransp;
	}


	public String getNode1() {
		return node1;
	}
	public void setNode1(String node1) {
		this.node1 = node1;
	}
	public String getNode2() {
		return node2;
	}
	public void setNode2(String node2) {
		this.node2 = node2;
	}
	public double getStartTime() {
		return startTime;
	}
	public void setStartTime(double startTime) {
		this.startTime = startTime;
	}
	public double getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(double finishTime) {
		this.finishTime = finishTime;
	}
	public double getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(double waitTime) {
		this.waitTime = waitTime;
	}

	public int getType(){
		return type;
	}

	public void setType(int type){
		this.type = type;
	}

	public int getTypeTransp(){
		return typeTransp;
	}

	public void setTypeTransp(int typeTransp){
		this.typeTransp = typeTransp;
	}

	public int getIdArcSolu() {
		return idArcSolu;
	}

	public void setIdArcSolu(int idArcSolu) {
		this.idArcSolu = idArcSolu;
	}

	public Route getRouteArc() {
		return routeArc;
	}

	public void setRouteArc(Route routeArc) {
		this.routeArc = routeArc;
	}

}
