package main.model;

import org.springframework.data.repository.CrudRepository;

public interface SolutionRepository extends CrudRepository<Solution,Integer> {

}
