package main.model;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {
	
    @Id
    @GeneratedValue
    private int userID;

    private String userName;
    private String password;
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy = "userPlan")
    @JsonIgnore
    List<Plan> plans;

    public User(){
    	
    }
    
    public User(String userName, String password){
    	this.userName = userName;
    	this.password = password;
    }

    public int getUserID(){
    	return userID;
    }

    public String getUserName(){
    	return userName;
    }

    public void setUserName(String userName){
    	this.userName = userName;
    }

    public String getPassword() { 
    	return password; 
    }
    
    public void setPassword(String pass){
    	this.password = pass;
    }

	public void setPlans(List<Plan> plans2) {
		this.plans = new ArrayList<Plan>(plans2);	
	}
    
    public List<Plan> getPlans(){
    	return plans;
    }
    
    public void addPlan(Plan p){
    	plans.add(p);
    }

	public Plan getPlan(int idP) {
		List<Plan> findPlan = new ArrayList<Plan>(plans);
		
		for(int i = 0; i<findPlan.size(); i++){
			
			Plan p = findPlan.get(i);
			
			if(p.getPlanId() == idP){
				return p;
			}
		}
		
		return null;
	}


}
