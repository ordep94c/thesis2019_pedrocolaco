package main.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;


@Entity
public class SolutionsDay {

    @Id
    @GeneratedValue
    private int soluID;
	
    @ManyToOne(cascade=CascadeType.MERGE)
	Plan planSolu;
	
	int day;
	
    @OneToOne(cascade=CascadeType.MERGE)
	Solution firstSolution;
    
    @OneToOne(cascade=CascadeType.MERGE)
	Solution totalTimeSolution;
    
    @OneToOne(cascade=CascadeType.MERGE)
	Solution waitingTimeSolution;
    
    @OneToOne(cascade=CascadeType.MERGE)
	Solution fairSolution;
	
	
	SolutionsDay(){
		
	}

	public SolutionsDay(Plan planSolu, int day, Solution firstSolution, Solution totalTimeSolution,
			Solution waitingTimeSolution, Solution fairSolution) {
		this.planSolu = planSolu;
		this.day = day;
		this.firstSolution = firstSolution;
		this.totalTimeSolution = totalTimeSolution;
		this.waitingTimeSolution = waitingTimeSolution;
		this.fairSolution = fairSolution;
	}



	public int getDay() {
		return day;
	}


	public void setDay(int day) {
		this.day = day;
	}


	public Solution getFirstSolution() {
		return firstSolution;
	}


	public void setFirstSolution(Solution firstSolution) {
		this.firstSolution = firstSolution;
	}


	public Solution getTotalTimeSolution() {
		return totalTimeSolution;
	}


	public void setTotalTimeSolution(Solution totalTimeSolution) {
		this.totalTimeSolution = totalTimeSolution;
	}


	public Solution getWaitingTimeSolution() {
		return waitingTimeSolution;
	}


	public void setWaitingTimeSolution(Solution waitingTimeSolution) {
		this.waitingTimeSolution = waitingTimeSolution;
	}


	public Solution getFairSolution() {
		return fairSolution;
	}


	public void setFairSolution(Solution fairSolution) {
		this.fairSolution = fairSolution;
	}

	public int getSoluID() {
		return soluID;
	}

	public void setSoluID(int soluID) {
		this.soluID = soluID;
	}

	public Plan getPlanSolu() {
		return planSolu;
	}

	public void setPlanSolu(Plan planSolu) {
		this.planSolu = planSolu;
	}
}
