package main.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Task {


	@Id
	@GeneratedValue
	private int IDtask;

	private String name;  
	private int type; //0 -> centro; 1 -> acamado; 2 -> n acamado
	private int typeTransport; //0 -> noTransport; 1 -> inicioTransporteNaoPrioritario; 2 -> inicioTransportePrioritario; 3 -> fimTransporteNaoPrioritario; 4 -> fimTransportePrioritario  
	private double lati;
	private double longi;
	private int iTW;
	private int fTW;
	private int dur;
	private boolean changeTW; // permite alargar a janela temporal inicial
	private String info;
	private int dayID;

	@ManyToOne(cascade=CascadeType.MERGE)
	private Patient patientTask;


	//	private Team visitTeam;

	public Task(){

	}

	public Task(int type, int typeTransport, String name, double lati, double longi, int iTW, int fTW, int d, int dayID) {
		this.type = type;
		this.typeTransport = typeTransport;
		this.name = name;
		this.lati = lati;
		this.longi = longi;
		this.iTW = iTW;
		this.fTW = fTW;
		this.dur = d;
		//		visitTeam = null;
		changeTW = false;

		this.dayID = dayID;
	}	

	public Task(int type, int typeTransport, int d, int e, int dur, boolean changeTW, int dayID, String info, Patient patient) {
		this.type = type;
		this.typeTransport = typeTransport;
		this.patientTask = patient;
		name = patient.getName();
		this.lati = patient.getLati();
		this.longi = patient.getLongi();
		this.iTW = d;
		this.fTW = e;
		this.dur = dur;
		//		visitTeam = null;
		this.changeTW = changeTW;
		this.info = info;
		this.dayID = dayID;
	}

	public int getIDTask() {
		return IDtask;
	}

	public void setIDTask(int IDTask){
		this.IDtask = IDTask;
	}

	public int getTypeTask() {
		return type;
	}

	public void setTypeTask(int type){
		this.type = type;
	}

	public int getTypeTransportTask() {
		return typeTransport;
	}

	public void setTypeTransportTask(int typeT){
		this.typeTransport = typeT;
	}

	public String getNameTask() {
		return name;
	}

	public void setNameTask(String name){
		this.name = name;
	}

	public double getLatiTask() {
		return lati;
	}

	public void setLatiTask(double lati){
		this.lati = lati;
	}

	public double getLongiTask() {
		return longi;
	}

	public void setLongiTask(double longi){
		this.longi = longi;
	}

	public int getIniTWTask() {
		return iTW;
	}

	public void setIniTWTask(int itW){
		this.iTW = itW;
	}

	public int getFinTWTask() {
		return fTW;
	}

	public void setFinTWTask(int ftW){
		this.fTW = ftW;
	}

	public int getDurationTask() {
		return dur;
	}

	public void setDurationTask(int dur){
		this.dur = dur;
	}

	public boolean getChangeTWTask(){
		return changeTW;
	}

	public void setChangeTWTask(boolean changeTW){
		this.changeTW = changeTW;
	}

	public int getDayTask(){
		return dayID;
	}

	public void setDayTask(int dayTask){
		this.dayID = dayTask;
	}

	public String getInfoTask(){
		return info;
	}

	public void setInfoTask(String info){
		this.info = info;
	}

	public Patient getPatientTask(){
		return patientTask;
	}

	public void setPatientTask(Patient patient){
		this.patientTask = patient;
	}

}
