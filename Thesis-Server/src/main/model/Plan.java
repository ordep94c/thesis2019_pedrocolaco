package main.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Plan {

	@Id
	@GeneratedValue
	private int planID;

	private String namePlan;
	private boolean transpCar; //true -> car | false -> foot
	private int days;
	private boolean lunch;
	private int[] capacityVans;

	private int maxTime;
	private int maxWaitTimeBetweenTasks;
	private int lunchTime;							

	private boolean sameTeam_samePatient_constraint;
	private int tasksTWresize;
	private boolean extensiveSearch;
	private int timeoutValue;

	private double centerLati;
	private double centerLongi;

	@ManyToOne(cascade=CascadeType.MERGE)
	private User userPlan;

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "vanPlan")
	@JsonIgnore
	List<Van> vans;

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "workerPlan")
	@JsonIgnore
	List<Worker> workers;

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "patientPlan")
	@JsonIgnore
	List<Patient> patients;

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "planSolu")
	@JsonIgnore
	List<SolutionsDay> solus; 

	public Plan(){

	}

	public Plan(String namePlan, boolean transpCar, int days, boolean lunch, User u){
		this.namePlan = namePlan;
		this.transpCar = transpCar;
		this.days = days;
		this.lunch = lunch;
		this.userPlan = u;
	}

	public Plan(int maxTime, int maxWaitTimeBetweenTasks, int lunchTime, boolean sameTeam_samePatient_constraint, int tasksTWresize, boolean extensiveSearch, int timeoutValue, double centerLati, double centerLongi){
		this.maxTime = maxTime;
		this.maxWaitTimeBetweenTasks = maxWaitTimeBetweenTasks;
		this.lunchTime = lunchTime;
		this.sameTeam_samePatient_constraint = sameTeam_samePatient_constraint;
		this.tasksTWresize = tasksTWresize;
		this.extensiveSearch = extensiveSearch;
		this.timeoutValue = timeoutValue;
		this.centerLati = centerLati;
		this.centerLongi = centerLongi;
	}

	public int getPlanId(){
		return planID;
	}

	public void setPlanId(int id){
		this.planID = id;
	}

	public String getNamePlan(){
		return namePlan;
	}

	public void setNamePlan(String namePlan){
		this.namePlan = namePlan;
	}

	public boolean getTranspCar(){
		return transpCar;
	}

	public void setTranspCar(boolean transpCar){
		this.transpCar = transpCar;
	}

	public int getDays(){
		return days;
	}

	public void setDays(int days){
		this.days = days;
	}

	public boolean getLunch(){
		return lunch;
	}

	public void setLunch(boolean lunch){
		this.lunch = lunch;
	}

	public User getUserPlan(){
		return userPlan;
	}

	public void setUserPlan(User u){
		this.userPlan = u;
	}

	public void setWorkers(List<Worker> workers2) {
		this.workers = new ArrayList<Worker>(workers2);	
	}

	public List<Worker> getWorkers(){
		return workers;
	}

	public void addWorker(Worker w){
		workers.add(w);
	}

	public Worker getWorker(int idW) {
		List<Worker> findWorker = new ArrayList<Worker>(workers);

		for(int i = 0; i<findWorker.size(); i++){

			Worker w = findWorker.get(i);

			if(w.getWorkerId() == idW){
				return w;
			}
		}

		return null;
	}

	public void setPatients(List<Patient> patients2) {
		this.patients = new ArrayList<Patient>(patients2);	
	}

	public List<Patient> getPatients(){
		return patients;
	}

	public void addPatient(Patient p){
		patients.add(p);
	}

	public Patient getPatient(int idP) {
		List<Patient> findPatient = new ArrayList<Patient>(patients);

		for(int i = 0; i<findPatient.size(); i++){

			Patient p = findPatient.get(i);

			if(p.getIDpatient() == idP){
				return p;
			}
		}

		return null;
	}

	public int getPlanID() {
		return planID;
	}

	public void setPlanID(int planID) {
		this.planID = planID;
	}

	public int getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(int maxTime) {
		this.maxTime = maxTime;
	}

	public int getMaxWaitTimeBetweenTasks() {
		return maxWaitTimeBetweenTasks;
	}

	public void setMaxWaitTimeBetweenTasks(int maxWaitTimeBetweenTasks) {
		this.maxWaitTimeBetweenTasks = maxWaitTimeBetweenTasks;
	}

	public int getLunchTime() {
		return lunchTime;
	}

	public void setLunchTime(int lunchTime) {
		this.lunchTime = lunchTime;
	}

	public boolean isSameTeam_samePatient_constraint() {
		return sameTeam_samePatient_constraint;
	}

	public void setSameTeam_samePatient_constraint(boolean sameTeam_samePatient_constraint) {
		this.sameTeam_samePatient_constraint = sameTeam_samePatient_constraint;
	}

	public int getTasksTWresize() {
		return tasksTWresize;
	}

	public void setTasksTWresize(int tasksTWresize) {
		this.tasksTWresize = tasksTWresize;
	}

	public boolean getExtensiveSearch() {
		return extensiveSearch;
	}

	public void setExtensiveSearch(boolean extensiveSearch) {
		this.extensiveSearch = extensiveSearch;
	}



	public List<SolutionsDay> getSolus() {
		return solus;
	}

	public void setSolus(List<SolutionsDay> solus) {
		this.solus = solus;
	}

	public int getTimeoutValue() {
		return timeoutValue;
	}

	public void setTimeoutValue(int timeoutValue) {
		this.timeoutValue = timeoutValue;
	}

	public List<Van> getVans() {
		return vans;
	}

	public void setVans(List<Van> vans) {
		this.vans = vans;
	}

	public int[] getCapacityVans() {
		return capacityVans;
	}

	public void setCapacityVans(int[] capacityVans) {
		this.capacityVans = capacityVans;
	}

	public double getCenterLati() {
		return centerLati;
	}

	public double getCenterLongi() {
		return centerLongi;
	}

	public void setCenterLati(double centerLati) {
		this.centerLati = centerLati;
	}

	public void setCenterLongi(double centerLongi) {
		this.centerLongi = centerLongi;
	}


}
