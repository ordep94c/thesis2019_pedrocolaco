package main.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Patient {

	@Id
	@GeneratedValue
	private int IDpatient;

	private double lati;
	private double longi;

	private String name;
	private String dirPic;
	private int contact;
	private String email;
	private String adress;
	private String info;

	@ManyToOne(cascade=CascadeType.MERGE)
	private Plan patientPlan;

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "patientTask")
	@JsonIgnore
	List<Task> tasks;

	public Patient(){

	}

	public Patient(String name, String dirPic, double lati, double longi, int contact, String adress, String email, String info, Plan patientPlan){
		this.name = name;
		this.dirPic = dirPic;
		this.lati = lati;
		this.longi = longi;
		this.contact = contact;
		this.email = email;
		this.adress = adress;
		this.info = info;
		this.patientPlan = patientPlan;
	}



	public List<Task> getTasks(){
		return tasks;
	}

	public void addTask(Task t){
		tasks.add(t);
	}

	public Task getTask(int idT) {
		List<Task> findTask = new ArrayList<Task>(tasks);

		for(int i = 0; i < findTask.size(); i++){

			Task t = findTask.get(i);

			if(t.getIDTask() == idT){
				return t;
			}
		}

		return null;
	}

	public int getIDpatient() {
		return IDpatient;
	}

	public void setIDpatient(int iDpatient) {
		IDpatient = iDpatient;
	}

	public double getLati() {
		return lati;
	}

	public void setLati(double lati) {
		this.lati = lati;
	}

	public double getLongi() {
		return longi;
	}

	public void setLongi(double longi) {
		this.longi = longi;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDirPic() {
		return dirPic;
	}

	public void setDirPic(String dirPic) {
		this.dirPic = dirPic;
	}

	public int getContact() {
		return contact;
	}

	public void setContact(int contact) {
		this.contact = contact;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Plan getPatientPlan() {
		return patientPlan;
	}

	public void setPatientPlan(Plan patientPlan) {
		this.patientPlan = patientPlan;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
}