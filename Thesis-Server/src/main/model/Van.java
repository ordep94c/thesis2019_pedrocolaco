package main.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Van {

    @Id
    @GeneratedValue
	private int IDvan;
	private int capacity;
	
    @ManyToOne(cascade=CascadeType.MERGE)
    private Plan vanPlan;
    
    
    public Van(){
    	
    }
    
    public Van(int capacity, Plan p){
    	this.capacity = capacity;
    	vanPlan = p;
    }

	public int getIDvan() {
		return IDvan;
	}

	public void setIDvan(int iDvan) {
		IDvan = iDvan;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public Plan getVanPlan() {
		return vanPlan;
	}

	public void setVanPlan(Plan vanPlan) {
		this.vanPlan = vanPlan;
	}
}
