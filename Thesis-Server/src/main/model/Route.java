package main.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Route {

	@Id
    @GeneratedValue
	int idRoute;
    
    @OneToMany(cascade=CascadeType.MERGE)
    List<ArcSolu> routeSol;
    
    
    Route(){
    	
    }
	
    public Route(List<ArcSolu> routeSol) {
		this.routeSol = routeSol;
	}

	public int getIdRoute() {
		return idRoute;
	}

	public void setIdRoute(int idRoute) {
		this.idRoute = idRoute;
	}

	public List<ArcSolu> getRouteSol() {
		return routeSol;
	}

	public void setRouteSol(List<ArcSolu> routeSol) {
		this.routeSol = routeSol;
	}
}
