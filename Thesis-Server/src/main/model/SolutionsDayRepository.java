package main.model;

import org.springframework.data.repository.CrudRepository;

public interface SolutionsDayRepository extends CrudRepository<SolutionsDay,Integer> {

}
