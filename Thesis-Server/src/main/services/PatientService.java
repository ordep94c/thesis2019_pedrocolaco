package main.services;

import main.model.Patient;
import main.model.Plan;

public interface PatientService {

	Patient[] findAll();

	Patient[] findWithName(String name);

    void create(Patient p);

    Patient findById(int id);

    void remove(int id);
	
	Patient[] findAllPatients(Plan p);
	
	void update(Patient oldPatient, Patient newPatient);
}
