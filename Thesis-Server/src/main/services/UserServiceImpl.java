package main.services;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.model.User;
import main.model.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository repository;
	
	@Override
	public User[] findAll() {
        List<User> l = new ArrayList<User>();
        for(User t: repository.findAll()) {
            l.add(t);
        }
        return l.toArray(new User[l.size()]);
	}

	@Override
	public void create(User u) {
		repository.save(u);
	}

	@Override
	public void update(User u) {

	}

	@Override
	public User findById(int id) {
		return repository.findById(id).get();
	}

    @Override
    public User findByUserandPass(String userName,String password) {
	    User u = repository.findByuserName(userName);
	    
	    if(u != null){
		    if(u.getPassword().equals(password)){
		        return u;
		    }
	    }
	    
        return null;
    }

    @Override
    public User findByUserName(String userName) {
        return repository.findByuserName(userName);
    }


	@Override
	public void remove(int id) {
        User ux = repository.findById(id).get();
        repository.delete(ux);	
	}

}
