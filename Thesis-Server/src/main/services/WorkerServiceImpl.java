package main.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.model.Plan;
import main.model.Worker;
import main.model.WorkerRepository;

@Service
public class WorkerServiceImpl implements WorkerService {

	@Autowired
    WorkerRepository repository;
	
	@Override
	public Worker[] findAll() {
        List<Worker> l = new ArrayList<Worker>();
        for(Worker w: repository.findAll()) {
        		l.add(w);
        }
        return l.toArray(new Worker[l.size()]);
	}
	
	@Override
	public Worker[] findAllPlans(Plan p) {
        List<Worker> l = new ArrayList<Worker>(p.getWorkers());
        
        return l.toArray(new Worker[l.size()]);
	}

	@Override
	public Worker[] findWithName(String name) {
		return repository.findByName(name);
	}

	@Override
	public void create(Worker p) {
		 repository.save(p); 	
	}

	@Override
	public Worker findById(int id) {
		   return repository.findById(id).get();
	}

	@Override
	public void remove(int id) {
        Worker w = repository.findById(id).get();
        w.setPlanWorker(null);
        repository.save(w);
        repository.delete(w);
        repository.save(w);
	}

	@Override
	public void update(Worker oldWorker, Worker newWorker) {

		oldWorker.setNameWorker(newWorker.getNameWorker());
		oldWorker.setDirPic(newWorker.getDirPic());
		oldWorker.setWorkerContact(newWorker.getWorkerContact());
		oldWorker.setEmailWorker(newWorker.getEmailWorker());
		oldWorker.setInfoWorker(newWorker.getInfoWorker());
		
		repository.save(oldWorker);
		
	}

}
