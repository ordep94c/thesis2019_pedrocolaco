package main.services;
import main.model.User;

public interface UserService {

    User[] findAll();

    User findByUserName(String userName);

    void create(User u);

    void update(User u);

    User findById(int id);

    User findByUserandPass(String userName, String password);

    void remove(int id);
	
}
