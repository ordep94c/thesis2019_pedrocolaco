package main.services;

import main.model.Plan;
import main.model.Van;

public interface VanService {

    Van[] findAll();

    void create(Van v);

    void update(Van v);

    Van findById(int id);

    void remove(int id);

	Van[] findByPlan(Plan p);

	void removeAllByPlan(Plan p);
	
}
