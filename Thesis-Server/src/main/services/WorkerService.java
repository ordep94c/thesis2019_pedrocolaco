package main.services;

import main.model.Plan;
import main.model.Worker;

public interface WorkerService {

	Worker[] findAll();

	Worker[] findWithName(String name);

    void create(Worker p);

    Worker findById(int id);

    void remove(int id);

	Worker[] findAllPlans(Plan p);
	
	void update(Worker oldWorker, Worker newWorker);
	
}
