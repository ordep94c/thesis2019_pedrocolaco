package main.services;

import java.util.List;

import main.model.Plan;
import main.model.User;
import main.model.Van;

public interface PlanService {

    Plan[] findAll();
    
	Plan[] findAllPlans(User u);

    void create(Plan p);
    
    void remove(int id);

	void update(Plan oldPlan, Plan newPlan);
	
	void update2(Plan oldPlan, Plan newPlan);

	void addVans(Plan newPlan, List<Van> vans);

	void removeSolus(int idP);
	
}
