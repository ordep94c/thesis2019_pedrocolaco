package main.services;

import main.model.Patient;
import main.model.Task;

public interface TaskService {

	Task[] findAll();
	
	Task[] findAllTasksByPatient(Patient p);

	Task[] findWithName(String name);

    void create(Task t);

    Task findById(int id);

    void remove(int id);
	
	void update(Task oldTask, Task newTask);
	
}
