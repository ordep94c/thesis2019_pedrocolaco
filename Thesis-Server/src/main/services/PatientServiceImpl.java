package main.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.model.Patient;
import main.model.PatientRepository;
import main.model.Plan;

@Service
public class PatientServiceImpl implements PatientService{

	@Autowired
    PatientRepository repository;
	
	@Override
	public Patient[] findAll() {
        List<Patient> l = new ArrayList<Patient>();
        for(Patient p: repository.findAll()) {
        		l.add(p);
        }
        return l.toArray(new Patient[l.size()]);
	}
	
	@Override
	public Patient[] findAllPatients(Plan p) {
        List<Patient> l = new ArrayList<Patient>(p.getPatients());
        
        return l.toArray(new Patient[l.size()]);
	}

	@Override
	public Patient[] findWithName(String name) {
		return repository.findByName(name);
	}

	@Override
	public void create(Patient p) {
		repository.save(p);		
	}

	@Override
	public Patient findById(int id) {
		return repository.findById(id).get();
	}

	@Override
	public void remove(int id) {
        Patient p = repository.findById(id).get();
        p.setPatientPlan(null);
        repository.save(p);
        repository.delete(p);
        repository.save(p);
	}

	@Override
	public void update(Patient oldPatient, Patient newPatient) {

		oldPatient.setName(newPatient.getName());
		oldPatient.setDirPic(newPatient.getDirPic());
		oldPatient.setLati(newPatient.getLati());
		oldPatient.setLongi(newPatient.getLongi());
		oldPatient.setContact(newPatient.getContact());
		oldPatient.setEmail(newPatient.getEmail());
		oldPatient.setAdress(newPatient.getAdress());
		oldPatient.setInfo(newPatient.getInfo());
		
		repository.save(oldPatient);
		
	}
	
}
