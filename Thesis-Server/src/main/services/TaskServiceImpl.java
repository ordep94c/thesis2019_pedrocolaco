package main.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.model.Patient;
import main.model.Task;
import main.model.TaskRepository;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
    TaskRepository repository;
	
	@Override
	public Task[] findAll() {
        List<Task> l = new ArrayList<Task>();
        for(Task t: repository.findAll()) {
        		l.add(t);
        }
        return l.toArray(new Task[l.size()]);
	}

	@Override
	public Task[] findWithName(String name) {
		return repository.findByName(name);
	}

	@Override
	public void create(Task t) {
		repository.save(t);	
	}

	@Override
	public Task findById(int id) {
		return repository.findById(id).get();
	}

	@Override
	public void remove(int id) {
        Task t = repository.findById(id).get();
        t.setPatientTask(null);
        repository.save(t);
        repository.delete(t);
        repository.save(t);	
	}

	@Override
	public void update(Task oldTask, Task newTask) {

		oldTask.setTypeTask(newTask.getTypeTask());
		oldTask.setTypeTransportTask(newTask.getTypeTransportTask());
		oldTask.setIniTWTask(newTask.getIniTWTask());
		oldTask.setFinTWTask(newTask.getFinTWTask());
		oldTask.setDurationTask(newTask.getDurationTask());
		oldTask.setChangeTWTask(newTask.getChangeTWTask());
		oldTask.setInfoTask(newTask.getInfoTask());
		
		repository.save(oldTask);
		
	}

	@Override
	public Task[] findAllTasksByPatient(Patient p) {
        List<Task> l = new ArrayList<Task>(p.getTasks());
        
        return l.toArray(new Task[l.size()]);
	}

}
