package main.services;

import main.model.ArcSolu;
import main.model.Plan;
import main.model.Route;
import main.model.Solution;
import main.model.SolutionsDay;

public interface SolutionsService {

    SolutionsDay[] findAll();
	
    void create(SolutionsDay p);
    
    void remove(int id);

	void create2(Solution s);

	Solution[] findAll2();

	ArcSolu[] findAll3();

	void create3(ArcSolu a);

	void create4(Route a);

	Route[] findAll4();

	SolutionsDay[] findByPlan(Plan pl);
	
}
