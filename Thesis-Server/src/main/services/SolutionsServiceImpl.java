package main.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.model.ArcSolu;
import main.model.ArcSoluRepository;
import main.model.Plan;
import main.model.Route;
import main.model.RouteRepository;
import main.model.Solution;
import main.model.SolutionRepository;
import main.model.SolutionsDay;
import main.model.SolutionsDayRepository;

@Service
public class SolutionsServiceImpl implements SolutionsService{

    @Autowired
    SolutionRepository repo;
	
	@Autowired
    SolutionsDayRepository repository;
	
	@Autowired
    ArcSoluRepository repositoryArcs;
	
	@Autowired
    RouteRepository repositoryRoutes;
	
	@Override
	public SolutionsDay[] findAll() {
        List<SolutionsDay> l = new ArrayList<SolutionsDay>();
        for(SolutionsDay p: repository.findAll()) {
            l.add(p);
        }
        return l.toArray(new SolutionsDay[l.size()]);
	}

	@Override
	public void create(SolutionsDay s) {
		repository.save(s);
	}
	
	@Override
	public void remove(int id) {

		SolutionsDay s = repository.findById(id).get();
		
		s.setPlanSolu(null);
        repository.save(s);
        repository.delete(s);
        repository.save(s);	
	}
	
	@Override
	public Solution[] findAll2() {
	
        List<Solution> l = new ArrayList<Solution>();
        for(Solution p: repo.findAll()) {
            l.add(p);
        }
        return l.toArray(new Solution[l.size()]);
	}
	
	@Override
	public void create2(Solution s) {
		repo.save(s);
	}
	
	@Override
	public ArcSolu[] findAll3() {
        List<ArcSolu> l = new ArrayList<ArcSolu>();
        for(ArcSolu p: repositoryArcs.findAll()) {
            l.add(p);
        }
        return l.toArray(new ArcSolu[l.size()]);
	}
	
	@Override
	public void create3(ArcSolu a) {
		repositoryArcs.save(a);
	}
	
	@Override
	public Route[] findAll4() {
        List<Route> l = new ArrayList<Route>();
        for(Route p: repositoryRoutes.findAll()) {
            l.add(p);
        }
        return l.toArray(new Route[l.size()]);
	}
	
	@Override
	public void create4(Route a) {
		repositoryRoutes.save(a);
	}

	@Override
	public SolutionsDay[] findByPlan(Plan pl) {
        List<SolutionsDay> l = new ArrayList<SolutionsDay>(pl.getSolus());

        return l.toArray(new SolutionsDay[l.size()]);
	}


}
