package main.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.model.Plan;
import main.model.PlanRepository;
import main.model.User;
import main.model.Van;

@Service
public class PlanServiceImpl implements PlanService {
	
    @Autowired
    PlanRepository repository;

	@Override
	public Plan[] findAllPlans(User u) {
        List<Plan> l = new ArrayList<Plan>(u.getPlans());
        
        return l.toArray(new Plan[l.size()]);
	}

	@Override
	public Plan[] findAll() {
        List<Plan> l = new ArrayList<Plan>();
        for(Plan p: repository.findAll()) {
            l.add(p);
        }
        return l.toArray(new Plan[l.size()]);
	}

	@Override
	public void create(Plan p) {
		repository.save(p);
	}

	@Override
	public void update(Plan oldPlan, Plan newPlan) {

		oldPlan.setNamePlan(newPlan.getNamePlan());
		oldPlan.setTranspCar(newPlan.getTranspCar());
		oldPlan.setDays(newPlan.getDays());
		oldPlan.setLunch(newPlan.getLunch());
		
		repository.save(oldPlan);
		
	}
	
	@Override
	public void update2(Plan oldPlan, Plan newPlan) {
		System.out.println("cc");

		oldPlan.setMaxTime(newPlan.getMaxTime());
		oldPlan.setMaxWaitTimeBetweenTasks(newPlan.getMaxWaitTimeBetweenTasks());
		oldPlan.setLunchTime(newPlan.getLunchTime());
		oldPlan.setSameTeam_samePatient_constraint(newPlan.isSameTeam_samePatient_constraint());
		oldPlan.setTasksTWresize(newPlan.getTasksTWresize());
		oldPlan.setExtensiveSearch(newPlan.getExtensiveSearch());
		oldPlan.setTimeoutValue(newPlan.getTimeoutValue());
		oldPlan.setCenterLati(newPlan.getCenterLati());
		oldPlan.setCenterLongi(newPlan.getCenterLongi());
		
		repository.save(oldPlan);
		
	}

	@Override
	public void remove(int id) {
        Plan px = repository.findById(id).get();
        px.setUserPlan(null);
        repository.save(px);
        repository.delete(px);	
	}

	@Override
	public void addVans(Plan plan, List<Van> vans) {
		plan.setVans(vans);
		
	}

	@Override
	public void removeSolus(int idP) {
		Plan px = repository.findById(idP).get();	
		
		px.setSolus(null);
		repository.save(px);
	}
	
}
