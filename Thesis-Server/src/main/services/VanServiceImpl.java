package main.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.model.Plan;
import main.model.Van;
import main.model.VanRepository;

@Service
public class VanServiceImpl implements VanService {
	
    @Autowired
    VanRepository repository;

	@Override
	public Van[] findAll() {
        List<Van> l = new ArrayList<Van>();
        for(Van t: repository.findAll()) {
            l.add(t);
        }
        return l.toArray(new Van[l.size()]);
	}

	@Override
	public void create(Van v) {
		repository.save(v);	
	}

	@Override
	public void update(Van u) {
		
	}

	@Override
	public Van findById(int id) {
		return repository.findById(id).get();
	}

	@Override
	public void remove(int id) {
        Van v = repository.findById(id).get();
        repository.delete(v);	
	}

	@Override
	public Van[] findByPlan(Plan p) {
        List<Van> l = new ArrayList<Van>(p.getVans());
        
        return l.toArray(new Van[l.size()]);
	}

	@Override
	public void removeAllByPlan(Plan p) {
		List<Van> listV = p.getVans();
		for(int i=0; i<listV.size(); i++){
			listV.get(i).setVanPlan(null);
			remove(listV.get(i).getIDvan());
		}
		
		p.setVans(null);
	}

}
