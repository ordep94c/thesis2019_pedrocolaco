package main;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import main.model.Patient;
import main.model.PatientRepository;
import main.model.Plan;
import main.model.PlanRepository;
import main.model.Task;
import main.model.TaskRepository;
import main.model.User;
import main.model.UserRepository;
import main.model.Van;
import main.model.VanRepository;
import main.model.Worker;
import main.model.WorkerRepository;



@SpringBootApplication
public class Application {
	

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }

    /*
     * Creating plan for tests
     */
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx,  UserRepository users, 
    																	PlanRepository plans,
    																	WorkerRepository workers,
    																	PatientRepository patients,
    																	TaskRepository tasks,
    																	VanRepository vans) {
        return args -> {

        	User u1 = new User("eu","a");
        	users.save(u1);
        	
        	Plan p1 = new Plan("Apoiem", true, 2, false, u1);
        	plans.save(p1);
        	
        	Worker w1 = new Worker("Félix Bermudes", "lebron.jpg", 913737375, "felix4444@hotmail.com", "This guy loves cakes.", p1);
        	Worker w2 = new Worker("Jorge de Brito", "doge.jpg", 917492583, "jorshua999@hotmail.com", "This lad is massive.", p1);
        	Worker w3 = new Worker("Asdrubal Feio", "", 912927574, "asdru@google.com", "Quem nunca.", p1);
        	Worker w4 = new Worker("Anacleto Dubio", "", 912927578, "anad@google.com", "Quem nunca2.", p1);
//        	Worker w5 = new Worker("Anacleto Dubio2", "",  912927578, "anad@google.com", "Quem nunca2.", p1);
//        	Worker w6 = new Worker("Anacleto Dubio3", "", 912927578, "anad@google.com", "Quem nunca2.", p1);
        	workers.save(w1);
        	workers.save(w2);
        	workers.save(w3);
        	workers.save(w4);
//        	workers.save(w5);
//        	workers.save(w6);
        	
        	
//        	Patient pa1 = new Patient("Madalena Gonçalves", "lebron.jpg", 38.70154, -9.22868, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
//        	Patient pa2 = new Patient("Mª de Lurdes Robalo", "lebron.jpg", 38.70228, -9.23019, 93747361,"Rua A", "ya2@coiso.com", "bbb", p1);
//        	Patient pa3 = new Patient("Manuel Barros", "lebron.jpg", 38.70378, -9.22785, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa4 = new Patient("M� Vit�ria Santos", "lebron.jpg", 38.70496, -9.22931, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa5 = new Patient("Carlos Firmino", "lebron.jpg", 38.70479, -9.23187, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa6 = new Patient("M� Lurdes das Neves", "lebron.jpg", 38.7045, -9.22895, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa7 = new Patient("Luciana Silva", "lebron.jpg", 38.70283, -9.2298, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa8 = new Patient("Deolinda Pereira", "lebron.jpg", 38.69936, -9.22903, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa9 = new Patient("Maria Manuela Soares", "lebron.jpg", 38.70285, -9.23365, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa10 = new Patient("Manuel Maria da Silva", "lebron.jpg", 38.71865, -9.24274, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa11 = new Patient("Maria Cristiana de Moura Palha�a", "lebron.jpg", 38.70196, -9.23069, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa12 = new Patient("Isabel Br�zida", "lebron.jpg", 38.71018, -9.24594, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa13 = new Patient("M� Em�lia Gar�oa", "lebron.jpg", 38.71018, -9.24594, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa14 = new Patient("Marie Esp�rito Santo", "lebron.jpg", 38.70229, -9.22884, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	Patient pa15 = new Patient("Victor Santos / Odete dos Santos", "lebron.jpg", 38.70936, -9.24381, 918482732, "Rua B", "ya3@coiso.com", "ccc", p1);
//        	patients.save(pa1);
//        	patients.save(pa2);
//        	patients.save(pa3);
//           	patients.save(pa4);
//        	patients.save(pa5);
//        	patients.save(pa6);
//           	patients.save(pa7);
//        	patients.save(pa8);
//        	patients.save(pa9);
//           	patients.save(pa10);
//        	patients.save(pa11);
//        	patients.save(pa12);
//           	patients.save(pa13);
//        	patients.save(pa14);
//        	patients.save(pa15);
        	
    		Patient pa1 = new Patient("Paciente447", "lebron.jpg", 38.713092, -9.234446, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa2 = new Patient("Paciente337", "defaultP.jpg", 38.713533, -9.238069, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa3 = new Patient("Paciente339", "", 38.710616, -9.243819, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa4 = new Patient("Paciente385", "defaultP.jpg", 38.706875, -9.239491, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa5 = new Patient("Paciente437", "", 38.725185, -9.221050, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);				//? - 400
    		Patient pa6 = new Patient("Paciente363", "", 38.708450, -9.241940, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa7 = new Patient("Paciente387", "", 38.715566, -9.244186, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa8 = new Patient("Paciente393", "lebron.jpg", 38.701385, -9.230130, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa9 = new Patient("Paciente313", "lebron.jpg", 38.700072, -9.229389, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa10 = new Patient("Paciente285", "lebron.jpg", 38.700767, -9.356072, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa11 = new Patient("Paciente428", "lebron.jpg", 38.707301, -9.229945, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa12 = new Patient("Paciente376", "lebron.jpg", 38.706077, -9.232339, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa13 = new Patient("Paciente357", "lebron.jpg", 38.724966, -9.228820, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa14 = new Patient("Paciente356", "lebron.jpg", 38.724940, -9.228842, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa15 = new Patient("Paciente312", "lebron.jpg", 38.702902, -9.233593, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa16 = new Patient("Paciente326", "lebron.jpg", 38.724447, -9.230897, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa17 = new Patient("Paciente379", "lebron.jpg", 38.708953, -9.227114, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa18 = new Patient("Paciente292", "lebron.jpg", 38.707709, -9.229777, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa19 = new Patient("Paciente374", "lebron.jpg", 38.704033, -9.231191, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa20 = new Patient("Paciente342", "lebron.jpg", 38.704204, -9.229045, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa21 = new Patient("Paciente451", "lebron.jpg", 38.726968, -9.237250, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa22 = new Patient("Paciente450", "lebron.jpg", 38.726968, -9.237250, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa23 = new Patient("Paciente359", "lebron.jpg", 38.706134, -9.228042, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa24 = new Patient("Paciente440", "lebron.jpg", 38.702730, -9.227412, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa25 = new Patient("Paciente295", "lebron.jpg", 38.705456, -9.225963, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa26 = new Patient("Paciente279", "lebron.jpg", 38.700080, -9.229410, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa27 = new Patient("Paciente293", "lebron.jpg", 38.700080, -9.229410, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa28 = new Patient("Paciente328", "lebron.jpg", 38.702504, -9.230356, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa29 = new Patient("Paciente367", "lebron.jpg", 38.701928, -9.229614, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa30 = new Patient("Paciente413", "lebron.jpg", 38.699689, -9.228664, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa31 = new Patient("Paciente409", "lebron.jpg", 38.699820, -9.228128, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa32 = new Patient("Paciente297", "lebron.jpg", 38.698761, -9.228718, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa33 = new Patient("Paciente329", "lebron.jpg", 38.702554, -9.230388, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa34 = new Patient("Paciente378", "lebron.jpg", 38.702441, -9.230236, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa35 = new Patient("Paciente419", "lebron.jpg", 38.725763, -9.228348, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa36 = new Patient("Paciente453", "lebron.jpg", 38.722429, -9.222712, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa37 = new Patient("Paciente458", "lebron.jpg", 38.705212, -9.226470, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa38 = new Patient("Paciente371", "lebron.jpg", 38.724360, -9.227491, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa39 = new Patient("Paciente415", "lebron.jpg", 38.730051, -9.226694, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa40 = new Patient("Paciente432", "lebron.jpg", 38.711154, -9.247815, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa41 = new Patient("Paciente397", "lebron.jpg", 38.724865, -9.228810, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa42 = new Patient("Paciente405", "lebron.jpg", 38.725185, -9.221050, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);				//? - 400
    		Patient pa43 = new Patient("Paciente452", "lebron.jpg", 38.704305, -9.227286, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa44 = new Patient("Paciente0", "lebron.jpg", 38.725185, -9.221050, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);				//? - 400
    		Patient pa45 = new Patient("Paciente394", "lebron.jpg", 38.724866, -9.223053, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa46 = new Patient("Paciente375", "lebron.jpg", 38.713238, -9.242563, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa47 = new Patient("Paciente429", "lebron.jpg", 38.725569, -9.226603, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa48 = new Patient("Paciente421", "lebron.jpg", 38.725561, -9.227428, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa49 = new Patient("Paciente430", "lebron.jpg", 38.725185, -9.221050, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);				//? - 400
    		Patient pa50 = new Patient("Paciente434", "lebron.jpg", 38.725185, -9.221050, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);				//? - 400
    		Patient pa51 = new Patient("Paciente355", "lebron.jpg", 38.725555, -9.227041, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa52 = new Patient("Paciente404", "lebron.jpg", 38.720478, -9.229981, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa53 = new Patient("Paciente352", "lebron.jpg", 38.719342, -9.231423, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa54 = new Patient("Paciente340", "lebron.jpg", 38.719368, -9.231412, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa55 = new Patient("Paciente289", "lebron.jpg", 38.718027, -9.231482, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa56 = new Patient("Paciente423", "lebron.jpg", 38.721523, -9.231324, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa57 = new Patient("Paciente398", "lebron.jpg", 38.724386, -9.227545, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa58 = new Patient("Paciente399", "lebron.jpg", 38.724386, -9.227545, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa59 = new Patient("Paciente360", "lebron.jpg", 38.728962, -9.242112, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa60 = new Patient("Paciente327", "lebron.jpg", 38.720281, -9.265393, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa61 = new Patient("Paciente288", "lebron.jpg", 38.726256, -9.220878, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
    		Patient pa62 = new Patient("Paciente287", "lebron.jpg", 38.722182, -9.226955, 957847377, "Rua A", "ya@coiso.com", "aaa", p1);
        	patients.save(pa1);
        	patients.save(pa2);
        	patients.save(pa3);
           	patients.save(pa4);
        	patients.save(pa5);
        	patients.save(pa6);
           	patients.save(pa7);
        	patients.save(pa8);
        	patients.save(pa9);
           	patients.save(pa10);
        	patients.save(pa11);
        	patients.save(pa12);
           	patients.save(pa13);
        	patients.save(pa14);
        	patients.save(pa15);
        	patients.save(pa16);
        	patients.save(pa17);
        	patients.save(pa18);
           	patients.save(pa19);
        	patients.save(pa20);
        	patients.save(pa21);
           	patients.save(pa22);
        	patients.save(pa23);
        	patients.save(pa24);
           	patients.save(pa25);
        	patients.save(pa26);
        	patients.save(pa27);
           	patients.save(pa28);
        	patients.save(pa29);
        	patients.save(pa30);
        	patients.save(pa31);
        	patients.save(pa32);
        	patients.save(pa33);
           	patients.save(pa34);
        	patients.save(pa35);
        	patients.save(pa36);
           	patients.save(pa37);
        	patients.save(pa38);
        	patients.save(pa39);
           	patients.save(pa40);
        	patients.save(pa41);
        	patients.save(pa42);
           	patients.save(pa43);
        	patients.save(pa44);
        	patients.save(pa45);
        	patients.save(pa46);
        	patients.save(pa47);
        	patients.save(pa48);
           	patients.save(pa49);
        	patients.save(pa50);
        	patients.save(pa51);
           	patients.save(pa52);
        	patients.save(pa53);
        	patients.save(pa54);
           	patients.save(pa55);
        	patients.save(pa56);
        	patients.save(pa57);
           	patients.save(pa58);
        	patients.save(pa59);
        	patients.save(pa60);
        	patients.save(pa61);
        	patients.save(pa62);
        	
        	Task t1 = new Task(1, 0, 0, 120, 10, false, 0, "infoooo", pa1);
        	Task t2 = new Task(1, 0, 120, 240, 20, false, 0, "infooyhtgrf", pa2);
        	Task t3 = new Task(2, 1, 0, 120, 10, false, 0, "uyntbgrf", pa3);
        	Task t4 = new Task(2, 1, 120, 240, 20, false, 0, "uj6ytgr", pa4);
        	Task t5 = new Task(2, 1, 0, 120, 10, false, 0, "87654r", pa5);
        	Task t6 = new Task(2, 1, 120, 240, 20, false, 0, "uyjhtg", pa6);
        	Task t7 = new Task(2, 2, 0, 120, 10, false, 0, "wfd", pa7);
        	
        	Task t8 = new Task(2, 0, 120, 240, 10, false, 0, "87654r", pa8);
        	Task t9 = new Task(2, 0, 120, 240, 10, false, 0, "uyjhtg", pa9);
        	Task t10 = new Task(2, 0, 120, 240, 10, false, 0, "wfd", pa10);
        	
        	Task t11 = new Task(2, 0, 240, 360, 10, false, 0, "uyjhtg", pa11);
        	Task t12 = new Task(2, 0, 240, 360, 10, false, 0, "wfd", pa12);
        	
        	Task t13 = new Task(2, 3, 360, 480, 10, false, 0, "87654r", pa13);
        	Task t14 = new Task(2, 3, 360, 480, 10, false, 0, "uyjhtg", pa14);
        	Task t15 = new Task(2, 4, 360, 480, 10, false, 0, "wfd", pa15);
        	tasks.save(t1);
        	tasks.save(t2);
        	tasks.save(t3);
        	tasks.save(t4);
        	tasks.save(t5);
        	tasks.save(t6);
        	tasks.save(t7);
        	tasks.save(t8);
        	tasks.save(t9);
        	tasks.save(t10);
        	tasks.save(t11);
        	tasks.save(t12);
        	tasks.save(t13);
        	tasks.save(t14);
        	tasks.save(t15);
        	

//        	Task t1 = new Task(2, 1, 0, 120, 2, true, 0, "abc", pa1);
//			Task t2 = new Task(2, 1, 0, 120, 2, true,0, "abc", pa2);
//			Task t3 = new Task(2, 1, 0, 120, 2, true,0, "abc", pa3);
//			Task t4 = new Task(2, 1, 0, 120, 2, true,0, "abc", pa4);
//			Task t5 = new Task(2, 1, 0, 120, 2, true,0, "abc", pa5);
//			Task t6 = new Task(2, 1, 0, 120, 2, true,0, "abc", pa6);
//			Task t7 = new Task(2, 1,  0, 120, 2, true,0, "abc", pa7);
//			Task t8 = new Task(2, 1,  0, 120, 2, true,0, "abc", pa8);
//			Task t9 = new Task(2, 1, 0, 120, 2, true,1, "abc", pa9);
//			Task t10 = new Task(2, 1,  0, 120, 2, true,1, "abc", pa10);
//			Task t11 = new Task(2, 1,  0, 120, 2, true,1, "abc", pa11);
//			Task t12 = new Task(2, 1,  0, 120, 2, true,1, "abc", pa12);
//			Task t13 = new Task(2, 1,  0, 120, 2, true,1, "abc", pa13);
//			Task t14 = new Task(2, 1,  0, 120, 2, true,1, "abc", pa14);
//			Task t15 = new Task(2, 1,  0, 120, 2, true,1, "abc", pa15);
			Task t16 = new Task(2, 0,  0, 120, 2, true,1, "abc", pa16);
			Task t17 = new Task(2, 0,  240, 360, 2, true,1, "abc", pa17);
			Task t18 = new Task(2, 0, 240, 360, 2, true,1, "abc", pa18);
			Task t19 = new Task(2, 0,  240, 360, 2, true,1, "abc", pa19);
			Task t20 = new Task(2, 0, 240, 360, 2, true,1, "abc", pa20);
			Task t21 = new Task(2, 0, 240, 360, 2, true,1, "abc", pa21);
			Task t22 = new Task(2, 0,  240, 360, 2, true,1, "abc", pa22);
			Task t23 = new Task(2, 0,  240, 360, 2, true,1, "abc", pa23);
			Task t24 = new Task(2, 0,  240, 360, 2, true,1, "abc", pa24);
			Task t25 = new Task(2, 0,  240, 360, 2, true,1, "abc", pa25);
			Task t26 = new Task(2, 0,  240, 360, 2, true,1, "abc", pa26);
			Task t27 = new Task(2, 0,  240, 360, 2, true,1, "abc", pa27);
			Task t28 = new Task(2, 0,  240, 360, 2, true,1, "abc", pa28);
			Task t29 = new Task(2, 0,  240, 360, 2, true,1, "abc", pa29);
//			Task t30 = new Task(2, 0,  240, 360, 2, true,0, "abc", pa30);
//			Task t31 = new Task(2, 0,  240, 360, 2, true,0, "abc", pa31);
//			Task t32 = new Task(2, 0,  240, 360, 2, true,0, "abc", pa32);
//			Task t33 = new Task(2, 0,  240, 360, 2, true,0, "abc", pa33);
//			Task t34 = new Task(2, 0,  240, 360, 2, true,0, "abc", pa34);
//			Task t35 = new Task(2, 0,  240, 360, 2, true,0, "abc", pa35);
//			Task t36 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa13);
//			Task t37 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa14);
//			Task t38 = new Task(2, 0, 480, 600, 2, true,0, "abc", pa8);
//			Task t39 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa11);
//			Task t40 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa35);
//			Task t41 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa15);
//			Task t42 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa9);
//			Task t43 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa2);
//			Task t44 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa3);
//			Task t45 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa4);
//			Task t46 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa5);
//			Task t47 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa6);
//			Task t48 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa16);
//			Task t49 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa7);
//			Task t50 = new Task(2, 0,  0, 120, 2, true,0, "abc", pa36);
//			Task t51 = new Task(2, 0,  0, 120, 2, true,0, "abc", pa37);
//			Task t52 = new Task(2, 0, 480, 600, 2, true,0, "abc", pa38);			
//			Task t53 = new Task(2, 0,  240, 360, 2, true,0, "abc", pa39);
//			Task t54 = new Task(2, 0,  0, 120, 2, true,0, "abc", pa40);
//			Task t55 = new Task(2, 0,  0, 120, 2, true,0, "abc", pa41);
//			Task t56 = new Task(2, 0,  0, 120, 2, true,0, "abc", pa42);
//			Task t57 = new Task(2, 0,  0, 120, 2, true,0, "abc", pa40);
//			Task t58 = new Task(2, 0,  120, 240, 2, true,0, "abc", pa43);
//			Task t59 = new Task(2, 0, 480, 600, 2, true,0, "abc", pa45);
//			Task t60 = new Task(2, 0, 480, 600, 2, true,0, "abc", pa46);
//			Task t61 = new Task(2, 0, 480, 600, 2, true,0, "abc", pa47);
//			Task t62 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa48);
//			Task t63 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa49);
//			Task t64 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa50);
//			Task t65 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa51);
//			Task t66 = new Task(2, 0, 480, 600, 2, true,0, "abc", pa52);
//			Task t67 = new Task(2, 0, 480, 600, 2, true,0, "abc", pa53);
//			Task t68 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa54);
//			Task t69 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa55);
//			Task t70 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa56);
//			Task t71 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa35);
//			Task t72 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa57);
//			Task t73 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa58);
//			Task t74 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa59);
//			Task t75 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa60);
//			Task t76 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa61);
//			Task t77 = new Task(2, 0,  480, 600, 2, true,0, "abc", pa62);
//        	tasks.save(t1);
//        	tasks.save(t2);
//        	tasks.save(t3);
//        	tasks.save(t4);
//        	tasks.save(t5);
//        	tasks.save(t6);
//        	tasks.save(t7);
//        	tasks.save(t8);
//        	tasks.save(t9);
//        	tasks.save(t10);
//        	tasks.save(t11);
//        	tasks.save(t12);
//        	tasks.save(t13);
//        	tasks.save(t14);
//        	tasks.save(t15);
        	tasks.save(t16);
        	tasks.save(t17);
        	tasks.save(t18);
        	tasks.save(t19);
        	tasks.save(t20);
        	tasks.save(t21);
        	tasks.save(t22);
        	tasks.save(t23);
        	tasks.save(t24);
        	tasks.save(t25);
        	tasks.save(t26);
        	tasks.save(t27);
        	tasks.save(t28);
        	tasks.save(t29);
//        	tasks.save(t30);
//        	tasks.save(t31);
//        	tasks.save(t32);
//        	tasks.save(t33);
//        	tasks.save(t34);
//        	tasks.save(t35);
//        	tasks.save(t36);
//        	tasks.save(t37);
//        	tasks.save(t38);
//        	tasks.save(t39);
//        	tasks.save(t40);
//        	tasks.save(t41);
//        	tasks.save(t42);
//        	tasks.save(t43);
//        	tasks.save(t44);
//        	tasks.save(t45);
//        	tasks.save(t46);
//        	tasks.save(t47);
//        	tasks.save(t48);
//        	tasks.save(t49);
//        	tasks.save(t50);
//        	tasks.save(t51);
//        	tasks.save(t52);
//        	tasks.save(t53);
//        	tasks.save(t54);
//        	tasks.save(t55);
//        	tasks.save(t56);
//        	tasks.save(t57);
//        	tasks.save(t58);
//        	tasks.save(t59);
//        	tasks.save(t60);
//        	tasks.save(t61);
//        	tasks.save(t62);
//        	tasks.save(t63);
//        	tasks.save(t64);
//        	tasks.save(t65);
//        	tasks.save(t66);
//        	tasks.save(t67);
//        	tasks.save(t68);
//        	tasks.save(t69);
//        	tasks.save(t70);
//        	tasks.save(t71);
//        	tasks.save(t72);
//        	tasks.save(t73);
//        	tasks.save(t74);
//        	tasks.save(t75);
//        	tasks.save(t76);
//        	tasks.save(t77);
			
        	Van v1 = new Van(4, p1);
        	Van v2 = new Van(7, p1);
        	Van v3 = new Van(7, p1);
        	Van v4 = new Van(7, p1);
        	vans.save(v1);
        	vans.save(v2);
        	vans.save(v3);
        	vans.save(v4);
//            
        };
    }

}