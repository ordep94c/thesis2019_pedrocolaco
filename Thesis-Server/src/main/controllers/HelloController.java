package main.controllers;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Hello! This is the Server that runs the SISTEMA DE APOIO À DECISÃO PARA A OTIMIZAÇÃO DO PLANEAMENTO DE SERVIÇOS DE APOIO DOMICILIÁRIO app \n";
    }

}