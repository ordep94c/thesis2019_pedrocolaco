package main.controllers;

import main.exceptions.ResourceNotFoundException;

public class Preconditions {
    public static <T> T checkFound(T resource) {
        if (resource == null) {
            throw new ResourceNotFoundException();
        }
        return resource;
    }
}