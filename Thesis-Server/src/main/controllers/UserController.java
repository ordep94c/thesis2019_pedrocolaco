package main.controllers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import heuristic.algo.MainAlgo;
import heuristic.algo.SolutionRoutes;
import heuristic.objects.Arc;
import heuristic.repo.SolutionsRepository;
import main.model.ArcSolu;
import main.model.Patient;
import main.model.Plan;
import main.model.Route;
import main.model.Solution;
import main.model.SolutionsDay;
import main.model.Task;
import main.model.User;
import main.model.Van;
import main.model.Worker;
import main.services.PatientService;
import main.services.PlanService;
import main.services.SolutionsService;
import main.services.TaskService;
import main.services.UserService;
import main.services.VanService;
import main.services.WorkerService;

@RestController
@RequestMapping(value="/users")
public class UserController {

	@Autowired
	UserService users;

	@Autowired
	PlanService plans;

	@Autowired
	VanService vans;

	@Autowired
	WorkerService workers;

	@Autowired
	PatientService patients;

	@Autowired
	TaskService tasks;

	@Autowired
	SolutionsService solus;

	@CrossOrigin
	@RequestMapping(value="", method= RequestMethod.GET)
	User[] getUsers() {

		return users.findAll();

	}

	@CrossOrigin
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	User showUser(@PathVariable int id) {
		User u = users.findById(id);

		Preconditions.checkFound(u);

		return u;
	}

	@CrossOrigin
	@RequestMapping(value="", method = RequestMethod.POST)
	User createUser(@RequestBody User u) {
		User checkExists = users.findByUserName(u.getUserName());

		if(checkExists != null){
			User fictionalUSer = new User();
			return fictionalUSer;
		}
		else{
			users.create(u);
			return users.findByUserName(u.getUserName());
		}


	}


	@CrossOrigin
	@RequestMapping(value="/auth/{userName}/{password}", method = RequestMethod.GET)
	User verifyUser(@PathVariable String userName, @PathVariable String password) {
		User u = users.findByUserandPass(userName,password);

		if(u == null){
			User fictionalUSer = new User();
			return fictionalUSer;
		}
		else{

			return u;
		}


	}

	@CrossOrigin
	@RequestMapping(value="/{id}/plans", method = RequestMethod.POST)
	Plan createPlan(@PathVariable int id, @RequestBody Plan p) {
		User u = users.findById(id);

		if(u != null){
			Plan newPlan = new Plan(p.getNamePlan(),p.getTranspCar(),p.getDays(),p.getLunch(), u);
			plans.create(newPlan);

			int[] capVans = p.getCapacityVans();
			List<Van> vansList = new ArrayList<Van>(capVans.length);
			for(int i=0; i<capVans.length; i++){
				Van v = new Van(capVans[i], newPlan);
				vansList.add(v);
				vans.create(v);
			}
			plans.addVans(newPlan, vansList);

			return newPlan;
		}
		else{
			Plan fictionalPlan = new Plan();
			return fictionalPlan;
		}


	}

	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}", method = RequestMethod.GET)
	Plan showPlan(@PathVariable int idU, @PathVariable int idP) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				return p;
			}
		}

		return null;
	}

	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}", method = RequestMethod.DELETE)
	Plan removePlan(@PathVariable int idU, @PathVariable int idP) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				plans.remove(idP);					
				return p;
			}
		}

		return null;
	}

	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}", method = RequestMethod.PUT)
	Plan editPlan(@PathVariable int idU, @PathVariable int idP, @RequestBody Plan editPlan) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);

			if(p != null){
				plans.update(p, editPlan);

				return p;
			}
		}

		return null;
	}

	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/generate", method = RequestMethod.PUT)
	Plan generateSolus(@PathVariable int idU, @PathVariable int idP, @RequestBody Plan editPlan) throws InterruptedException, ExecutionException {

		User u = users.findById(idU);
		if(u != null){
			Plan p = u.getPlan(idP);

			if(p != null){
				Plan prePlan = new Plan(p.getMaxTime(), p.getMaxWaitTimeBetweenTasks(), p.getLunchTime(), p.isSameTeam_samePatient_constraint(), p.getTasksTWresize(), p.getExtensiveSearch(), p.getTimeoutValue(), p.getCenterLati(), p.getCenterLongi());
				plans.update2(p, editPlan);

				MainAlgo solu = new MainAlgo(p);
				solu.doIt();
				TimeUnit.SECONDS.sleep(1);

				List<SolutionsRepository> allDaysSolutions = new ArrayList<SolutionsRepository>(solu.getAllDaysSolutions());

				if(allDaysSolutions.size() == 0){
					plans.update2(p, prePlan);
				}

				for(int i=0; i<allDaysSolutions.size(); i++){

					SolutionsRepository dayS = new SolutionsRepository(allDaysSolutions.get(i));

					//FIRST SOLUTION FROM A DAY FROM A PLAN
					SolutionRoutes soluFirst = dayS.getFirstSolution();

					if(soluFirst != null){

						Map<Integer,Route> routesArcs = new HashMap<Integer,Route>();
						Set<Entry<heuristic.objects.Worker, ArrayList<Arc>>> workersSoluFirst3 = new HashSet<Entry<heuristic.objects.Worker, ArrayList<Arc>>>(soluFirst.getRoutesArcs().entrySet());
						Iterator<Entry<heuristic.objects.Worker, ArrayList<Arc>>> it = workersSoluFirst3.iterator();
						while(it.hasNext()){
							Entry<heuristic.objects.Worker, ArrayList<Arc>> entry = it.next();
							int idWorker = entry.getKey().getID();
							ArrayList<Arc> listArcs = entry.getValue();
							List<ArcSolu> listArcsSolu = new ArrayList<ArcSolu>();
							for(int j=0;j<listArcs.size();j++){
								Arc a = listArcs.get(j);
								ArcSolu arc = new ArcSolu(a.getNode1().getName(), a.getNode2().getName(), a.getBestTime(), a.getBestTime()+a.getNode2().getDuration(),a.getWaitTime(),a.getNode2().getType(),a.getNode2().getTypeTransport());
								solus.create3(arc);
								listArcsSolu.add(arc);
							}
							Route r = new Route(listArcsSolu);
							solus.create4(r);
							routesArcs.put(idWorker, r);
						}

						Map<Integer, Double> valuesTimeWorkedByWorker = new HashMap<Integer, Double>();
						Set<Entry<heuristic.objects.Worker, Double>> workersTimeWorkedSoluFirst = new HashSet<Entry<heuristic.objects.Worker, Double>>(soluFirst.getValuesTimeWorkedByWorker().entrySet());
						Iterator<Entry<heuristic.objects.Worker, Double>> it2 = workersTimeWorkedSoluFirst.iterator();
						while(it2.hasNext()){
							Entry<heuristic.objects.Worker, Double> entry2 = it2.next();
							int idWorker2 = entry2.getKey().getID();
							double timeWorked = entry2.getValue();

							valuesTimeWorkedByWorker.put(idWorker2, timeWorked);

						}

						Map<Integer, Double> valuesTimeWaitedByWorker = new HashMap<Integer, Double>();
						Set<Entry<heuristic.objects.Worker, Double>> workersTimeWaitSoluFirst = new HashSet<Entry<heuristic.objects.Worker, Double>>(soluFirst.getValuesTimeWaitingByWorker().entrySet());
						Iterator<Entry<heuristic.objects.Worker, Double>> it3 = workersTimeWaitSoluFirst.iterator();
						while(it3.hasNext()){
							Entry<heuristic.objects.Worker, Double> entry3 = it3.next();
							int idWorker3 = entry3.getKey().getID();
							double timeWait = entry3.getValue();

							valuesTimeWaitedByWorker.put(idWorker3, timeWait);

						}

						double totalTimeWorkedFirstSolu = soluFirst.getTotalTimeWorked();
						double totalTimeWaitingFirstSolu = soluFirst.getTotalTimeWaiting();
						double fairnessValueFirstSolu = soluFirst.getFairnessValue();

						Solution firstSolu = new Solution(routesArcs, valuesTimeWorkedByWorker, valuesTimeWaitedByWorker, totalTimeWorkedFirstSolu, totalTimeWaitingFirstSolu, fairnessValueFirstSolu);
						solus.create2(firstSolu);

						//TOTAL TIME SOLUTION FROM A DAY FROM A PLAN
						SolutionRoutes soluTotalTime = dayS.getTotalTimeSolution();

						Map<Integer,Route> routesArcsTT = new HashMap<Integer,Route>();
						Set<Entry<heuristic.objects.Worker, ArrayList<Arc>>> workersSoluTT = new HashSet<Entry<heuristic.objects.Worker, ArrayList<Arc>>>(soluTotalTime.getRoutesArcs().entrySet());
						Iterator<Entry<heuristic.objects.Worker, ArrayList<Arc>>> itTT = workersSoluTT.iterator();
						while(itTT.hasNext()){
							Entry<heuristic.objects.Worker, ArrayList<Arc>> entry = itTT.next();
							int idWorker = entry.getKey().getID();
							ArrayList<Arc> listArcs = entry.getValue();
							ArrayList<ArcSolu> listArcsSolu = new ArrayList<ArcSolu>();
							for(int j=0;j<listArcs.size();j++){
								Arc a = listArcs.get(j);
								ArcSolu arc = new ArcSolu(a.getNode1().getName(), a.getNode2().getName(), a.getBestTime(), a.getBestTime()+a.getNode2().getDuration(),a.getWaitTime(),a.getNode2().getType(),a.getNode2().getTypeTransport());
								solus.create3(arc);
								listArcsSolu.add(arc);
							}
							Route r = new Route(listArcsSolu);
							solus.create4(r);
							routesArcsTT.put(idWorker, r);
						}

						Map<Integer, Double> valuesTimeWorkedByWorkerTT = new HashMap<Integer, Double>();
						Set<Entry<heuristic.objects.Worker, Double>> workersTimeWorkedSoluTT = new HashSet<Entry<heuristic.objects.Worker, Double>>(soluTotalTime.getValuesTimeWorkedByWorker().entrySet());
						Iterator<Entry<heuristic.objects.Worker, Double>> itTT2 = workersTimeWorkedSoluTT.iterator();
						while(itTT2.hasNext()){
							Entry<heuristic.objects.Worker, Double> entry2 = itTT2.next();
							int idWorker2 = entry2.getKey().getID();
							double timeWorked = entry2.getValue();

							valuesTimeWorkedByWorkerTT.put(idWorker2, timeWorked);

						}

						Map<Integer, Double> valuesTimeWaitedByWorkerTT = new HashMap<Integer, Double>();
						Set<Entry<heuristic.objects.Worker, Double>> workersTimeWaitSoluTT = new HashSet<Entry<heuristic.objects.Worker, Double>>(soluTotalTime.getValuesTimeWaitingByWorker().entrySet());
						Iterator<Entry<heuristic.objects.Worker, Double>> itTT3 = workersTimeWaitSoluTT.iterator();
						while(itTT3.hasNext()){
							Entry<heuristic.objects.Worker, Double> entry3 = itTT3.next();
							int idWorker3 = entry3.getKey().getID();
							double timeWait = entry3.getValue();

							valuesTimeWaitedByWorkerTT.put(idWorker3, timeWait);

						}

						double totalTimeWorkedFirstSoluTT = soluTotalTime.getTotalTimeWorked();
						double totalTimeWaitingFirstSoluTT = soluTotalTime.getTotalTimeWaiting();
						double fairnessValueFirstSoluTT = soluTotalTime.getFairnessValue();

						Solution totalTimeSolu = new Solution(routesArcsTT, valuesTimeWorkedByWorkerTT, valuesTimeWaitedByWorkerTT, totalTimeWorkedFirstSoluTT, totalTimeWaitingFirstSoluTT, fairnessValueFirstSoluTT);
						solus.create2(totalTimeSolu);

						//TOTAL WAIT TIME SOLUTION FROM A DAY FROM A PLAN
						SolutionRoutes soluTotalWaitTime = dayS.getWaitingTimeSolution();

						Map<Integer,Route> routesArcsTW = new HashMap<Integer,Route>();
						Set<Entry<heuristic.objects.Worker, ArrayList<Arc>>> workersSoluTW = new HashSet<Entry<heuristic.objects.Worker, ArrayList<Arc>>>(soluTotalWaitTime.getRoutesArcs().entrySet());
						Iterator<Entry<heuristic.objects.Worker, ArrayList<Arc>>> itTW = workersSoluTW.iterator();
						while(itTW.hasNext()){
							Entry<heuristic.objects.Worker, ArrayList<Arc>> entry = itTW.next();
							int idWorker = entry.getKey().getID();
							ArrayList<Arc> listArcs = entry.getValue();
							ArrayList<ArcSolu> listArcsSolu = new ArrayList<ArcSolu>();
							for(int j=0;j<listArcs.size();j++){
								Arc a = listArcs.get(j);
								ArcSolu arc = new ArcSolu(a.getNode1().getName(), a.getNode2().getName(), a.getBestTime(), a.getBestTime()+a.getNode2().getDuration(),a.getWaitTime(),a.getNode2().getType(),a.getNode2().getTypeTransport());
								solus.create3(arc);
								listArcsSolu.add(arc);
							}
							Route r = new Route(listArcsSolu);
							solus.create4(r);
							routesArcsTW.put(idWorker, r);
						}

						Map<Integer, Double> valuesTimeWorkedByWorkerTW = new HashMap<Integer, Double>();
						Set<Entry<heuristic.objects.Worker, Double>> workersTimeWorkedSoluTW = new HashSet<Entry<heuristic.objects.Worker, Double>>(soluTotalWaitTime.getValuesTimeWorkedByWorker().entrySet());
						Iterator<Entry<heuristic.objects.Worker, Double>> itTW2 = workersTimeWorkedSoluTW.iterator();
						while(itTW2.hasNext()){
							Entry<heuristic.objects.Worker, Double> entry2 = itTW2.next();
							int idWorker2 = entry2.getKey().getID();
							double timeWorked = entry2.getValue();

							valuesTimeWorkedByWorkerTW.put(idWorker2, timeWorked);

						}

						Map<Integer, Double> valuesTimeWaitedByWorkerTW = new HashMap<Integer, Double>();
						Set<Entry<heuristic.objects.Worker, Double>> workersTimeWaitSoluTW = new HashSet<Entry<heuristic.objects.Worker, Double>>(soluTotalWaitTime.getValuesTimeWaitingByWorker().entrySet());
						Iterator<Entry<heuristic.objects.Worker, Double>> itTW3 = workersTimeWaitSoluTW.iterator();
						while(itTW3.hasNext()){
							Entry<heuristic.objects.Worker, Double> entry3 = itTW3.next();
							int idWorker3 = entry3.getKey().getID();
							double timeWait = entry3.getValue();

							valuesTimeWaitedByWorkerTW.put(idWorker3, timeWait);

						}

						double totalTimeWorkedFirstSoluTW = soluTotalWaitTime.getTotalTimeWorked();
						double totalTimeWaitingFirstSoluTW = soluTotalWaitTime.getTotalTimeWaiting();
						double fairnessValueFirstSoluTW = soluTotalWaitTime.getFairnessValue();

						Solution totalTimeWaitSolu = new Solution(routesArcsTW, valuesTimeWorkedByWorkerTW, valuesTimeWaitedByWorkerTW, totalTimeWorkedFirstSoluTW, totalTimeWaitingFirstSoluTW, fairnessValueFirstSoluTW);
						solus.create2(totalTimeWaitSolu);

						//FAIR SOLUTION FROM A DAY FROM A PLAN
						SolutionRoutes soluFair = dayS.getFairSolution();

						Map<Integer,Route> routesArcsF = new HashMap<Integer,Route>();
						Set<Entry<heuristic.objects.Worker, ArrayList<Arc>>> workersSoluF = new HashSet<Entry<heuristic.objects.Worker, ArrayList<Arc>>>(soluFair.getRoutesArcs().entrySet());
						Iterator<Entry<heuristic.objects.Worker, ArrayList<Arc>>> itF = workersSoluF.iterator();
						while(itF.hasNext()){
							Entry<heuristic.objects.Worker, ArrayList<Arc>> entry = itF.next();
							int idWorker = entry.getKey().getID();

							ArrayList<Arc> listArcs = entry.getValue();
							ArrayList<ArcSolu> listArcsSolu = new ArrayList<ArcSolu>();
							for(int j=0;j<listArcs.size();j++){
								Arc a = listArcs.get(j);
								ArcSolu arc = new ArcSolu(a.getNode1().getName(), a.getNode2().getName(), a.getBestTime(), a.getBestTime()+a.getNode2().getDuration(),a.getWaitTime(),a.getNode2().getType(),a.getNode2().getTypeTransport());
								solus.create3(arc);
								listArcsSolu.add(arc);
							}

							Route r = new Route(listArcsSolu);
							solus.create4(r);
							routesArcsF.put(idWorker, r);
						}

						Map<Integer, Double> valuesTimeWorkedByWorkerF = new HashMap<Integer, Double>();
						Set<Entry<heuristic.objects.Worker, Double>> workersTimeWorkedSoluF = new HashSet<Entry<heuristic.objects.Worker, Double>>(soluFair.getValuesTimeWorkedByWorker().entrySet());
						Iterator<Entry<heuristic.objects.Worker, Double>> itF2 = workersTimeWorkedSoluF.iterator();
						while(itF2.hasNext()){
							Entry<heuristic.objects.Worker, Double> entry2 = itF2.next();
							int idWorker2 = entry2.getKey().getID();
							double timeWorked = entry2.getValue();

							valuesTimeWorkedByWorkerF.put(idWorker2, timeWorked);

						}

						Map<Integer, Double> valuesTimeWaitedByWorkerF = new HashMap<Integer, Double>();
						Set<Entry<heuristic.objects.Worker, Double>> workersTimeWaitSoluF = new HashSet<Entry<heuristic.objects.Worker, Double>>(soluFair.getValuesTimeWaitingByWorker().entrySet());
						Iterator<Entry<heuristic.objects.Worker, Double>> itF3 = workersTimeWaitSoluF.iterator();
						while(itF3.hasNext()){
							Entry<heuristic.objects.Worker, Double> entry3 = itF3.next();
							int idWorker3 = entry3.getKey().getID();
							double timeWait = entry3.getValue();

							valuesTimeWaitedByWorkerF.put(idWorker3, timeWait);

						}

						double totalTimeWorkedFirstSoluF = soluFair.getTotalTimeWorked();
						double totalTimeWaitingFirstSoluF = soluFair.getTotalTimeWaiting();
						double fairnessValueFirstSoluF = soluFair.getFairnessValue();

						Solution fairSolu = new Solution(routesArcsF, valuesTimeWorkedByWorkerF, valuesTimeWaitedByWorkerF, totalTimeWorkedFirstSoluF, totalTimeWaitingFirstSoluF, fairnessValueFirstSoluF);
						solus.create2(fairSolu);

						SolutionsDay solusDayPlan = new SolutionsDay(p, i, firstSolu, totalTimeSolu, totalTimeWaitSolu, fairSolu);

						solus.create(solusDayPlan);
					}
				}

				return p;

			}
		}

		return null;
	}

	@CrossOrigin
	@RequestMapping(value="/{id}/plans", method = RequestMethod.GET)
	Plan[] getPlans(@PathVariable int id) {
		User u = users.findById(id);

		if(u != null){
			return plans.findAllPlans(u);
		}

		return null;
	}


	/*
	 * CREATE Worker on plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/workers", method = RequestMethod.POST)
	Worker createWorker(@PathVariable int idU, @PathVariable int idP, @RequestBody Worker newW) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				Worker newWorker = new Worker(newW.getNameWorker(), newW.getDirPic(), newW.getWorkerContact(),newW.getEmailWorker(), newW.getInfoWorker(), p);
				workers.create(newWorker);
				return newWorker;
			}

		}
		else{
			Worker fictionalWorker = new Worker();
			return fictionalWorker;
		}

		return null;
	}


	/*
	 * GET Vans of plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/vans", method = RequestMethod.GET)
	Van[] getVans(@PathVariable int idU, @PathVariable int idP) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				return vans.findByPlan(p);
			}
		}

		return null;
	}

	/*
	 * CREATE Van on plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/vans", method = RequestMethod.PUT)
	Plan createVan(@PathVariable int idU, @PathVariable int idP,  @RequestBody int[] vansCap) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){

				vans.removeAllByPlan(p);

				List<Van> vansList = new ArrayList<Van>(vansCap.length);
				for(int i=0; i<vansCap.length; i++){
					Van v = new Van(vansCap[i], p);
					vansList.add(v);
					vans.create(v);
				}

				plans.addVans(p, vansList);

				return p;
			}

		}

		return null;
	}


	/*
	 * GET Workers of plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/workers", method = RequestMethod.GET)
	Worker[] getWorkers(@PathVariable int idU, @PathVariable int idP) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				return workers.findAllPlans(p);
			}
		}

		return null;
	}

	/*
	 * GET Worker of plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/workers/{idW}", method = RequestMethod.GET)
	Worker getWorker(@PathVariable int idU, @PathVariable int idP, @PathVariable int idW) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				return workers.findById(idW);
			}
		}

		return null;
	}

	/*
	 * DELETE Worker of plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/workers/{idW}", method = RequestMethod.DELETE)
	Worker removeWorker(@PathVariable int idU, @PathVariable int idP, @PathVariable int idW) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				Worker w = workers.findById(idW);
				if(w != null){
					workers.remove(idW);
					return w;
				}				
			}
		}

		return null;
	}

	/*
	 * UPDATE Worker of plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/workers/{idW}", method = RequestMethod.PUT)
	Worker editWorker(@PathVariable int idU, @PathVariable int idP, @PathVariable int idW, @RequestBody Worker editWorker) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);

			if(p != null){
				Worker w = workers.findById(idW);

				if(w != null){
					workers.update(w, editWorker);
					return w;
				}
			}
		}

		return null;
	}


	/*
	 * CREATE Patient on plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/patients", method = RequestMethod.POST)
	Patient createPatient(@PathVariable int idU, @PathVariable int idP, @RequestBody Patient newP) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				Patient newPatient = new Patient(newP.getName(), newP.getDirPic(), newP.getLati(), newP.getLongi(), newP.getContact(), newP.getEmail(), newP.getAdress(), newP.getAdress(), p);
				patients.create(newPatient);
				return newPatient;
			}

		}
		else{
			Patient fictionalPatient = new Patient();
			return fictionalPatient;
		}

		return null;
	}


	/*
	 * GET Patients of plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/patients", method = RequestMethod.GET)
	Patient[] getPatients(@PathVariable int idU, @PathVariable int idP) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				return patients.findAllPatients(p);
			}
		}

		return null;
	}

	/*
	 * GET Patient of plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/patients/{idD}", method = RequestMethod.GET)
	Patient getPatient(@PathVariable int idU, @PathVariable int idP, @PathVariable int idD) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				return patients.findById(idD);
			}
		}

		return null;
	}

	/*
	 * DELETE Patient of plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/patients/{idD}", method = RequestMethod.DELETE)
	Patient removePatient(@PathVariable int idU, @PathVariable int idP, @PathVariable int idD) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				Patient pa = patients.findById(idD);
				if(p != null){
					patients.remove(idD);
					return pa;
				}				
			}
		}

		return null;
	}

	/*
	 * UPDATE Patient of plan of user
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/patients/{idD}", method = RequestMethod.PUT)
	Patient editPatient(@PathVariable int idU, @PathVariable int idP, @PathVariable int idD, @RequestBody Patient editPatient) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);

			if(p != null){
				Patient pa = patients.findById(idD);

				if(pa != null){
					patients.update(pa, editPatient);
					return pa;
				}
			}
		}

		return null;
	}


	/*
	 * GET Tasks of Patient of Plan of User
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/patients/{idD}/tasks", method = RequestMethod.GET)
	Task[] getTasks(@PathVariable int idU, @PathVariable int idP, @PathVariable int idD) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				Patient d = p.getPatient(idD);
				if(d != null){
					return tasks.findAllTasksByPatient(d);	
				}

			}
		}

		return null;
	}


	/*
	 * GET Tasks of Patient of Plan of User
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/patients/{idD}/tasks/{idT}", method = RequestMethod.GET)
	Task getTasks(@PathVariable int idU, @PathVariable int idP, @PathVariable int idD, @PathVariable int idT) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				Patient d = p.getPatient(idD);
				if(d != null){
					return tasks.findById(idT);
				}

			}
		}

		return null;
	}

	/*
	 * CREATE Task on Patient of Plan of User
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/patients/{idD}/tasks", method = RequestMethod.POST)
	Task createTask(@PathVariable int idU, @PathVariable int idP, @PathVariable int idD, @RequestBody Task newT) {
		User u = users.findById(idU);
		System.out.println("aaa111");
		if(u != null){
			System.out.println("aaa222");
			Plan p = u.getPlan(idP);
			if(p != null){
				System.out.println("aaa333");
				Patient pa = patients.findById(idD);
				if( pa != null){
					System.out.println("aaa444: "+newT.getTypeTask()+"-"+newT.getTypeTransportTask()+"-"+newT.getIniTWTask()+"-"+newT.getFinTWTask()+"-"+newT.getChangeTWTask()+"-"+newT.getDayTask()+"-"+newT.getInfoTask());
					Task newTask = new Task(newT.getTypeTask(), newT.getTypeTransportTask(), newT.getIniTWTask(), newT.getFinTWTask(), newT.getDurationTask(), newT.getChangeTWTask(), newT.getDayTask(), newT.getInfoTask(), newT.getPatientTask());
					tasks.create(newTask);
					return newTask;
				}
				else{
					System.out.println("aaa555");
					Task fictionalTask = new Task();
					return fictionalTask;
				}

			}
			else{
				Task fictionalTask = new Task();
				return fictionalTask;
			}

		}
		else{
			Task fictionalTask = new Task();
			return fictionalTask;
		}

	}

	/*
	 * DELETE Task of Patient of Plan of User
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/patients/{idD}/tasks/{idT}", method = RequestMethod.DELETE)
	Task removeTask(@PathVariable int idU, @PathVariable int idP, @PathVariable int idD, @PathVariable int idT) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				Patient pa = patients.findById(idD);
				if(pa != null){
					Task t = tasks.findById(idT);
					if(t != null){
						tasks.remove(idT);
						return t;
					}
				}				
			}
		}

		return null;
	}

	/*
	 * UPDATE Task of Patient of Plan of User
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/patients/{idD}/tasks/{idT}", method = RequestMethod.PUT)
	Task editTask(@PathVariable int idU, @PathVariable int idP, @PathVariable int idD, @PathVariable int idT, @RequestBody Task editTask) {
		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);

			if(p != null){
				Patient pa = patients.findById(idD);

				if(pa != null){
					Task t = tasks.findById(idT);

					if(t != null){
						tasks.update(t, editTask);
						return t;
					}

				}
			}
		}

		return null;
	}

	/*
	 * GET Tasks of Patient of Plan of User
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/solus", method = RequestMethod.GET)
	SolutionsDay[] getSolus(@PathVariable int idU, @PathVariable int idP) {

		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				return solus.findByPlan(p);
			}
		}

		return null;
	}


	/*
	 * REMOVE Sols
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/solus", method = RequestMethod.DELETE)
	Plan removeSolus(@PathVariable int idU, @PathVariable int idP) {

		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				List<SolutionsDay> sols = p.getSolus();
				for(int i=0; i<sols.size(); i++){
					solus.remove(sols.get(i).getSoluID());
				}
				//				plans.removeSolus(idP);
				return p;
			}
		}

		return null;
	}


	/*
	 * GET Sols
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/sol", method = RequestMethod.GET)
	Solution[] getSolus2(@PathVariable int idU, @PathVariable int idP) {

		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				return solus.findAll2();
			}
		}

		return null;
	}

	/*
	 * GET Arcs
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/arcsSol", method = RequestMethod.GET)
	ArcSolu[] getArcsSolu(@PathVariable int idU, @PathVariable int idP) {

		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				return solus.findAll3();
			}
		}

		return null;
	}

	/*
	 * GET Routes
	 */
	@CrossOrigin
	@RequestMapping(value="/{idU}/plans/{idP}/routesSol", method = RequestMethod.GET)
	Route[] getRoutesSolu(@PathVariable int idU, @PathVariable int idP) {

		User u = users.findById(idU);

		if(u != null){
			Plan p = u.getPlan(idP);
			if(p != null){
				return solus.findAll4();
			}
		}

		return null;
	}

}
